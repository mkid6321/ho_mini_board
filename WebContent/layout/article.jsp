<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="decorator" uri="http://www.opensymphony.com/sitemesh/decorator" %>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<title><decorator:title default="SIMPLE" /></title>
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
	<link rel="icon" href="favicon.ico" type="image/x-icon">
	<link rel='stylesheet' type='text/css' media='all' href="css/style.css" />
	<style type="text/css">
		#header   {position:relative;width:640px;margin:auto;}
		#contents {position:relative;width:640px;margin:auto;}			
		#footer   {position:relative;width:640px;margin:auto;}		
	</style>
	<script src="js/jquery-2.1.4.min.js"></script>
	<script src="js/common.js"></script>
	<decorator:head />
</head>

<body style="text-align:center;margin:0 auto;">
	<div style="height:40px;">		
		<div style="position:relative;">

		</div>
	</div>
	
	<div style="position:relative;margin:0 auto;">		
		<div style="">			
			<div id="contents"><decorator:body /></div>			
		</div>		
	</div>
	
	<div style="position:relative;margin:0 auto;">		
		<div style="">			
			<div id="footer"><br/><br/><br/><br/><br/></div>			
		</div>		
	</div>
</body>

</html>