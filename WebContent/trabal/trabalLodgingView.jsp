<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="me" uri="/WEB-INF/CustomTag.tld" %>

<html>
<head>

<style>

	
</style>
<script type="text/javascript">


</script>
</head>
<body>
<header class="blue accent-3 relative nav-sticky">
        <div class="container-fluid text-white">
            <div class="row">
                <div class="col">
                    <h3 class="my-3">
                        <i class="icon icon-note-important"></i> 숙박업체 상세정보 
                    </h3>
                </div>
            </div>
        </div>
    </header>
<div class="container-fluid my-3">
    <div class="d-flex row">
        <div class="col-md-7">
                <!-- Basic Validation -->
                <div class="card mb-3 shadow no-b r-0">
                    <div class="card-body">
                            <div class="form-group">
                                <div class="form-line">
                                    <label class="form-label">숙박업체명</label>
                                    <div class="border pt-2 pb-2 pl-2 pr-2 r-3 white">여기어때</div>
                                </div>
                            </div>
                            <div class="form-group">
                            	<div class="form-line">
	                                <label class="form-label">숙박유형</label>
		                            <div class="border pt-2 pb-2 pl-2 pr-2 r-3 white">여기어때</div>
	                            </div>
                            </div>
                            <div class="form-group">
                                <div class="form-line">
                                	<label class="form-label">주소</label>
                                    <div class="border pt-2 pb-2 pl-2 pr-2 r-3 white">여기어때</div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-line">
                                	<label class="form-label">전화번호</label>
                                    <div class="border pt-2 pb-2 pl-2 pr-2 r-3 white">여기어때</div>
                                </div>
                            </div>
                            <div class="form-group">
                            	<div class="form-line">
	                                <label class="form-label">편의시설</label>
		                            <div class="border pt-2 pb-2 pl-2 pr-2 r-3 white">여기어때</div>
	                            </div>
                            </div>
                            <div class="form-group">
                            	<div class="form-line">
                            		<label class="form-label">사진</label>
                            		<div class="border pt-2 pb-2 pl-2 pr-2 r-3 white">여기어때</div>
                            	</div>
							</div>
                            <div class="form-group">
                                <div class="form-line">
                                	<label class="form-label">상세설명</label>
                                    <div class="border pt-2 pb-2 pl-2 pr-2 r-3 white">
		                                 <p>ㆍ주변정보</p>
		                                 <p>ㆍ기본정보</p>
		                                 <p>ㆍ인원 추가 정보</p>
		                                 <p>ㆍ투숙객 혜택</p>
		                                 <p>ㆍ부대시설 정보</p>
		                                 <p>ㆍ조식 정보</p>
		                                 <p>ㆍ취소 및 환불 규정</p>
		                                 <p>ㆍ확인사항 및 기타</p>
		                            </div>
                                </div>
                            </div>
                            <div class="form-group">
                            	<div class="form-line">
	                                <label class="form-label">삭제여부</label>
		                            <div class="border pt-2 pb-2 pl-2 pr-2 r-3 white">삭제</div>
	                            </div>
                            </div>
                            <button class="btn btn-primary waves-effect" type="submit">수정</button>
                    </div>
                </div>
                <!-- #END# Basic Validation -->
            </div>
        <div class="col-md-5">
        	<h3 class="my-3">
                          객실목록 <a class="btn btn-outline-primary btn-xs" href="./trabalRoomAdd.action"> 객실등록</a>
                          <a class="btn btn-outline-primary btn-xs" href="./trabalPriceCalendar.action"> 가격테이블</a>
            </h3>
            <hr>
            
            
        </div>
    </div>
</div>
	
</body>


</html>