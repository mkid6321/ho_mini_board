<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="me" uri="/WEB-INF/CustomTag.tld" %>

<html>
<head>

<style>

	
</style>
<script type="text/javascript" src="js/multifile.js"></script>
<script type="text/javascript">

function jsUploaded(src, contentType, length) {
	var new_row = document.createElement('div');
	new_row.style.cssText = 'display: inline-block;text-align:center;';

	// Delete button
	var new_row_button = document.createElement( 'input' );
	new_row_button.type = 'button';
	new_row_button.value = '삭제';
	
	var hidden_0 = document.createElement( 'input' );
	hidden_0.type = 'hidden';
	hidden_0.name = 'nic_filename'; 
	hidden_0.value = src;
	
	var hidden_2 = document.createElement( 'input' );
	hidden_2.type = 'hidden';
	hidden_2.name = 'nic_length'; 
	hidden_2.value = length;
	
	var hidden_1 = document.createElement( 'input' );
	hidden_1.type = 'hidden';
	hidden_1.name = 'nic_contentType'; 
	hidden_1.value = contentType;
	
	// References
	var element = document.createElement('img');
	element.style.cssText = 'width:50px;height:50px;margin:4px 1px;';
	element.src = src;
	
	new_row.appendChild(element);
	new_row.appendChild(document.createElement('br'));
	
	// Delete function
	new_row_button.onclick= function(){
		
		new_row.parentNode.removeChild(new_row);

		// Appease Safari
		//    without it Safari wants to reload the browser window
		//    which nixes your already queued uploads
		return false;
	};

	// Add button
	new_row.appendChild(new_row_button);
	
	// Add param
	new_row.appendChild(hidden_0);
	new_row.appendChild(hidden_1);
	new_row.appendChild(hidden_2);
}

</script>
</head>
<body>
<header class="blue accent-3 relative nav-sticky">
    <div class="container-fluid text-white">
        <div class="row">
            <div class="col">
                <h3 class="my-3">
                    <i class="icon icon-note-important"></i>
                    Form Validation <span class="s-14"> <a class="btn btn-outline-primary btn-xs" href="https://jqueryvalidation.org/" target="_blank"> View Plugin Docs</a> </span>
                </h3>
            </div>
        </div>
    </div>
</header>
<div class="container-fluid my-3">
    <div class="d-flex row">
        <div class="col-md-7">
                <!-- Basic Validation -->
                <div class="card mb-3 shadow no-b r-0">
                    <div class="card-body">
                        <form id="form_validation" action="./trabalRoomInsert.action" method="post" enctype="multipart/form-data" novalidate="novalidate">
                        <input type="hidden" id="lodging_idx" value="1">
                            <div class="form-group">
                                <div class="form-line">
                                    <label class="form-label">객실명fqwe</label>
                                    <input type="text" class="form-control" name="room_name" required="">
                                </div>
                            </div>
                            <div class="form-group">
                            	<div class="form-row">
                                    <div class="col-md-6">
                                        <label for="validationCustom01">기준인원</label>
                                        <input type="text" name="quota" class="form-control" id="validationCustom01" placeholder="First name" value="Mark" required="">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="validationCustom02">최대인원</label>
                                        <input type="text" name="max_quota" class="form-control" id="validationCustom02" placeholder="Last name" value="Otto" required="">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                            	<div class="form-line">
	                                <label class="form-label">편의시설</label>
		                            <%-- <select class="select2" name="facility[]" multiple="multiple"> --%>
		                            <select class="select2" name="facility" multiple="multiple">
		                                <option value="AL">TV</option>
		                                <option value="WY">금고</option>
		                                <option value="LH">냉장고</option>
		                                <option value="KA">헤어드라이기</option>
		                            </select>
	                            </div>
                            </div>
                            <div class="form-group">
                            	<div class="form-line">
                            		<label class="form-label">사진 업로드</label>
                            		<input id="my_file_element" type="file" />
                            	</div>
								<div id="files_list" style="border:1px solid #DEDEDE;padding:5px;margin-top:5px;background:#fff;"><strong>Files (maximum 20):</strong></div>
								<script>
									var multi_selector1 = new MultiSelector(document.getElementById('files_list'), 20, 'file');
									multi_selector1.addElement(document.getElementById('my_file_element'));
								</script>	
							</div>
                            <div class="form-group">
                                <div class="form-line">
                                	<label class="form-label">상세설명</label>
                                    <textarea class="editor" placeholder="상세설명" rows="17" name="description" id="contents">
		                                 <p>ㆍ기본정보</p>
		                                 <p>ㆍ예약공지</p>
		                                 <p>ㆍ취소 및 환불 규정</p>
		                                 <p>ㆍ확인사항 및 기타</p>
		                            </textarea>
                                </div>
                            </div>
                            <div class="form-group">
                            	<div class="form-line">
	                                <label class="form-label">삭제여부</label>
		                            <select class="custom-select select2" required>
		                                <option value="">선택하세요.</option>
		                                <option value="true">삭제</option>
		                                <option value="false">활성</option>
		                            </select>
	                            </div>
                            </div>
                            <button class="btn btn-primary waves-effect" type="submit">등록</button>
                        </form>
                    </div>
                </div>
                <!-- #END# Basic Validation -->
            </div>
        <div class="col-md-5">
            <h3>등록시 유의사항</h3>
            <hr>
            <p>These forms are using jquery validations plugin its flexible and easy to use. Please check official docs to write your own rules</p>
            <a href="https://jqueryvalidation.org/" target="_blank" class="btn btn-xs btn-primary">Plugin Docs</a>
            <h4 class="mt-5">Related Files</h4>
            <table class="table">
                <tbody>
                <tr>
                    <th>Type</th>
                    <th>File</th>
                </tr>
                <tr>
                    <td><span class="badge badge-danger">HTML</span></td>
                    <td>
                        form-jquery-validations.html
                    </td>
                </tr>
                <tr>
                    <td><span class="badge badge-warning">JS</span></td>
                    <td>
                        _validations.js
                    </td>
                </tr>
                </tbody>
            </table>
            <h4 class="mt-5">Code Example</h4>
            <hr>
        </div>
    </div>
</div>
	
</body>


</html>