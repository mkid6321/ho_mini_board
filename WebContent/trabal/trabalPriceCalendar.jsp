<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="me" uri="/WEB-INF/CustomTag.tld" %>

<html>
<head>

<style>

	
</style>
<script type="text/javascript">

function fc_event(uid) {
	
	$("#calendar").fullCalendar('removeEvents', uid); 
	
	return false;
}

</script>
</head>
<body>


<header class="blue accent-3 relative">
    <div class="container-fluid text-white">
        <div class="row p-t-b-10">
            <div class="col">
                <h4>
                    <i class="icon-calendar"></i> 가격테이블
                </h4>
            </div>
        </div>
        <div class="row ">
            <ul class="nav">
                <li>
                    <a class="nav-link" href="#"><i class="icon icon-list"></i>달력</a>
                </li>
                <li>
                    <a class="nav-link active" href="#"><i class="icon icon-clipboard-add"></i>엑셀등록</a>
                </li>
            </ul>
        </div>
    </div>
</header>
<div class="animatedParent animateOnce">
    <div class="container-fluid p-0">
            <div class="row no-gutters">
                    <div class="col-md-3">
                        <div class="card r-0 b-0 shadow sticky">
                            <div class="card-header white ">
                                <h6>Draggable Events</h6>
                            </div>

                            <div class="card-body b-t pt-2 pb-2 no-b">
                                <div class="checkbox">
                                    <label for="drop-remove">
                                        <input type="checkbox" id="drop-remove">
                                        Remove chip once added in calander
                                    </label>
                                </div>
                            </div>
                            <div class="card-footer white">
                                <div class="btn-group" style="width: 100%; margin-bottom: 10px;">
                                    <ul class="fc-color-picker list-inline-item" id="color-chooser">
                                        <li class="list-inline-item"><a class="" href="#" style="color: rgb(60, 141, 188);"><i class="icon-circle s-24"></i> 기준가</a></li>
                                        <li class="list-inline-item"><a class="" href="#" style="color: rgb(211, 47, 47);"><i class="icon-circle s-24"></i> 할인가</a></li>
                                    </ul>
                                </div>
                                <div class="input-group">
                                    <input id="new-event" type="text" class="form-control r-30" placeholder="Event Title">
                                    <div class="input-group-btn">
                                        <a id="add-new-event" class="btn-fab shadow btn-danger ml-2" style="background-color: rgb(60, 141, 188); border-color: rgb(60, 141, 188); color: rgb(255, 255, 255);"><i class="icon-add"></i></a>
                                    </div>
                                    <!-- /btn-group -->
                                </div>
                                <!-- /input-group -->
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <div id="external-events" class="p-3">
                            
                        </div>

                      <!-- /. box -->

                    </div>
            <!-- /.col -->
            <div class="col-md-9">
              <div class="card no-r no-b shadow">
                <div class="card-body p-0">
                    <div id='calendar'></div>
                </div>
              </div>
        </div>
        </div>
    </div>
</div>
    
</body>

<!-- 일별 이벤트 가져오기 
var events = $('#calendar').fullCalendar('clientEvents');
for (var i = 0; i < events.length; i++) {
    var start_date = new Date(events[i].start._d);
    var end_date = '';
    if (events[i].end != null) {
        end_date = new Date(events[i].end._d);
    }
    var title = events[i].title;

    var st_day = start_date.getDate();
    var st_monthIndex = start_date.getMonth() + 1;
    var st_year = start_date.getFullYear();

    var en_day ='';
    var en_monthIndex = '';
    var en_year = '';
    if (end_date != '') {
        en_day = end_date.getDate()-1;
        en_monthIndex = end_date.getMonth()+1;
        en_year = end_date.getFullYear();
    }

    console.log('Title-'+title+', start Date-' + st_year + '-' + st_monthIndex + '-' + st_day + ' , End Date-' + en_year + '-' + en_monthIndex + '-' + en_day);
}
-->

</html>