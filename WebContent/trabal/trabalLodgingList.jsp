<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="me" uri="/WEB-INF/CustomTag.tld" %>

<html>
<head>

<style>

	
</style>
<script type="text/javascript">

function jsLodgingView() {
	location.href = './trabalLodgingView.action';
}

</script>
</head>
<body>

<header class="blue accent-3 relative nav-sticky">
        <div class="container-fluid text-white">
            <div class="row p-t-b-10 ">
                <div class="col">
                    <h4>
                        <i class="icon-box"></i>
                        Projects
                    </h4>
                </div>
            </div>
            <div class="row">
                <ul class="nav responsive-tab nav-material nav-material-white" id="v-pills-tab">
                    <li>
                        <a class="nav-link active" id="v-pills-1-tab" data-toggle="pill" href="#v-pills-1">
                            <i class="icon icon-home2"></i>Today</a>
                    </li>
                    <li>
                        <a class="nav-link" id="v-pills-2-tab" data-toggle="pill" href="#v-pills-2"><i class="icon icon-plus-circle mb-3"></i>Yesterday</a>
                    </li>
                    <li>
                        <a class="nav-link" id="v-pills-3-tab" data-toggle="pill" href="#v-pills-3"><i class="icon icon-calendar"></i>By Date</a>
                    </li>
                </ul>
                <a class="btn-fab absolute fab-right-bottom btn-primary" data-toggle="control-sidebar">
                    <i class="icon icon-menu"></i>
                </a>
            </div>
        </div>
    </header>
<div class="container-fluid relative animatedParent animateOnce p-0">
    <div class="row no-gutters">
        <div class="col-md-9">
            <div class="pl-3 pr-3 my-3">
                <div class="row">
                    <div class="col-md-3">
                        <div class="card shadow my-3 no-b">
                            <img class="card-img-top" src="contents/1_main.jpg" alt="">
                            <div class="card-img-overlay">
                                <div class="avatar-group mt-3">
                                    <figure class="avatar">
                                        <img src="assets/img/dummy/u4.png" alt=""></figure>
                                    <figure class="avatar">
                                        <span class="avatar-letter avatar-letter-l circle"></span>
                                    </figure>
                                    <figure class="avatar">
                                        <img src="assets/img/dummy/u5.png" alt=""></figure>
                                    <figure class="avatar">
                                        <img src="assets/img/dummy/u6.png" alt=""></figure>
                                </div>
                            </div>
                            <div class="card-body">
                                <h6 class="mb-1">Paper Panel 7</h6>
                                <span>A light version of paper panel.</span>
                            </div>
                            <div class="p-2 b-t">
                                <div class="row">
                                    <div class="col-md-6">
                                               <span class="easy-pie-chart easy-pie-chart-sm" data-percent="30"
                                                     data-options='{"lineWidth": 5, "barColor": "#ed5564","size":60}'>
                                    <span class="percent">30</span>
                            </span>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="mt-2 ml-2">
                                            <strong>Deadline:</strong><br>2 Days Left
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card shadow my-3 no-b">
                            <img class="card-img-top" src="contents/2_main.jpg" alt="">
                            <div class="card-img-overlay">
                                <div class="avatar-group mt-3">
                                    <figure class="avatar">
                                        <img src="assets/img/dummy/u4.png" alt=""></figure>
                                    <figure class="avatar">
                                        <span class="avatar-letter avatar-letter-l circle"></span>
                                    </figure>
                                    <figure class="avatar">
                                        <img src="assets/img/dummy/u5.png" alt=""></figure>
                                    <figure class="avatar">
                                        <img src="assets/img/dummy/u6.png" alt=""></figure>
                                </div>
                            </div>
                            <div class="card-body">
                                <h6 class="mb-1">Paper Panel 7</h6>
                                <span>A light version of paper panel.</span>
                            </div>
                            <div class="p-2 b-t">
                                <div class="row">
                                    <div class="col-md-6">
                                               <span class="easy-pie-chart easy-pie-chart-sm" data-percent="55"
                                                     data-options='{"lineWidth": 5, "barColor": "#7dc855","size":60}'>
                                    <span class="percent">55</span>
                            </span>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="mt-2 ml-2">
                                            <strong>Deadline:</strong><br>2 Days Left
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card shadow my-3 no-b">
                            <img class="card-img-top" src="contents/3_main.jpg" alt="">
                            <div class="card-img-overlay">
                                <div class="avatar-group mt-3">
                                    <figure class="avatar">
                                        <img src="assets/img/dummy/u4.png" alt=""></figure>
                                    <figure class="avatar">
                                        <span class="avatar-letter avatar-letter-l circle"></span>
                                    </figure>
                                    <figure class="avatar">
                                        <img src="assets/img/dummy/u5.png" alt=""></figure>
                                    <figure class="avatar">
                                        <img src="assets/img/dummy/u6.png" alt=""></figure>
                                </div>
                            </div>
                            <div class="card-body">
                                <h6 class="mb-1">Paper Panel 7</h6>
                                <span>A light version of paper panel.</span>
                            </div>
                            <div class="p-2 b-t">
                                <div class="row">
                                    <div class="col-md-6">
                                               <span class="easy-pie-chart easy-pie-chart-sm" data-percent="55"
                                                     data-options='{"lineWidth": 5, "barColor": "#7dc855","size":60}'>
                                    <span class="percent">55</span>
                            </span>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="mt-2 ml-2">
                                            <strong>Deadline:</strong><br>2 Days Left
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card shadow my-3 no-b"  onclick="jsLodgingView();">
                            <img class="card-img-top" src="contents/4_main.jpg" alt="">
                            <div class="card-img-overlay">
                                <div class="avatar-group mt-3">
                                    <figure class="avatar">
                                        <img src="assets/img/dummy/u4.png" alt=""></figure>
                                    <figure class="avatar">
                                        <span class="avatar-letter avatar-letter-l circle"></span>
                                    </figure>
                                    <figure class="avatar">
                                        <img src="assets/img/dummy/u5.png" alt=""></figure>
                                    <figure class="avatar">
                                        <img src="assets/img/dummy/u6.png" alt=""></figure>
                                </div>
                            </div>
                            <div class="card-body">
                                <h6 class="mb-1">Paper Panel 7</h6>
                                <span>A light version of paper panel.</span>
                            </div>
                            <div class="p-2 b-t">
                                <div class="row">
                                    <div class="col-md-6">
                                               <span class="easy-pie-chart easy-pie-chart-sm" data-percent="55"
                                                     data-options='{"lineWidth": 5, "barColor": "#7dc855","size":60}'>
                                    <span class="percent">55</span>
                            </span>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="mt-2 ml-2">
                                            <strong>Deadline:</strong><br>2 Days Left
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <!-- Right Sidebar -->
            <aside class="white h-100 shadow p-t-20">
                <div class="col-sm-12 p-b-10">
                    <div class="card-title">지역</div>
                    <select class="select2" name="states[]" multiple="multiple">
                        <option value="강원도">강원도</option>
                        <option value="WY">경기도</option>
                        <option value="LH">충청북도</option>
                        <option value="KA">충청남도</option>
                    </select>
                </div>
                
                <div class="col-sm-12 p-b-10">
                    <div class="card-title">숙박유형</div>
                    <select class="select2" name="inp[]" multiple="multiple">
                        <option value="모텔">모텔</option>
                        <option value="호텔">호텔</option>
                        <option value="펜션">펜션</option>
                        <option value="게스트하우스">게스트하우스</option>
                    </select>
                </div>
                
                <div class="col-sm-12 p-b-10">
                    <label for="validationCustom01">숙박업체명</label>
                    <input type="text" class="form-control" id="validationCustom01" placeholder="" value="" required="">
                </div>
                        
                <div class="col-sm-12 p-b-10">
                	<button class="btn btn-primary" type="submit">검색</button>
                </div>
            </aside>
            <!-- /.right-sidebar -->
        </div>
    </div>
</div>
	
</body>

</html>