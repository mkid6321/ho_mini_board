<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>

<head>
<style>
.no{width:40px;text-align:center;}
.title{width:140px;text-align:left;}
.description{width:280px; text-align:left; }
.del_flag{width:80px; text-align:center;}
.action{width:80px; text-align:left;}

</style>
<script type="text/javascript">

function jsBulletinEdit(){

	var insertLevel = $.trim($('#acpp').val());	  
	if(insertLevel.length == 0 || insertLevel < 0 || insertLevel > 255){
		alert("입력값의 범위가 유효하지 않습니다.\n0 이상 255 이하를 입력해주세요.");
		return;
	}
	
	$('#admin').submit();
}
function jsAddCategory(){
	var categoryListElement = $('#categoryList');
	
	var flag = $('#C_DEL_FLAG').is(':checked');
	
	var html = "<tr style=\"height:27px;background-color: #f9f9f9\" onmouseover=\"this.style.backgroundColor='#eeeeee'\" onmouseout=\"this.style.backgroundColor='#f9f9f9'\">";
	html+="<td class=\"no assort\">"+($('#categoryList tr').length+1)+"</td>";		
	html+="<td class=\"title\"><input type=\"text\" name=\"C_CATEGORY_NAME\" value=\""+$('#C_CATEGORY_NAME').val()+"\" /></td>";		
	html+="<td class=\"description\"><input type=\"text\" name=\"C_DESCRIPTION\" style=\"width: 100%;\" value=\""+$('#C_DESCRIPTION').val()+"\" /><input type=\"hidden\" name=\"C_CATEGORY_IDX\"/></td>";	
	html+="<td class=\"del_flag\"><select name=\"C_DEL_FLAG\" style=\"height:20px;\"><option value=\"false\" "+((flag=="true")?"":"selected=\"selected\"")+">사용</option><option value=\"true\""+((flag=="true")?"selected=\"selected\"":"")+">미사용</option></select></td>";
	html+="<td class=\"action\"><a href=\"#\" class=\"button button_orange\" style=\"width: 20px;\" onclick=\"jsMoveUp(this); return false;\">↑</a> <a href=\"#\" class=\"button button_orange\" style=\"width: 20px;\" onclick=\"jsMoveDown(this); return false;\">↓</a> <a href=\"#\" class=\"button button_orange\" style=\"width: 20px;\" onclick=\"jsDelete(this); return false;\">x</a></td>";
	html+="</tr>";
	
	categoryListElement.append(html);
	
	$('#C_CATEGORY_NAME').val("");
	$('#C_DESCRIPTION').val("");
	$('#C_DEL_FLAG').attr("checked", false);
}
function jsMoveUp(obj){
	var row = obj.parentNode.parentNode;
	var prevRow = row.previousElementSibling;
	
	if(prevRow == null) return false;
	
	row.parentNode.insertBefore(row,prevRow);
	
	jsRowNum();
}
function jsMoveDown(obj){
	var row = obj.parentNode.parentNode;
	var nextRow = row.nextElementSibling;
	
	if(nextRow == null) return false;
	
	row.parentNode.insertBefore(row,nextRow.nextElementSibling);
	
	jsRowNum();
}
function jsDelete(obj){
	var row = obj.parentNode.parentNode;
	
	row.parentNode.removeChild(row);
	
	jsRowNum();
}
function jsRowNum() {
	var eleCategory = document.getElementById("categoryList");
	var eleChild = eleCategory.getElementsByClassName("no");
	
	for(i=0; i<eleChild.length; i++) {
		eleChild[i].innerHTML = (i+1);	
	}
}

$( window ).load(function() {
	$('#menu_bulletin > a').find(":last-child").removeClass("fa-angle-down");
	$('#menu_bulletin > a').find(":last-child").addClass("fa-angle-up");
	/* $('#menu_bulletin > ul').children().eq(0).css("background-color","#4d5565"); */
	$('#menu_bulletin > ul').children().eq(0).children().eq(0).css("color","#FFFFFF");
	$('#menu_bulletin > ul').show();
});
</script>
</head>
<body>

<div class="container-fluid my-3">

<div class="d-flex row">
	<div class="col-md-7">
        
	    <!-- Basic Validation -->
	    <div class="card mb-3 shadow no-b r-0">
	        <div class="card-header white">
	            <h6>게시판 페이지당 글 수</h6>
	        </div>
	        <div class="card-body">
	        	<form id="admin" action="./adminPropertyUpdate.action" class="needs-validation" novalidate>
				<input type="hidden" name="property_id" value="ARTICLE_COUNT_PER_PAGE"/>
				
				<div class="form-row">
					<div class="col-md-6 mb-3">
                        <label for="validationCustom01">글 수</label>
                        <input type="text" class="form-control" id="acpp" name="property_value" value="${BULLETIN_SETUP.ARTICLE_COUNT_PER_PAGE}" placeholder="" />
                        <div class="valid-feedback">
                            Looks good!
                        </div>
                    </div>                    
                </div>
				<button class="btn btn-primary" type="submit" onclick="jsBulletinEdit(); return false;">수정</button>
				<button class="btn btn-secondary" type="submit" onclick="jsBulletinCancel(); return false;">복원</button>
				</form>
	        </div>
	        
	    </div>
	    <!-- #END# Basic Validation -->
	    
	    <!-- Basic Validation -->
	    <div class="card mb-3 shadow no-b r-0">
	        <div class="card-header white">
	            <h6>게시판 기본 카테고리</h6>
	        </div>
	        <div class="card-body">
	        	<form id="admin" action="./adminPropertyUpdate.action" class="needs-validation" novalidate>
				<input type="hidden" name="property_id" value="BULLETIN_BASE_CATEGORY"/>
				
				<table style="width:100%;">
					<tr style="height:27px;border: 1px solid #dedede;border-bottom: 0;" class="titlebar_bg">
						<td class="no assort">순서</td>
						<td class="title assort">이름</td>
						<td class="description assort">설명</td>
						<td class="del_flag assort">사용여부</td>
						<td class="action assort"></td>
					</tr>
				</table>	
				<table id="categoryList" style="width:100%;border: 1px solid #dedede;">
				<c:forEach items="${BULLETIN_SETUP.BULLETIN_BASE_CATEGORY}" var="list" varStatus="status">
					<tr style="height:27px;background-color: #f9f9f9" onmouseover="this.style.backgroundColor='#eeeeee'" onmouseout="this.style.backgroundColor='#f9f9f9'">
						<td class="no assort">${list.output_seq}</td>			
						<td class="title"><input type="text" name="C_CATEGORY_NAME" value="${list.category_name}"/></td>		
						<td class="description"><input type="text" name="C_DESCRIPTION" value="${list.description}" style="width:100%;"/></td>		
						<td class="del_flag">
							<select name="C_DEL_FLAG" style="height:20px;" >
								<option value="true" ${list.del_flag == true?"selected":""}>미사용</option>
								<option value="false" ${list.del_flag == false?"selected":""}>사용</option>
							</select>
						</td>
						<td class="action"><a href="#" class="button button_orange" style="width: 20px;" onclick="jsMoveUp(this); return false;">↑</a> <a href="#" class="button button_orange" style="width: 20px;" onclick="jsMoveDown(this); return false;">↓</a> <a href="#" class="button button_orange" style="width: 20px;" onclick="jsDelete(this); return false;">x</a></td>						
					</tr>
				</c:forEach>
				</table>
				<table style="width:100%;">
					<tr style="height:27px;border: 1px solid #dedede;border-top: 0;" class="titlebar_bg">
						<td class="no assort">Last</td>
						<td class="title"><input type="text" id="C_CATEGORY_NAME" placeholder="Title"/></td>		
						<td class="description"><input type="text" id="C_DESCRIPTION" style="width: 100%" placeholder="Description"/></td>		
						<td class="del_flag"><select id="C_DEL_FLAG" style="height:20px;"><option value="false" selected="selected">사용</option><option value="true">미사용</option></select></td>
						<td class="action"><a href="#" class="button button_orange" onclick="jsAddCategory()">추가</a></td>
					</tr>
				</table>
				<br/>
				<button class="btn btn-primary" type="submit" onclick="javascript:document.bbc.submit(); return false;">수정</button>
				<button class="btn btn-secondary" type="submit" onclick="jsBulletinCancel(); return false;">복원</button>
				</form>
	        </div>
	        
	    </div>
	    <!-- #END# Basic Validation -->
	    
	</div>
</div>

</div>
</body>

</html>