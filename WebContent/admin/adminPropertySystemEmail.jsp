<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="me" uri="/WEB-INF/CustomTag.tld" %>

<html>
<head>
	<script type="text/javascript">
	
	function jsSetupUpdate(){
	  $('#admin').attr('action','./adminPropertyUpdate.action');
	  $('#admin').submit();
	}
	
	function jsBulletinEdit(){
		var insertLevel = $.trim($('#acpp').val());	  
		if(insertLevel.length == 0 || insertLevel < 0 || insertLevel > 255){
			alert("입력값의 범위가 유효하지 않습니다.\n0 이상 255 이하를 입력해주세요.");
			return;
		}
		
		$('#admin').submit();
	}
	
	$( window ).load(function() {
		$('#menu_setup > a').find(":last-child").removeClass("fa-angle-down");
		$('#menu_setup > a').find(":last-child").addClass("fa-angle-up");
		/* $('#menu_setup > ul').children().eq(1).css("background-color","#4d5565"); */
		$('#menu_setup > ul').children().eq(1).children().eq(0).css("color","#FFFFFF");
		$('#menu_setup > ul').show();
	});
	
	</script>
</head>
<body>

<div class="container-fluid my-3">

<div class="d-flex row">
	<div class="col-md-7">
	
		<!-- Basic Validation -->
	    <div class="card mb-3 shadow no-b r-0">
	        <div class="card-header white">
	            <h6>전자 메일 설정</h6>
	        </div>
	        <div class="card-body">
	        	<form id="admin" action="adminSetup.action" class="needs-validation" novalidate>
				<input type="hidden" name="property_id" value="SYSTEM_EMAIL"/>
				
				<div class="form-row">
					<div class="col-md-6 mb-3">
                        <label for="validationCustom01">아이디</label>
                        <input type="text" class="form-control" name="admin_email" id="admin_email" value="${SETUP.ADMIN_EMAIL}" placeholder="사용중인 네이버 아이디를 입력하세요." />
                        <div class="valid-feedback">
                            Looks good!
                        </div>
                    </div>
                    <div class="col-md-6 mb-3">
                        <label for="validationCustom02">비밀번호</label>
                        <input type="password" class="form-control" id="admin_email_pwd" name="admin_email_pwd" value="${SETUP.ADMIN_EMAIL_PWD}" placeholder="패스워드를 입력하세요." />
                        <div class="valid-feedback">
                            Looks good!
                        </div>
                    </div>
                </div>
				<button class="btn btn-primary" type="submit" onclick="jsSetupUpdate(); return false;">수정</button>
				<button class="btn btn-secondary" type="submit" onclick="jsSetupCancel(); return false;">복원</button>
				</form>
	        </div>
	        
	    </div>
	    <!-- #END# Basic Validation -->
        
	</div>
</div>
	
	
	
	<%-- 
	<div style="height: 30px;text-align: left;background-color: #e9ecf0;padding-left: 10px;line-height: 30px;font-weight: bold;color: #7b8598;">
		가입시 최초 레벨 설정
	</div>
	<div class="skin_join" style="padding: 20px 0;">
	<ul>
		<li>
			<em>레벨</em>
			<div><input type="text" maxlength="20" name="init_level" id="init_level" value="${SETUP.INIT_LEVEL}">
			</div>				        	
		</li>
	</ul>
	</div>
	--%>
</div>
	
</body>

</html>