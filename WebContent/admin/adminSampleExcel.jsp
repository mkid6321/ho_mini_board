<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="me" uri="/WEB-INF/CustomTag.tld" %>

<html>
<head>
<style>
	.no{width:40px;text-align:center;white-space:nowrap;}
	.user_id{width:40px;text-align:left;white-space:nowrap;}
	.user_name{width:40px; text-align:left;white-space:nowrap;}
	.description{width:260px; text-align:left;white-space:nowrap;}
	.email{width:90px; text-align:left;white-space:nowrap;}
	.user_level{width:40px; text-align:center;white-space:nowrap;}
	.del_flag{width:40px; text-align:center;white-space:nowrap;}
	.date{width:90px; text-align:left;white-space:nowrap;}
	
</style>
<script type="text/javascript" src="js/multifile.js"></script>
<script type="text/javascript">

$( window ).load(function() {
	$('#menu_setup > a').find(":last-child").removeClass("fa-angle-down");
	$('#menu_setup > a').find(":last-child").addClass("fa-angle-up");
	/* $('#menu_setup > ul').children().eq(1).css("background-color","#4d5565"); */
	$('#menu_setup > ul').children().eq(1).children().eq(0).css("color","#FFFFFF");
	$('#menu_setup > ul').show();
});

function jsSubmit() {
	$('#admin').attr('action', './adminSampleExcelInsert.action');
	$('#admin').submit();
}

function jsUploaded(src, contentType, length) {
	var new_row = document.createElement('div');
	new_row.style.cssText = 'display: inline-block;text-align:center;';

	// Delete button
	var new_row_button = document.createElement( 'input' );
	new_row_button.type = 'button';
	new_row_button.value = '삭제';
	
	var hidden_0 = document.createElement( 'input' );
	hidden_0.type = 'hidden';
	hidden_0.name = 'nic_filename'; 
	hidden_0.value = src;
	
	var hidden_2 = document.createElement( 'input' );
	hidden_2.type = 'hidden';
	hidden_2.name = 'nic_length'; 
	hidden_2.value = length;
	
	var hidden_1 = document.createElement( 'input' );
	hidden_1.type = 'hidden';
	hidden_1.name = 'nic_contentType'; 
	hidden_1.value = contentType;
	
	// References
	var element = document.createElement('img');
	element.style.cssText = 'width:50px;height:50px;margin:4px 1px;';
	element.src = src;
	
	new_row.appendChild(element);
	new_row.appendChild(document.createElement('br'));
	
	// Delete function
	new_row_button.onclick= function(){
		
		new_row.parentNode.removeChild(new_row);

		// Appease Safari
		//    without it Safari wants to reload the browser window
		//    which nixes your already queued uploads
		return false;
	};

	// Add button
	new_row.appendChild(new_row_button);
	
	// Add param
	new_row.appendChild(hidden_0);
	new_row.appendChild(hidden_1);
	new_row.appendChild(hidden_2);
}
</script>
</head>
<body>

<div class="container-fluid my-3">

<div class="d-flex row">
	<div class="col-md-7">
                     
	<div class="card mb-3 shadow no-b r-0">
	<form id="admin" action="adminSampleExcelInsert.action" method="post" enctype="multipart/form-data">
	<input type="hidden" id="file_type" name="file_type" value="excel"/> 
	
		<div class="card-header white">
	        <h6>엑셀 업로드</h6>
	    </div>
		<div class="card-body">
			<div class="row p-b-10" style="padding-left: 15px;">
				<input id="my_file_element" type="file" />
			</div>
			
			<div id="files_list" style="border:1px solid #DEDEDE;padding:5px;background:#fff;"><strong>Files (maximum 1):</strong></div>
		</div>
		
	</form>
	</div>
	
	<div style="text-align:right;">
		<a href="#" class="btn btn-primary mt-2" onclick="jsSubmit(); return false;">업로드</a>
	</div>
		
    </div>
</div> 

	
    <script>
		var multi_selector1 = new MultiSelector(document.getElementById('files_list'), 1, 'file');
		multi_selector1.addElement(document.getElementById('my_file_element'));
	</script>	
	
</div>
</body>


</html>