<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>  
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>  

<html>
<head>
<script type="text/javascript">

function jsUserEdit(){
	$('input').removeClass('empty');
	// 이름에 공백 체크
	if( $('#user_name').val().length < 1 ){
		alert('이름은 필수 입력사항입니다.');
		$('#user_name').addClass('empty');
		return false;
	} else {
		var blank_pattern = /[\s]/g;
		if( blank_pattern.test( $('#user_name').val() ) == true){
		    alert('이름에 공백은 사용할 수 없습니다. ');
		    return false;
		}
	}
	
	// 레벨에 숫자체크 
	if( $('#user_level').val().length < 1) {
		alert('레벨은 필수 입력 사항입니다.');
		$('#user_level').addClass('empty');
		return false;
	} else	if( !$.isNumeric( $('#user_level').val() )){
		alert('레벨은 숫자만 입력 가능합니다.');
		$('#user_level').addClass('empty');
		return false;
	} else if( $('#user_level').val() < 0 || $('#user_level').val() > 255) {
		alert('레벨은 0~255 숫자만 입력가능합니다.');
		$('#user_level').addClass('empty');
		return false;
	}
	
	
	// url 주소 체크 
	var chkExp = /http:\/\/([\w\-]+\.)+/g;
	if( chkExp.test( $('#homepage').val() ) != true && $('#homepage').val().length > 0){
		alert('url이 올바른 양식이 아닙니다.');	
		$('#homepage').addClass('empty');
		return false;
	}
	
	// email 체크
	var regExp = /[0-9a-zA-Z][_0-9a-zA-Z-]*@[_0-9a-zA-Z-]+(\.[_0-9a-zA-Z-]+){1,2}$/;
	//입력을 안했으면
	if($('#email').val().lenght == 0) { 
		alert('email은 필수입력사항입니다.');
		$('#email').addClass('empty');
		return false;	
	}
	//이메일 형식에 맞지않으면
	if (!$('#email').val().match(regExp)) { 
		alert('email을 형식이 맞지않습니다.');	
		$('#email').addClass('empty');
		return false;
	}
	
  $('#admin').attr('action','./adminUserUpdate.action');
  $('#admin').submit();
}
function jsUserCancel(){
	$('#admin').attr('action','./adminUserList.action');
	$('#admin').submit();
}

$( window ).load(function() {
	/* $('#menu_user').css("background-color","#4d5565"); */
	$('#menu_user > a').css("color","#FFFFFF");
});

</script>
</head>
<body>

<div class="container-fluid my-3">

<div class="d-flex row">
	<div class="col-md-7">
	
		<!-- Basic Validation -->
	    <div class="card mb-3 shadow no-b r-0">
	        <div class="card-header white">
	            <h6>BASIC VALIDATION</h6>
	        </div>
	        <div class="card-body">
	        
				<form id="admin" action="adminUserEdit.action" >
				<input type="hidden" id="nowPage" name="nowPage" value="${parameters.nowPage}"/>
				<input type="hidden" id="searchMethod" name="searchMethod" value="${parameters.searchMethod}"/>
				<input type="hidden" id="searchWord" name="searchWord" value="${parameters.searchWord}"/>
				
				
					<div class="form-row">
					
						<div class="col-sm-12 p-b-10">
	                        <label for="group_name">아이디</label>
	                        <input type="text" class="form-control" id="user_id" name="user_id" value="${USER_INFO.USER_ID}" readonly="readonly" />
	                    </div>
	                    <div class="col-sm-12 p-b-10">
	                        <label for="group_name">비밀번호</label>
	                        <input type="password" class="form-control" id="pwd" name="pwd" placeholder="8-17자 이내 입력. 영문(대/소문자 구분)/숫자" value="" required>
	                        <div class="valid-feedback">
	                            Looks good!
	                        </div>
	                    </div>
	                    <div class="col-sm-12 p-b-10">
	                        <label for="group_name">사용자 이름</label>
	                        <input type="text" class="form-control" id="user_name" name="user_name" placeholder="" value="${USER_INFO.USER_NAME}" required>
	                        <div class="valid-feedback">
	                            Looks good!
	                        </div>
	                    </div>
	                    <div class="col-sm-12 p-b-10">
	                        <label for="group_name">사용자 레벨</label>
	                        <input type="text" class="form-control" id="user_level" name="user_level" placeholder="" value="${USER_INFO.USER_LEVEL}" required>
	                        <div class="valid-feedback">
	                            Looks good!
	                        </div>
	                    </div>
	                    <div class="col-sm-12 p-b-10">
	                        <label for="group_name">홈페이지</label>
	                        <input type="text" class="form-control" id="homepage" name="homepage" placeholder="" value="${USER_INFO.HOMEPAGE}" required>
	                        <div class="valid-feedback">
	                            Looks good!
	                        </div>
	                    </div>
	                    <div class="col-sm-12 p-b-10">
	                        <label for="group_name">이메일</label>
	                        <input type="text" class="form-control" id="email" name="email" placeholder="" value="${USER_INFO.EMAIL}" required>
	                        <div class="valid-feedback">
	                            Looks good!
	                        </div>
	                    </div>
	                    <div class="col-sm-12 p-b-10">
	                        <label for="group_name">인사말/설명</label>
	                        <input type="text" class="form-control" id="description" name="description" placeholder="" value="${USER_INFO.DESCRIPTION}" required>
	                        <div class="valid-feedback">
	                            Looks good!
	                        </div>
	                    </div>
	                    <div class="col-sm-12 p-b-10">
	                        <label for="group_name">가입일</label>
	                        <input type="text" class="form-control" name="insert_date" id="insert_date" value="<fmt:formatDate value='${USER_INFO.INSERT_DATE}' pattern="yy.MM.dd hh:mm"/>" readonly="readonly">
	                    </div>
	                    <div class="col-sm-12 p-b-10">
	                        <label for="group_name">최종 수정일</label>
	                        <input type="text" class="form-control" name="update_date" id="update_date" value="<fmt:formatDate value='${USER_INFO.UPDATE_DATE}' pattern="yy.MM.dd hh:mm"/>" readonly="readonly">
	                    </div>
	                    <div class="col-sm-12 p-b-10">
	                        <label for="validationCustom01">사용여부</label>
	                        <div class="form-group">
	                             <select class="custom-select" id="del_flag" name="del_flag" required>
	                                 <option value="">선택해주세요.</option>
	                                 <option value="false" ${USER_INFO.DEL_FLAG == false?"selected":""}>사용</option>
	                                 <option value="true" ${USER_INFO.DEL_FLAG == true?"selected":""}>미사용</option>
	                             </select>
	                             <div class="invalid-feedback">Example invalid custom select feedback</div>
	                         </div>
	                    </div>
	                    
					</div>
				
				</form>
			</div>
			
		</div>
		
		<div style="text-align:right;">
			<a href="#" onclick="jsUserEdit(); return false;" class="btn btn-primary mt-2">수정</a> <a href="#" class="btn btn-secondary mt-2" onclick="jsUserCancel(); return false;">취소</a>
		</div>

	</div>
</div>

</div>
</body>

</html>