<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>  
<%@ taglib prefix="me" uri="/WEB-INF/CustomTag.tld" %>
  
<html>
<head>
	<script type="text/javascript">		
		
		function jsWrite(){
		  $('#admin').attr('action','./adminBulletinAdd.action');
		  $('#admin').submit();
		}
		
		function jsSearch(){
		  jsGoPage(1);
		}
		
		function jsGoPage(no){
		  $('#nowPage').val(no);
		  $('#admin').attr('action','./adminBulletinList.action');
		  $('#admin').submit();
		}		
		
		function jsLogin(){
		  location.replace("./articleLogin.jsp");	  
		}
		
		$( window ).load(function() {
			$('#menu_setup > a').find(":last-child").removeClass("fa-angle-down");
			$('#menu_setup > a').find(":last-child").addClass("fa-angle-up");
			/* $('#menu_setup > ul').children().eq(0).css("background-color","#4d5565"); */
			$('#menu_setup > ul').children().eq(0).children().eq(0).css("color","#FFFFFF");
			$('#menu_setup > ul').show();
		});
		
	</script>
</head>
<body>

<div class="container-fluid animatedParent animateOnce">
        <div class="tab-content my-3" id="v-pills-tabContent">
            <div class="tab-pane animated fadeInUpShort show active" id="v-pills-all" role="tabpanel" aria-labelledby="v-pills-all-tab">
            
<div class="row my-3">
	<div class="col-md-12">
	    <div class="card r-0 shadow">
	        <div class="table-responsive">

	<form action="adminPropertyList.action" id="admin" method="get">
	<input type="hidden" id="nowPage" name="nowPage" value="${parameters.nowPage }"/>
	<input type="hidden" id="user_id" name="user_id"/>
	 
	<table class="table table-striped table-hover r-0">
		<thead>
	    <tr class="no-b">
	         <th>프로퍼티 아이디</th>
	         <th>값</th>
	         <th>설명</th>
	    </tr>
	    </thead>
   		<tbody>
   		<c:forEach items="${PROPERTY_LIST}" var="list" varStatus="status">
		<tr onmouseover="this.style.backgroundColor='#FAFAFA'" onmouseout="this.style.backgroundColor=''">
			<td class="title">${list.PROPERTY_ID}</td>				
			<td class="value">${list.PROPERTY_VALUE}</td>
			<td class="description">${list.DESCRIPTION}</td>		
		</tr>
		</c:forEach>
   		</tbody>
   	</table>
   	
   	</form>
   	
   			</div>
   		</div>
   	</div>
</div>

<nav class="my-3" aria-label="Page navigation">
    ${PAGE}
</nav>
	
	<%-- <table>
		<tr>
			<td style="width:150px;text-align:left;"></td>
			<td style="width:340px;text-align:center;">${PAGE}</td>
			<td style="width:150px;text-align:right;padding-top:2px;"><a href="#" onclick="jsWrite(); return false;" class="button button_blue">Write</a></td>			
		</tr>	
	</table>
	
	<table>
		<tr>
			<td style="width:640px;text-align:center;padding-top:20px;">	
				<select name="searchMethod">
					<option value="property_id" ${parameters.searchMethod == "property_id"?"selected":""}>프로퍼티 아이디</option>
					<option value="property_value" ${parameters.searchMethod == "property_value"?"selected":""}>값</option>
					<option value="description" ${parameters.searchMethod == "description"?"selected":""}>설명</option>
				</select>
				<input type="text" size="24" name="searchWord" id="searchWord" placeholder=" 검색어" style="height:14px;" value="${parameters.searchWord}" />
				<a href="#" onclick="jsSearch(); return false;" class="button button_orange">Search</a>
			</td>		
		</tr>
	</table> --%>
	
		</div>
	</div>
</div>
</body>

</html>