<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="me" uri="/WEB-INF/CustomTag.tld" %>

<html>
<head>
	<script type="text/javascript">
	
	function jsWrite(){
	  $('#admin').attr('action','./adminUserAdd.action');
	  $('#admin').submit();
	}
	
	function jsEdit(userId){
	  $('#admin').attr('action','./adminUserEdit.action');
	  $('#user_id').val(userId);
	  $('#admin').submit();
	}
	
	function jsSearch(){
	  jsGoPage(1);
	}
	
	function jsGoPage(no){
	  $('#nowPage').val(no);
	  $('#admin').attr('action','./adminUserList.action');
	  $('#admin').submit();
	}
	
	function jsLogin(){
	  location.replace("./articleLogin.jsp");	  
	}
	
	$( window ).load(function() {
		/* $('#menu_user').css("background-color","#4d5565"); */
		$('#menu_user > a').css("color","#FFFFFF");
	});
	
	</script>
</head>
<body>
<div class="container-fluid animatedParent animateOnce">
	<div class="tab-content my-3" id="v-pills-tabContent">
        <div class="tab-pane animated fadeInUpShort show active" id="v-pills-all" role="tabpanel" aria-labelledby="v-pills-all-tab">
            
<div class="row my-3">
	<div class="col-md-12">
	    <div class="card r-0 shadow">
	        <div class="table-responsive">

	<form action="adminBulletinList.action" id="admin" method="get">
	<input type="hidden" id="nowPage" name="nowPage" value="${parameters.nowPage }"/>
	 
	<table class="table table-striped table-hover r-0">
		<thead>
	    <tr class="no-b">
	         <th style="width: 30px">
	             <div class="custom-control custom-checkbox">
	                 <input type="checkbox" id="checkedAll" class="custom-control-input"><label
	                     class="custom-control-label" for="checkedAll"></label>
	             </div>
	         </th>
	         <th>이름</th>
	         <th>아이디</th>
	         <th>설명</th>
	         <th>레벨</th>
	         <th>삭제여부</th>
	         <th>생성일자</th>
	         <th></th>
	    </tr>
	    </thead>
   		<tbody>
		
		<c:set var="num" value="${NUM}"/>
		<c:forEach items="${USER_LIST}" var="list" varStatus="status">
		<tr>
            <td>
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input checkSingle"
                           id="user_id_1" required><label
                        class="custom-control-label" for="user_id_1"></label>
                        <%-- ${num}<c:set var="num" value="${num-1}"/> --%>
                </div>
            </td>

            <td>
                <div class="avatar avatar-md mr-3 mt-1 float-left">
                    <span class="avatar-letter avatar-letter-a  avatar-md circle"></span>
                </div>
                <div>
                    <div>
                        <strong>${list.USER_NAME }</strong>
                    </div>
                    <small> ${list.EMAIL }</small>
                </div>
            </td>

            <td>${list.USER_ID }</td>
            <td>${list.DESCRIPTION }</td>

            <td>${list.USER_LEVEL }</td>
            <td>${list.DEL_FLAG }</td>
            <td><me:datestring date="${list.INSERT_DATE}" /></td>
            <td>
                <a href="#"><i class="icon-eye mr-3"></i></a>
                <a href="./adminUserEdit.action?user_id=${list.USER_ID}"><i class="icon-pencil"></i></a>
            </td>
        </tr>    
		</c:forEach>
		
		</tbody>
	</table>
	
	<%-- <table>
		<tr>
			<td style="width:640px;text-align:center;padding-top:20px;">
				<select id="searchMethod" required="required" name="searchMethod">
					<option value="user_id" ${parameters.searchMethod == 'user_id'?"selected":""}>사용자 아이디	</option>
					<option value="user_name" ${parameters.searchMethod == 'user_name'?"selected":""}>사용자 이름</option>
				</select>	
				<input type="text" size="24" name="searchWord" value="${parameters.searchWord}" id="searchWord" placeholder=" 검색어" style="height:14px;" />
				<a href="#" onclick="jsSearch(); return false;" class="button button_orange">Search</a>
			</td>		
		</tr>
	</table> --%>
	</form>
	
	
	  		</div>
	    </div>
	</div>
</div>

<nav class="my-3" aria-label="Page navigation">
    ${PAGE}
</nav>

		</div>
	</div>
</div>
	
</body>

</html>