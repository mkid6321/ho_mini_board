<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>  
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>  
<%@ taglib prefix="me" uri="/WEB-INF/CustomTag.tld" %>

<html>
<head>
<script type="text/javascript">

	var list = <me:jsonstring object="${MESSAGE_LIST}" />;

	function jsView(messageIdx) {
		
		if($("#empty_area" ).hasClass("d-none") == false) {
			$('#empty_area').addClass("d-none");
			$('#message_area').removeClass("d-none");
		}
		
		list.forEach(function(value, index) {
			if(value.MESSAGE_IDX == messageIdx) {
				
				$('#sender_name').text(value.USER_NAME);
				$('#sender_id').text(value.USER_ID);
				var d = new Date(value.INSERT_DATE);
				$('#send_date').text(d.customFormat( "#DDD# #DD#/#MM#/#YYYY#, #hh#:#mm# #AMPM#" ));
				$('#send_title').text(value.TITLE);
				$('#send_contents').text(value.CONTENTS);
				
				return;
			}
		});
	}

</script>
</head>
<body>

<div class="container-fluid animatedParent animateOnce p-0">
    <div class="animated fadeInUpShort">
        <div class="row no-gutters">
            <div class="col-md-3 white">
                <div class="sticky white">
                    <ul class="nav nav-tabs nav-material">
                        <li class="nav-item">
                            <a class="nav-link p-3 active show" id="w2--tab1" data-toggle="tab" href="#w2-tab1"><i class="icon icon-mail-envelope-closed s-18 text-success"></i> 안읽음</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link p-3 " id="w2--tab2" data-toggle="tab" href="#w2-tab2"><i class="icon icon-star yellow-text"></i> 중요</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link p-3" id="w2--tab3" data-toggle="tab" href="#w2-tab3"><i class="icon icon-list"></i> 전체</a>
                        </li>
                    </ul>
                </div>
                <div class="slimScroll">
                    <div class="tab-content" id="v-pills-tabContent">
                        <div class="tab-pane fade show active" id="w2-tab1" role="tabpanel"
                             aria-labelledby="w2-tab1">
                            <ul class="list-unstyled ">
                                <c:forEach items="${MESSAGE_LIST}" var="list" varStatus="status">
                                <li class="media p-3 b-b ${list.STATUS eq '0' ? 'light' : ''}" onclick="jsView('${list.MESSAGE_IDX}')">
                                    <img class="d-flex mr-3 height-50" src="assets/img/dummy/u2.png"
                                         alt="Generic placeholder image">
                                    <div class="media-body text-truncate">
                                        <small class="float-right">
                                            <span><me:datestring date="${list.INSERT_DATE}" /></span>
                                            <a href="#" class="mr-2 ml-2">
                                                <i class="icon-star-o "></i>
                                            </a>
                                        </small>
                                        <h6 class="mt-0 mb-1 font-weight-normal">${list.USER_ID}</h6>
                                        <span>${list.TITLE}</span>
                                        <br>
                                        <small>${list.CONTENTS}</small>
                                    </div>
                                </li>
                                </c:forEach>
                            </ul>
                        </div>
                        <div class="tab-pane fade" id="w2-tab2" role="tabpanel" aria-labelledby="w2-tab2">
                            <ul>
                                <li class="media p-3 b-b">
                                    <img class="d-flex mr-3 height-50" src="assets/img/dummy/u3.png"
                                         alt="Generic placeholder image">
                                    <div class="media-body text-truncate">
                                        <h6 class="mt-0 mb-1 font-weight-normal">Doe Joe</h6>
                                        <span>Message sent via your  Market </span>
                                        <br>
                                        <small>Congratulations! Your update to WeRock Multipurpose</small>
                                    </div>
                                </li>
                                <li class="media p-3 b-b">
                                    <img class="d-flex mr-3 height-50" src="assets/img/dummy/u4.png"
                                         alt="Generic placeholder image">
                                    <div class="media-body text-truncate">
                                        <h6 class="mt-0 mb-1 font-weight-normal">Doe Joe</h6>
                                        <span>Message sent via your  Market </span>
                                        <br>
                                        <small>Congratulations! Your update to WeRock Multipurpose</small>
                                    </div>
                                </li>
                                <li class="media p-3 b-b">
                                    <img class="d-flex mr-3 height-50" src="assets/img/dummy/u5.png"
                                         alt="Generic placeholder image">
                                    <div class="media-body text-truncate">
                                        <h6 class="mt-0 mb-1 font-weight-normal">Doe Joe</h6>
                                        <span>Message sent via your  Market </span>
                                        <br>
                                        <small>Congratulations! Your update to WeRock Multipurpose</small>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="tab-pane fade" id="w2-tab3" role="tabpanel" aria-labelledby="w2-tab3">
                            <ul>
                                <li class="media p-3 b-b">
                                    <img class="d-flex mr-3 height-50" src="assets/img/dummy/u9.png"
                                         alt="Generic placeholder image">
                                    <div class="media-body text-truncate">
                                        <h6 class="mt-0 mb-1 font-weight-normal">Doe Joe</h6>
                                        <span>Message sent via your  Market </span>
                                        <br>
                                        <small>Congratulations! Your update to WeRock Multipurpose</small>
                                    </div>
                                </li>
                                <li class="media p-3 b-b">
                                    <img class="d-flex mr-3 height-50" src="assets/img/dummy/u11.png"
                                         alt="Generic placeholder image">
                                    <div class="media-body text-truncate">
                                        <h6 class="mt-0 mb-1 font-weight-normal">Doe Joe</h6>
                                        <span>Message sent via your  Market </span>
                                        <br>
                                        <small>Congratulations! Your update to WeRock Multipurpose</small>
                                    </div>
                                </li>
                                <li class="media p-3 b-b">
                                    <img class="d-flex mr-3 height-50" src="assets/img/dummy/u10.png"
                                         alt="Generic placeholder image">
                                    <div class="media-body text-truncate">
                                        <h6 class="mt-0 mb-1 font-weight-normal">Doe Joe</h6>
                                        <span>Message sent via your  Market </span>
                                        <br>
                                        <small>Congratulations! Your update to WeRock Multipurpose</small>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-9 b-l">
                <div class="m-md-3">
                
                	<!--Message Start-->
                    <div class="card b-0 m-2" id="empty_area">
                        <div class="card-body ">
                            <div class="text-center p-5">
						        <i class="icon-note-important s-64 text-primary"></i>
						        <h4 class="my-3">No Contents Found</h4>
						        <p>You have not added any contents add first contents now</p>
						    </div>
                        </div>
                    </div>
                    <!--Message End-->
                        
                    <!--Message Start-->
                    <div class="card b-0 m-2 d-none" id="message_area">
                        <div class="card-body">
                            <div data-toggle="collapse" data-target="#message1">
                            	
                                <div class="media">
                                    <img class="d-flex mr-3 height-50" src="assets/img/dummy/u8.png"
                                         alt="Generic placeholder image">
                                    <div class="media-body">
                                        <h6 class="mt-0 mb-1 font-weight-normal"><span id="sender_name">Doe Joe</span> (<span id="sender_id">Doe Joe</span>)</h6>
                                        <span id="send_title">Message sent via your Market</span>
                                        <br>
                                        <small id="send_date">Mon 9/18/2017, 9:54 PM</small>
                                        <div class="collapse my-3 show" id="message1">
                                            <div id="send_contents">
                                                <p>Hello John,</p>
                                                <p>Post-ironic shabby chic VHS, Marfa keytar flannel lomo try-hard
                                                    keffiyeh cray. Actually fap
                                                    fanny
                                                    pack yr artisan trust fund. High Life dreamcatcher church-key
                                                    gentrify. Tumblr stumptown
                                                    four dollar
                                                    toast vinyl, cold-pressed try-hard blog authentic keffiyeh
                                                    Helvetica lo-fi tilde
                                                    Intelligentsia. Lomo
                                                    locavore salvia bespoke, twee fixie paleo cliche brunch Schlitz
                                                    blog McSweeney's messenger
                                                    bag swag
                                                    slow-carb. Odd Future photo booth pork belly, you probably
                                                    haven't heard of them actually
                                                    tofu ennui
                                                    keffiyeh lo-fi Truffaut health goth. Narwhal sustainable retro
                                                    disrupt.</p>
                                            </div>
                                            <div class="btn-group float-right">
                                                <a class="btn btn-outline-primary btn-xs" href="#modalCreateMessage"
                                                   data-toggle="modal"
                                                   data-target="#modalCreateMessage">전달</a>
                                                <a class="btn btn-outline-primary btn-xs" href="#modalCreateMessage"
                                                   data-toggle="modal"
                                                   data-target="#modalCreateMessage">답장</a>
                                                <a class="btn btn-outline-primary btn-xs" href="#modalCreateMessage"
                                                   data-toggle="modal"
                                                   data-target="#modalCreateMessage">중요</a>
                                                <a class="btn btn-outline-primary btn-xs" href="#modalCreateMessage"
                                                   data-toggle="modal"
                                                   data-target="#modalCreateMessage">삭제</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--Attachments Start-->
                        <!-- <div class="card-footer white">
                            <ul class="mailbox-attachments clearfix">
                                <li>
                                    <span class="mailbox-attachment-icon"><i
                                            class="icon-document-file-pdf text-danger"></i></span>
                                    <div class="mailbox-attachment-info">
                                        <a href="#" class="mailbox-attachment-name"><i class="icon-paperclip"></i>
                                            Sep2014-report.pdf</a>
                                        <a href="#" class="btn btn-success btn-xs float-right r-3"><i class="icon-cloud-download"></i></a>
                                        <span class="mailbox-attachment-size">1,245 KB
                                        </span>
                                    </div>
                                </li>
                                <li>
                                    <span class="mailbox-attachment-icon"><i
                                            class="icon-document-file-doc text-primary"></i></span>
                                    <div class="mailbox-attachment-info">
                                        <a href="#" class="mailbox-attachment-name"><i class="icon-paperclip"></i>
                                            App Description.docx</a>
                                        <a href="#" class="btn btn-warning btn-xs float-right r-3"><i class="icon-cloud-download"></i></a>
                                        <span class="mailbox-attachment-size">1,245 KB</span>
                                    </div>
                                </li>
                                <li>
                                    <span class="mailbox-attachment-icon has-img"><img src="assets/img/demo/portfolio/p1.jpg"
                                                                                       alt="Attachment"></span>
                                    <div class="mailbox-attachment-info">
                                        <a href="#" class="mailbox-attachment-name"><i class="icon-camera"></i>
                                            photo1.png</a>
                                        <a href="#" class="btn btn-primary btn-xs float-right r-3"><i class="icon-cloud-download"></i></a>
                                        <span class="mailbox-attachment-size">2.67 MB</span>
                                    </div>
                                </li>
                                <li>
                                    <span class="mailbox-attachment-icon has-img"><img src="assets/img/demo/portfolio/p2.jpg"
                                                                                       alt=""></span>
                                    <div class="mailbox-attachment-info">
                                        <a href="#" class="mailbox-attachment-name"><i class="icon-camera"></i>
                                            photo2.png</a>
                                        <a href="#" class="btn btn-danger btn-xs float-right r-3"><i class="icon-cloud-download"></i></a>
                                        <span class="mailbox-attachment-size">1.9 MB</span>
                                    </div>
                                </li>
                            </ul>
                        </div> -->
                        <!--Attachments End-->
                    </div>
                    <!--Message End-->

                </div>
            </div>
        </div>
    </div>
</div>

<a href="./adminMessageAdd.action" class="btn-fab btn-fab-md fab-right fab-right-bottom-fixed shadow btn-primary"><i class="icon-add"></i></a>

</body>

</html>