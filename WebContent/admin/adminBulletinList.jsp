<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>  
<%@ taglib prefix="me" uri="/WEB-INF/CustomTag.tld" %>
  
<html>
<head>
	<script type="text/javascript">		
		
		function jsWrite(){
		  $('#admin').attr('action','./adminBulletinAdd.action');
		  $('#admin').submit();
		}
		
		function jsSearch(){
		  jsGoPage(1);
		}
		
		function jsGoPage(no){
		  $('#nowPage').val(no);
		  $('#admin').attr('action','./adminBulletinList.action');
		  $('#admin').submit();
		}		
		
		function jsLogin(){
		  location.replace("./articleLogin.jsp");	  
		}
		
		$( window ).load(function() {
			$('#menu_bulletin > a').find(":last-child").removeClass("fa-angle-down");
			$('#menu_bulletin > a').find(":last-child").addClass("fa-angle-up");
			/* $('#menu_bulletin > ul').children().eq(1).css("background-color","#4d5565"); */
			$('#menu_bulletin > ul').children().eq(1).children().eq(0).css("color","#FFFFFF");
			$('#menu_bulletin > ul').show();
		});
		
	</script>
</head>
<body>
<div class="container-fluid animatedParent animateOnce">
        <div class="tab-content my-3" id="v-pills-tabContent">
            <div class="tab-pane animated fadeInUpShort show active" id="v-pills-all" role="tabpanel" aria-labelledby="v-pills-all-tab">
            
<div class="row my-3">
	<div class="col-md-12">
	    <div class="card r-0 shadow">
	        <div class="table-responsive">
	        
	      
	<form id="admin" action="adminBulletinList.action" >
	<input type="hidden" id="nowPage" name="nowPage" value="${parameters.nowPage}"/>
	
	
	<table class="table table-striped table-hover r-0">
		<thead>
        <tr class="no-b">
            <th style="width: 30px">
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" id="checkedAll" class="custom-control-input"><label
                        class="custom-control-label" for="checkedAll"></label>
                </div>
            </th>
            <th>번호</th>
            <th>게시판 이름</th>
            <th>등록글수</th>
            <th>접근레벨</th>
            <th>입력레벨</th>
            <th>사용여부</th>
            <th>생성일자</th>
            <th></th>
        </tr>
        </thead>
		
		<tbody>

		<c:forEach items="${BULLETIN_LIST}" var="list" varStatus="status">
        <tr>
            <td>
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input checkSingle"
                           id="user_id_1" required><label
                        class="custom-control-label" for="user_id_1"></label>
                </div>
            </td>
			<td>${list.GROUP_IDX}</td>
			<td><a href="./articleList.action?group_idx=${list.GROUP_IDX}">${list.GROUP_NAME}</a></td>				
			<td>${list.ARTICLE_CNT}</td>
			<td>${list.ACCESS_LEVEL}</td>		
			<td>${list.INSERT_LEVEL}</td>
			<td>${list.DEL_FLAG}</td>
			<td><me:datestring date="${list.INSERT_DATE}" /></td>
			<td>
                <a href="#"><i class="icon-eye mr-3"></i></a>
                <a href="./adminBulletinEdit.action?group_idx=${list.GROUP_IDX}"><i class="icon-pencil"></i></a>
            </td>
		</tr>
		</c:forEach>
		
		</tbody>
	</table>
	
	
	
	
	</form>
	
	  		</div>
	    </div>
	</div>
</div>

<nav class="my-3" aria-label="Page navigation">
    ${PAGE}
</nav>

		</div>
	</div>
</div>


<%-- <table>
		<tr>
			<td style="text-align:center;padding-top:20px;">	
				<select name="searchMethod">
					<option value="group_name" ${parameters.searchMethod == "group_name"?"selected":""}>게시판 이름</option>
				</select>
				<input type="text" size="24" name="searchWord" id="searchWord" placeholder=" 검색어" style="height:14px;" value="${parameters.searchWord}" />
				<a href="#" onclick="jsSearch(); return false;" class="button button_orange">Search</a>
			</td>		
		</tr>
	</table> --%>

<%-- <table>
		<tr>
			<td style="width:150px;text-align:left;"></td>
			<td style="text-align:center;">${PAGE}</td>
			<td style="width:150px;text-align:right;padding-top:2px;"><a href="#" onclick="jsWrite(); return false;" class="button button_blue">Write</a></td>			
		</tr>	
	</table> --%>
	
</body>

</html>