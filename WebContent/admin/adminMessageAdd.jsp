<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>  
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 

<html>
<head>
	<script type="text/javascript">
	
	function jsSetManager(userId){
		$('#receiver').val(userId);
	}
	
	function jsSubmit(){
		var receiver = $("#receiver").val();
		if(!receiver) {
			alert("받을 사람을 입력해주세요.");
			return;
		}
		
		var title = $("#title").val();
		if(!title) {
			alert("제목을 입력해주세요.");
			return;
		}
		
		var contents = $("#contents").val();
		if(!contents) {
			alert("내용을 입력해주세요.");	
			return;
		}
	  
	  	//$('form').submit();
	  	
	  	$.ajax({
            url:'./exclude/adminMessageInsert.action',
            type:'post',
            data:$('form').serialize(),
            success:function(data){
            	if(data == 'true') {
            		Chat.socket.send('to.'+receiver+' '+title);
            		
            		$("#receiver").val('');
            		$("#title").val('');
            		$('#contents').trumbowyg('empty');
            		
            		alert("메시지가 성공적으로 보내졌습니다.");
            	}
            	
            }
        })
	}
	
	$( window ).load(function() {
		/* $('#menu_user').css("background-color","#4d5565"); */
		$('#menu_user > a').css("color","#FFFFFF");
	});
	</script>
</head>
<body>

<div class="container-fluid animatedParent animateOnce p-0">
    <div class="animated fadeInUpShort">
        <div class="row no-gutters">
            <div class="col-md-3 white">
                <div class="sticky white">
                    <div class="card-header white ">
                         <form>
                             <div class="form-group has-right-icon m-0">
                                 <input class="form-control light r-30" placeholder="Search" type="text">
                                 <i class="icon-search"></i>
                             </div>
                         </form>
                     </div>
                </div>
                <div class="slimScroll">
                    <div class="tab-content" id="v-pills-tabContent">
                        <div class="tab-pane fade show active" id="w2-tab1" role="tabpanel"
                             aria-labelledby="w2-tab1">
                            <ul class="list-unstyled ">
                               <!-- Alphabet with number of contacts -->
	                           <!-- <li class="pt-3 pb-3 bg-light sticky">
	                               <strong>A</strong>
	                               <span class="badge r-3 badge-success float-right">20</span>
	                           </li> -->
							   <c:forEach items="${MANAGER_LIST}" var="list" varStatus="status">
	                           <!-- Single contact -->
	                           <li class="my-1">
	                               <div class="card no-b p-3">
	                                   <div class="" onclick="jsSetManager('${list.USER_ID}');">
	                                       <div class="float-right">
	                                           <a href="#" class="btn-fab btn-fab-sm btn-primary r-5">
	                                               <i class="icon-mail-envelope-closed2 p-0"></i>
	                                           </a>
	                                           <a href="#" class="btn-fab btn-fab-sm btn-success r-5">
	                                               <i class="icon-star p-0"></i>
	                                           </a>
	                                       </div>
	                                       <div class="image mr-3  float-left">
	                                           <img class="w-40px" src="assets/img/dummy/u1.png" alt="User Image">
	                                       </div>
	                                       <div>
	                                           <div>
	                                               <strong>${list.USER_NAME}</strong>
	                                           </div>
	                                           <small> ${list.EMAIL}</small>
	                                       </div>
	                                   </div>
	                               </div>
	                           </li>
	                           </c:forEach>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-9 b-l">
                <div class="m-md-3">
                    <!--Message Start-->
                    <div class="card b-0 m-2">
                            <form action="./adminMessageInsert.action" method="post" enctype="multipart/form-data">
		                       <div class="form-group has-icon m-0">
		                           <i class="icon-user-o"></i>
		                           <input class="form-control form-control-lg r-0 b-0" type="text"
		                                  name="receiver" id="receiver" placeholder="Recipient User ID">
		                       </div>
		                       <div class="b-b"></div>
		                       <div class="form-group has-icon m-0">
		                           <i class="icon-subject"></i>
		                           <input class="form-control form-control-lg r-0 b-0" type="text"
		                                  name="title" id="title" placeholder="제목">
		                       </div>
		                       <textarea class="editor" placeholder="Write Something..."
		                                 rows="17" name="contents" id="contents"></textarea>
		                       <!-- <div class="b-b"></div>
		                       <input hidden id="file" name="file" type="file"/>
		                       <div class="dropzone dropzone-file-area p-3 m-3 bg-light" id="fileUpload">
		                           <div class="dz-default dz-message">
		                               <span>Drop or Click to add Attachments </span>
		                               <div>You can also click to open file browser</div>
		                           </div>
		                       </div> -->
		                       <div class="card-footer">
		
		                           <button class="btn btn-primary l-s-1 s-12 text-uppercase" onclick="jsSubmit();return false;">
		                               Send Message
		                           </button>
		                       </div>
		                   </form>
                    </div>
                    <!--Message End-->

                </div>
            </div>
        </div>
    </div>
</div>

</body>

</html>