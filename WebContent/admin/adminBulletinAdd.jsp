<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>

<head>
	<style>
	.no{width:40px;text-align:center;}
	.title{width:140px;text-align:left;}
	.description{width:280px; text-align:left; }
	.del_flag{width:80px; text-align:center;}
	.action{width:60px; text-align:left;}
	</style>
	<script type="text/javascript">
	
	function jsBulletinList() {
		$('#admin').attr('action','./adminBulletinList.action');
		$('#admin').submit();
	}
	
	function jsBulletinInsert(){
		
		var adminGroupName = $.trim($('#admin_group_name').val());
		if(adminGroupName.length == 0 || adminGroupName.length < 5 || adminGroupName.length > 30 ){
			alert("입력값이 유효하지 않습니다.\n5자 이상 30자 이하로 입력해주세요.");
			return;
		}
		
		var adminDescription = $.trim($('#admin_description').val());
		if( adminDescription.length == 0 || adminDescription.length < 5 || adminDescription.length > 250){
			alert("입력값이 유효하지 않습니다.\n5자 이상 250자 이하로 입력해주세요.");
			return;
		}
		
		var accessLevel = $.trim($('#access_level').val());
		if(accessLevel.length == 0 || accessLevel < 0 || accessLevel > 255){
			alert("입력값의 범위가 유효하지 않습니다.\n0 이상 255 이하를 입력해주세요.");
			return;
		}
		
		var insertLevel = $.trim($('#insert_level').val());	  
		if(insertLevel.length == 0 || insertLevel < 0 || insertLevel > 255){
			alert("입력값의 범위가 유효하지 않습니다.\n0 이상 255 이하를 입력해주세요.");
			return;
		}
		
		$('#admin').attr('action','./adminBulletinInsert.action');
		$('#admin').submit();
	}
	function jsAddCategory(){
		var categoryListElement = $('#categoryList');
		
		var flag = $('#C_DEL_FLAG option:selected').val();
		console.log("! "+flag);
		var html ="<tr style=\"height:27px;background-color: #f9f9f9\" onmouseover=\"this.style.backgroundColor='#eeeeee'\" onmouseout=\"this.style.backgroundColor='#f9f9f9'\">";
			html+="<td class=\"no assort\">"+($('#categoryList tr').length+1)+"</td>";		
			html+="<td class=\"title\"><input type=\"text\" name=\"C_CATEGORY_NAME\" value=\""+$('#C_CATEGORY_NAME').val()+"\" /></td>";		
			html+="<td class=\"description\"><input type=\"text\" name=\"C_DESCRIPTION\" value=\""+$('#C_DESCRIPTION').val()+"\" style=\"width:100%;\"/></td>";	
			html+="<td class=\"del_flag\"><select name=\"C_DEL_FLAG\"><option value=\"true\" "+ ((flag=='true')?"selected":"")+">사용</option><option value=\"false\" "+((flag=='true')?"":"selected")+">미사용</option</select></td>";			
			html+="<td class=\"action\"><a href=\"#\" class=\"button button_orange\" style=\"width: 20px;\" onclick=\"jsMoveUp(this); return false;\">↑</a> <a href=\"#\" class=\"button_orange\" style=\"width: 20px;\" onclick=\"jsMoveDown(this); return false;\">↓</a></td>";
			html+="</tr>";
		
		categoryListElement.append(html);
		
		$('#C_CATEGORY_NAME').val('');
		$('#C_DESCRIPTION').val('');
		$('#C_DEL_FLAG option:eq(0)').attr("selected","selected");
	}
	
	function jsMoveUp(obj){
		var row = obj.parentNode.parentNode;
		var prevRow = row.previousElementSibling;
		
		if(prevRow == null) return false;
		
		row.parentNode.insertBefore(row,prevRow);
		
		jsRowNum();
	}
	function jsMoveDown(obj){
		var row = obj.parentNode.parentNode;
		var nextRow = row.nextElementSibling;
		
		if(nextRow == null) return false;
		
		row.parentNode.insertBefore(row,nextRow.nextElementSibling);
		
		jsRowNum();
	}
	function jsRowNum() {
		var eleCategory = document.getElementById("categoryList");
		var eleChild = eleCategory.getElementsByClassName("no");
		
		for(i=0; i<eleChild.length; i++) {
			eleChild[i].innerHTML = (i+1);	
		}
	}
	
	$( window ).load(function() {
		$('#menu_bulletin > a').find(":last-child").removeClass("fa-angle-down");
		$('#menu_bulletin > a').find(":last-child").addClass("fa-angle-up");
		/* $('#menu_bulletin > ul').children().eq(1).css("background-color","#4d5565"); */
		$('#menu_bulletin > ul').children().eq(1).children().eq(0).css("color","#FFFFFF");
		$('#menu_bulletin > ul').show();
	});
	
	</script>
</head>
<body>

<div class="container-fluid my-3">

<div class="d-flex row">
	<div class="col-md-7">
	
		<!-- Basic Validation -->
	    <div class="card mb-3 shadow no-b r-0">
	        <div class="card-header white">
	            <h6>BASIC VALIDATION</h6>
	        </div>
	        <div class="card-body">
	            <form class="needs-validation" novalidate id="admin" id="admin" action="adminBulletinInsert.action">
				<input type="hidden" id="group_idx" name="group_idx" value="${parameters.group_idx}"/>
				<input type="hidden" id="nowPage" name="nowPage" value="${parameters.nowPage}"/>
				<input type="hidden" id="searchMethod" name="searchMethod" value="${parameters.searchMethod}"/>
				<input type="hidden" id="searchWord" name="searchWord" value="${parameters.searchWord}"/>
	
	                <div class="form-row">
	                    <div class="col-sm-12 p-b-10">
	                        <label for="group_name">게시판 이름</label>
	                        <input type="text" class="form-control" id="group_name" name="group_name" placeholder="5~30자 사이 입력" value="" required>
	                        <div class="valid-feedback">
	                            Looks good!
	                        </div>
	                    </div>
	                    <div class="col-sm-12 p-b-10">
	                        <label for="description">게시판 설명</label>
	                        <input type="text" class="form-control" id="description" name="description" placeholder="5~250자 사이 입력" value="" required>
	                        <div class="valid-feedback">
	                            Looks good!
	                        </div>
	                    </div>
	                    <div class="col-sm-12 p-b-10">
	                        <label for="validationCustom01">접근 레벨</label>
	                        <input type="text" class="form-control" id="access_level" name="access_level" placeholder="0~255 사이 입력. 0은 모두, 255는 관리자만" value="0" required>
	                        <div class="valid-feedback">
	                            Looks good!
	                        </div>
	                    </div>
	                    <div class="col-sm-12 p-b-10">
	                        <label for="validationCustom01">입력 레벨</label>
	                        <input type="text" class="form-control" id="insert_level" name="insert_level" placeholder="0~255 사이 입력. 0은 모두, 255는 관리자만" value="0" required>
	                        <div class="valid-feedback">
	                            Looks good!
	                        </div>
	                    </div>
	                    <div class="col-sm-12 p-b-10">
	                        <label for="validationCustom01">사용여부</label>
	                        <div class="form-group">
	                             <select class="custom-select" id="del_flag" name="del_flag" required>
	                                 <option value="">선택해주세요.</option>
	                                 <option value="false">사용</option>
	                                 <option value="true">미사용</option>
	                             </select>
	                             <div class="invalid-feedback">Example invalid custom select feedback</div>
	                         </div>
	                    </div>
	                </div>
	                
	                <label>카테고리</label>
	                <table style="width:100%;">
						<tr style="height:27px;border: 1px solid #dedede;border-bottom: 0;" class="titlebar_bg">
							<td class="no assort">순서</td>
							<td class="title assort">이름</td>
							<td class="description assort">설명</td>
							<td class="del_flag assort">삭제여부</td>
							<td class="action assort"></td>
						</tr>
					</table>	
					<table id="categoryList" style="width:100%; border: 1px solid #dedede;">
					<c:forEach items="${CATEGORY_LIST}" var="list" varStatus="status">
						<tr style="height:27px;background-color: #f9f9f9" onmouseover="this.style.backgroundColor='#eeeeee'" onmouseout="this.style.backgroundColor='#f9f9f9'">
							<td class="no assort">${list.output_seq}</td>
							<td class="title"><input type="text" name="C_CATEGORY_NAME" value="${list.category_name}"/></td>		
							<td class="description"><input type="text" name="C_DESCRIPTION" value="${list.description}" style="width:100%;"/></td>		
							<td class="del_flag">
								<select name="C_DEL_FLAG"> 
									<option value="true" ${list.del_flag == "true"?"selected":""}>미사용</option>
									<option value="false" ${list.del_flag == "false"?"selected":""}>사용</option>
								</select>
							</td>
							<td class="action"><a href="#" class="button button_orange" style="width: 20px;" onclick="jsMoveUp(this); return false;">↑</a> <a href="#" class="button button_orange" style="width: 20px;" onclick="jsMoveDown(this); return false;">↓</a></td>						
						</tr>
					</c:forEach>
					</table>
					<table style="width:100%;">
						<tr style="height:27px;border: 1px solid #dedede;border-top: 0;" class="titlebar_bg">
							<td class="no assort">Last</td>
							<td class="title"><input type="text" id="C_CATEGORY_NAME" placeholder="Title"/></td>		
							<td class="description"><input type="text" id="C_DESCRIPTION" style="width: 100%" placeholder="Description"/></td>		
							<td class="del_flag">
								<select id="C_DEL_FLAG" name="C_DEL_FLAG" style="height:20px;">
									<option value="false" selected="selected">사용</option>
									<option value="true">미사용</option>
								</select>
							</td>
							<td class="action"><a href="#" class="button button_orange" onclick="jsAddCategory()">추가</a></td>
						</tr>
					</table>
	                
	                
	                
	        	</form>
			</div>
			
		</div>
		
		<div style="text-align:right;">
			<a href="#" class="btn btn-primary mt-2" onclick="jsBulletinInsert()">등록</a> <a href="#" class="btn btn-secondary mt-2" onclick="jsBulletinList()">취소</a>
		</div>
        
    </div>
</div> 

</div>
</body>

</html>