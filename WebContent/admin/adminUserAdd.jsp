<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>  

<html>
<head>
	<script type="text/javascript">
	
	function jsUserCancel(){
		$('#admin').attr('action','./adminUserList.action');
		$('#admin').submit();
	}
	
	var isCheckedId = false;
	
	function jsUserAdd(){
		// 빈값 넣었을때 css 제거 
		$("input").removeClass('empty');
		// 공백체크 정규식
		var blank_pattern = /[\s]/g;
		if( $('#user_id').val().length > 0 ){
			if( blank_pattern.test( $('#user_id').val() ) == true ){
				alert('아이디에는 공백이 들어갈 수 없습니다 . \n다시 입력해 주세요');
				return false;
			} else if( $('#user_id').val().search(/^[a-z0-9_]{5,16}$/)){
				alert('아이디에는 영문 숫자로 5~16자만 입력이 가능합니다. \n다시 입력해 주세요');
				return false;
			}
		} else {
			alert('아이디는 필수 입력사항입니다.');
			// 빈값 넣었을때 css 추가 
			$('#user_id').addClass('empty');
			return false;
		}
		
		// 비번을 넣었나 체크 
		if( $('#pwd').val().length >  0 ){
			if( blank_pattern.test( $('#pwd').val() ) == true ){
				alert('비밀번호에는 공백이 들어갈 수 없습니다 . \n다시 입력해 주세요');
				return false;
			} else if( $('#pwd').val().length < 8 || $('#pwd').val().length > 16 ){
				alert('비밀번호는  8~17자만 입력이 가능합니다. \n다시 입력해 주세요');
				return false;
			}
		} else {
			alert('비밀번호는 필수 입력사항입니다.');
			$('#pwd').addClass('empty');
			return false;
		}
		
		// 이름이 빈값인지 체크 
		if( $('#user_name').val().length > 0 ){
			// 이름에 공백 체크
			if( blank_pattern.test( $('#user_name').val() ) == true){
			    alert('이름에 공백은 사용할 수 없습니다. ');
			    return false;
			} 
		} else {
			alert('사용자이름은 필수 입력사항입니다.');
			$('#user_name').addClass('empty');
			return false;
		}
		
		// url 주소 체크 
		var chkExp = /http:\/\/([\w\-]+\.)+/g;
		if( $('#homepage').val().length > 0 && chkExp.test( $('#homepage').val() ) != true ){
			alert('홈페이지 url이 올바른 형식이 아닙니다.');	
			return false;
		}
		
		if( $('#email').val().length > 0 ){
			if(!emailcheck( $('#email').val())) {alert("이메일을 확인해주세요.");return;}
	  	} else {
			alert('이메일은 필수 입력사항입니다.');
		  	$('#email').addClass('empty');
		  	return false;
	  	}
	  
	  	if(!isCheckedId) {alert("아이디 중복 체크를 해주세요.");return;}
	  
	  	$('#admin').attr('action','./adminUserInsert.action');
	  	$('#admin').submit();
	}
	
	function jsCheckId() {
		
	  if( $.trim($("#user_id").val()).length == 0 ){alert("아이디를 입력해주세요.");return;}
	  
	  $.ajax({
	         url:'./exclude/articleCheckId.action?user_id='+$.trim($("#user_id").val()),
	         type:'post',
	         success:function(data){
	         	if(eval(data)){alert('이미 존재하는  아이디입니다.'); isCheckedId = false; $('#user_id').val(''); return;}else{ isCheckedId = true; alert("사용할 수 있습니다. ")}
	         }
	   })
	}
				
	function emailcheck(strValue){
		var regExp = /[0-9a-zA-Z][_0-9a-zA-Z-]*@[_0-9a-zA-Z-]+(\.[_0-9a-zA-Z-]+){1,2}$/;
		//입력을 안했으면
		if(strValue.lenght == 0) {	return false;	}
		//이메일 형식에 맞지않으면
		if (!strValue.match(regExp)) {	return false;	}
		return true;
	}	
	
	$( window ).load(function() {
		/* $('#menu_user').css("background-color","#4d5565"); */
		$('#menu_user > a').css("color","#FFFFFF");
	});
	</script>
</head>
<body>

<div class="container-fluid my-3">

<div class="d-flex row">
	<div class="col-md-7">
	
		<!-- Basic Validation -->
	    <div class="card mb-3 shadow no-b r-0">
	        <div class="card-header white">
	            <h6>BASIC VALIDATION</h6>
	        </div>
	        <div class="card-body">
	            <form class="needs-validation" novalidate id="admin" id="admin" action="./adminUserInsert.action">
				<input type="hidden" id="nowPage" name="nowPage" value="${parameters.nowPage}"/>
				<input type="hidden" id="searchMethod" name="searchMethod" value="${parameters.searchMethod}"/>
				<input type="hidden" id="searchWord" name="searchWord" value="${parameters.searchWord}"/>
				
					<div class="form-row">
					
	                    <div class="col-sm-12 p-b-10">
	                        <label for="group_name">아이디</label>
	                        <div class="input-group">
	                          <input type="text" class="form-control" id="user_id" name="user_id" placeholder="영문/숫자만 입력가능, 5-16자 이내 입력, 단 첫글자는 영문" value="" aria-describedby="button-addon2" required>
							  <div class="input-group-append">
							    <button class="btn btn-outline-secondary" type="button" id="button-addon2" onclick="jsCheckId(); return false;">ID CHECK</button>
							  </div>
							</div>
	                        <div class="valid-feedback">
	                            Looks good!
	                        </div>
	                    </div>
	                    <div class="col-sm-12 p-b-10">
	                        <label for="group_name">비밀번호</label>
	                        <input type="password" class="form-control" id="pwd" name="pwd" placeholder="8-17자 이내 입력. 영문(대/소문자 구분)/숫자" value="" required>
	                        <div class="valid-feedback">
	                            Looks good!
	                        </div>
	                    </div>
	                    <div class="col-sm-12 p-b-10">
	                        <label for="group_name">사용자 이름</label>
	                        <input type="text" class="form-control" id="user_name" name="user_name" placeholder="" value="" required>
	                        <div class="valid-feedback">
	                            Looks good!
	                        </div>
	                    </div>
	                    <div class="col-sm-12 p-b-10">
	                        <label for="group_name">사용자 레벨</label>
	                        <input type="text" class="form-control" id="user_level" name="user_level" placeholder="" value="" required>
	                        <div class="valid-feedback">
	                            Looks good!
	                        </div>
	                    </div>
	                    <div class="col-sm-12 p-b-10">
	                        <label for="group_name">홈페이지</label>
	                        <input type="text" class="form-control" id="homepage" name="homepage" placeholder="" value="" required>
	                        <div class="valid-feedback">
	                            Looks good!
	                        </div>
	                    </div>
	                    <div class="col-sm-12 p-b-10">
	                        <label for="group_name">이메일</label>
	                        <input type="text" class="form-control" id="email" name="email" placeholder="" value="" required>
	                        <div class="valid-feedback">
	                            Looks good!
	                        </div>
	                    </div>
	                    <div class="col-sm-12 p-b-10">
	                        <label for="group_name">인사말/설명</label>
	                        <input type="text" class="form-control" id="description" name="description" placeholder="" value="" required>
	                        <div class="valid-feedback">
	                            Looks good!
	                        </div>
	                    </div>
	                    <div class="col-sm-12 p-b-10">
	                        <label for="validationCustom01">사용여부</label>
	                        <div class="form-group">
	                             <select class="custom-select" id="del_flag" name="del_flag" required>
	                                 <option value="">선택해주세요.</option>
	                                 <option value="false">사용</option>
	                                 <option value="true">미사용</option>
	                             </select>
	                             <div class="invalid-feedback">Example invalid custom select feedback</div>
	                         </div>
	                    </div>
					</div>
				</form>
			</div>
			
		</div>
		
		<div style="text-align:right;">
			<a href="#" onclick="jsUserAdd(); return false;" class="btn btn-primary mt-2">등록</a> <a href="#" class="btn btn-secondary mt-2" onclick="jsUserCancel(); return false;">취소</a>
		</div>

	</div>
</div>

</div>
</body>

</html>