<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>

<head>
	<script type="text/javascript" src="js/multifile.js"></script>
	<script type="text/javascript" src="js/nicEdit.js"></script>
	<script type="text/javascript">
	
		function jsReply(){
		  if( $.trim($("#title").val()).length == 0 ){alert("제목을 입력해주세요.");return;}
		  var nicE = new nicEditors.findEditor('editor');
		  $('#editor').val(nicE.getContent());
		  if( $.trim($("#editor").val()).length == 0 ){alert("내용을 입력해주세요.");return;}
		       
		  $('#blog').attr('action','./articleReplyInsert.action');
		  $('#blog').submit();
		}
					
	</script>
</head>
<body>
	<form id="blog" action="articleReplyInsert.action" method="post" enctype="multipart/form-data">
	<input type="hidden" id="group_idx" name="group_idx" value="${parameters.group_idx}"/>
	<input type="hidden" id="article_idx" name="article_idx" value="${parameters.article_idx}"/>
	<input type="hidden" id="nowPage" name="nowPage" value="${parameters.nowPage}"/>
	<input type="hidden" id="searchMethod" name="searchMethod" value="${parameters.searchMethod}"/>
	<input type="hidden" id="searchWord" name="searchWord" value="${parameters.searchWord}"/>
	<input type="hidden" name="origin_idx" value="${BLOG_VIEW.ORIGIN_IDX}"/>
	<input type="hidden" name="depth" value="${BLOG_VIEW.DEPTH}"/>
	<input type="hidden" name="reply_order" value="${BLOG_VIEW.REPLY_ORDER}"/>
	
	<table>	
	<tr>	
		<td align="left" style="font-size:x-small;">
			<input type="text" id="title" name="title" value="re: ${BLOG_VIEW.TITLE}" style="width:598px;height:20px;"/>
		</td>				        	
	</tr>
	<tr>	
		<td align="left" style="padding:4px 0;">
			<c:if test="${CATEGORY_LIST.size() > 0}">
				<label style="font-size:xx-small;color:#444;font-weight:bold;">카테고리</label>
				<select name="category_idx">
				<c:forEach var="list" items="${CATEGORY_LIST}" varStatus="status">
					<option value="${list.CATEGORY_IDX}" ${BLOG_VIEW.CATEGORY_IDX == list.CATEGORY_IDX ? 'selected="selected"' : '' }>${list.CATEGORY_NAME}</option>
				</c:forEach>
				</select> 
			</c:if>
			<c:if test="${session.SESSION_USER_INFO.USER_LEVEL == 255}">
				<label style="font-size:xx-small;color:#444;font-weight:bold;">공지사항</label> 
				<select id="notice" name="notice">
					<option value="true" ${BLOG_VIEW.NOTICE_FLAG == 'true'?"selected":""}>공지</option>
					<option value="false" ${BLOG_VIEW.NOTICE_FLAG == 'false'?"selected":""}>일반</option>
				</select>	
			</c:if>
			<c:if test="${session.SESSION_USER_INFO.USER_LEVEL != 255}">
				<input type="hidden" name="notice" value="false"/>
			</c:if>
			<label style="font-size:xx-small;color:#444;font-weight:bold;">접근레벨</label> 
			<select name="inquery_level" >
				<option value="0">모두</option>
				<option value="${session.SESSION_USER_INFO.USER_LEVEL}">${session.SESSION_USER_INFO.USER_LEVEL} 이상</option>
				<option value="255">관리자만</option>
			</select>
		</td>
	</tr>
	<tr>
		<td align="left">
			<textarea id="editor" name="contents" style="width:600px;height:300px;">
				<br/>
				<br/>
				<br/>
				<p style="color:gray;">
				===================================================================<br/>
				${BLOG_VIEW.CONTENTS}
				</p>
			</textarea>
		</td>				        	
	</tr>
	<tr>
		<td align="left">
			<br/>
			<input id="my_file_element" type="file" /><br/><br/>
			<div id="files_list" style="border:1px solid #DEDEDE;padding:5px;background:#fff;font-size:x-small;"><strong>Files (maximum 2):</strong></div>
		</td>
	</tr>
	<tr style="height:24px;">
		<td align="right"><a href="#" onclick="jsBack(); return false;" class="button button_orange">Cancel</a> <a href="#" onclick="jsReply(); return false;" class="button button_blue">Reply</a></td>
	</tr>		        
	</table>
	
	</form>	
	<script>
		bkLib.onDomLoaded(function() {
			new nicEditor({fullPanel : true}).panelInstance('editor');	
		});
	</script>	
	<script>
		var multi_selector1 = new MultiSelector( document.getElementById( 'files_list' ), 2, 'file' );
		multi_selector1.addElement( document.getElementById( 'my_file_element' ) );
	</script>			
</body>

</html>