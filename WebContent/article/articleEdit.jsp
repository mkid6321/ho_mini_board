<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
 
<html>

<head>
	<script type="text/javascript" src="js/multifile.js"></script>
	<script type="text/javascript" src="js/nicEdit.js"></script>
	<script type="text/javascript">	
		
		function jsUpdate(){
		  if( $.trim($("#title").val()).length == 0 ){alert("제목을 입력해주세요.");return;} 
		  var nicE = new nicEditors.findEditor('editor');
		  $('#editor').val(nicE.getContent());
		  if( $.trim($("#editor").val()).length == 0 ){alert("내용을 입력해주세요.");return;}
			
		  $('#blog').attr('action', './articleUpdate.action');
		  $('#blog').submit();
		}
		
		var src = '';
		var imano = '';
		function jsFileDelete(file_idx, filename){
			
			var new_element = document.createElement( 'input' );
			new_element.type = 'hidden';
			new_element.name = 'delete_file';
			new_element.value = file_idx;
			
			var list_target = document.getElementById( 'images_list' );
			list_target.appendChild(new_element);
			
			document.getElementById(file_idx).style.display="none"; 
			
			if(filename.trim().length > 0) {
			  
				var nicE = new nicEditors.findEditor('editor');
			  	var content = nicE.getContent();
				
			  	var target = "<img src=\""+filename+"\"";
				
			  	var startIndex = content.indexOf(target);
			  	var endIndex = content.indexOf(">", startIndex);
				
			  	var real = content.substring(startIndex, endIndex+1);
	
			  	nicE.setContent(content.replace(real, ""));
			}
		}
		
		function jsFileResult(req){
		  if( eval(req.responseText) ){ 
			  document.getElementById(imano).style.display="none"; alert("삭제되었습니다.");
			  
			  if(src.length > 0) {
				  var nicE = new nicEditors.findEditor('editor');
				  var content = nicE.getContent();
					
				  var target = "<img src=\""+src+"\"";
					
				  var startIndex = content.indexOf(target);
				  var endIndex = content.indexOf(">", startIndex);
					
				  var real = content.substring(startIndex, endIndex+1);
	
				  nicE.setContent(content.replace(real, ""));
			  }
		  	  return;
		  }else{	alert("삭제하지 못했습니다. 관리자에게 문의하세요."); }		
		}
				
		function jsUploaded(src, contentType, length) {
			var new_row = document.createElement('div');
			new_row.style.cssText = 'display: inline-block;text-align:center;';

			// Delete button
			var new_row_button = document.createElement( 'input' );
			new_row_button.type = 'button';
			new_row_button.value = '삭제';
			
			var hidden_0 = document.createElement( 'input' );
			hidden_0.type = 'hidden';
			hidden_0.name = 'nic_filename'; 
			hidden_0.value = src;
			
			var hidden_2 = document.createElement( 'input' );
			hidden_2.type = 'hidden';
			hidden_2.name = 'nic_length'; 
			hidden_2.value = length;
			
			var hidden_1 = document.createElement( 'input' );
			hidden_1.type = 'hidden';
			hidden_1.name = 'nic_contentType'; 
			hidden_1.value = contentType;
			
			// References
			var element = document.createElement('img');
			element.style.cssText = 'width:50px;height:50px;margin:4px 1px;';
			element.src = src;
			
			new_row.appendChild(element);
			new_row.appendChild(document.createElement('br'));

			// Delete function
			new_row_button.onclick= function(){
				
				var nicE = new nicEditors.findEditor('editor');
				var content = nicE.getContent();
				
				var target = "<img src=\""+src+"\"";
				
				var startIndex = content.indexOf(target);
				var endIndex = content.indexOf(">", startIndex);
				
				var real = content.substring(startIndex, endIndex+1);

				nicE.setContent(content.replace(real, ""));
				
				new_row.parentNode.removeChild(new_row);

				// Appease Safari
				//    without it Safari wants to reload the browser window
				//    which nixes your already queued uploads
				return false;
			};

			// Add button
			new_row.appendChild(new_row_button);
			
			// Add param
			new_row.appendChild(hidden_0);
			new_row.appendChild(hidden_1);
			new_row.appendChild(hidden_2);

			// Add it to the list
			var list_target = document.getElementById( 'images_list' );
			list_target.appendChild(new_row);
 		}
	</script>
</head>
<body>
	<form id="blog" action="articleUpdate.action" method="post" enctype="multipart/form-data">
	<input type="hidden" id="group_idx" name="group_idx" value="${parameters.group_idx}"/>
	<input type="hidden" id="article_idx" name="article_idx" value="${parameters.article_idx}"/>
	<input type="hidden" id="nowPage" name="nowPage" value="${parameters.nowPage}"/>
	<input type="hidden" id="searchMethod" name="searchMethod" value="${parameters.searchMethod}"/>
	<input type="hidden" id="searchWord" name="searchWord" value="${parameters.searchWord}"/>
	
	<table>
		<tr>	
			<td align="left">
				<input type="text" id="title" name="title" value="${BLOG_VIEW.TITLE}" style="width:598px;height:20px;"/>
			</td>				        	
		</tr>
		<tr>	
			<td align="left" style="padding:4px 0;">
				<c:if test="${CATEGORY_LIST.size() > 0}">
					<label style="font-size:xx-small;color:#444;font-weight:bold;">카테고리</label> 
					<select name="category_idx">
					<c:forEach var="list" items="${CATEGORY_LIST}" varStatus="status">
						<option value="${list.CATEGORY_IDX}" ${BLOG_VIEW.CATEGORY_IDX == list.CATEGORY_IDX ? 'selected="selected"' : '' }>${list.CATEGORY_NAME}</option>
					</c:forEach>
					</select> 
				</c:if>
				<c:if test="${session.SESSION_USER_INFO.USER_LEVEL == 255}">
					<label style="font-size:xx-small;color:#444;font-weight:bold;">공지사항</label> 
					<select id="notice" name="notice">
						<option value="true" ${BLOG_VIEW.NOTICE_FLAG == 'true'?"selected":""}>공지</option>
						<option value="false" ${BLOG_VIEW.NOTICE_FLAG == 'false'?"selected":""}>일반</option>
					</select>	
				</c:if>
				<c:if test="${session.SESSION_USER_INFO.USER_LEVEL != 255}">
					<input type="hidden" name="notice" value="false"/>
				</c:if>
				<label style="font-size:xx-small;color:#444;font-weight:bold;">접근레벨</label> 
				<select name="inquery_level" >
					<option value="0" ${BLOG_VIEW.INQUERY_LEVEL == '0'?"selected":""}>모두</option>
					<option value="${session.SESSION_USER_INFO.USER_LEVEL}" ${BLOG_VIEW.INQUERY_LEVEL == session.SESSION_USER_INFO.USER_LEVEL?"selected":""}>${session.SESSION_USER_INFO.USER_LEVEL} 이상</option>
					<option value="255" ${BLOG_VIEW.INQUERY_LEVEL == '255'?"selected":""}>관리자만</option>
				</select>
			</td>		        	
		</tr>
		<tr>	
			<td align="left">
				<textarea id="editor" name="contents" style="width:600px;height:300px;">
				${BLOG_VIEW.CONTENTS} 
				</textarea>
			</td>				        	
		</tr>
		<tr>
			<td>
				<br/>
				<div id="now_images_list" style="border:1px solid #DEDEDE;padding:5px;background:#fff;font-size:x-small;text-align:left;">
					<strong>저장된 이미지</strong><br/>
					<c:forEach items="${BLOG_VIEW.FILELIST}" var="list">
					<c:if test="${list.INPUT_TYPE == 'nicedit'}">
					<div id='${list.FILE_IDX}' style="display: inline-block;text-align:center;" >
						<img src="./upload/${list.FILENAME}" style="width:50px;height:50px;margin:4px 1px;"/><br/>
						<a href="#" class="button button_white" onclick="jsFileDelete('${list.FILE_IDX}','./upload/${list.FILENAME}');">삭제</a>
					</div>
					</c:if>
					</c:forEach>
					<c:if test="${BLOG_VIEW.FILELIST.size() == 0}"><span style="margin-left : 10px;">저장된 이미지가 없습니다.</span></c:if>
				</div>
				<br/>
				<div id="now_files_list" style="border:1px solid #DEDEDE;padding:5px;background:#fff;font-size:x-small;text-align:left;"><strong>저장된 파일</strong>
				<c:forEach items="${BLOG_VIEW.FILELIST}" var="list">
				<c:if test="${list.INPUT_TYPE != 'nicedit'}">
					<c:url var="download" value="fileDownload.action">
						<c:param name="filepath" value="${list.FILEPATH}" />
						<c:param name="filename" value="${list.FILENAME}" />			
					</c:url>
					<div id='${list.FILE_IDX}' style="font-size:x-small;text-align:left;">파일명 : <a href="${download}">${list.FILENAME}</a> 파일크기 : ${list.FILESIZE} byte <a href="#" class="button button_white" onclick="jsFileDelete('${list.FILE_IDX}','');">삭제</a></div>
				</c:if>
				</c:forEach>
				<c:if test="${BLOG_VIEW.FILELIST.size() == 0}"><span style="margin-left : 10px;"> 저장된 파일이 없습니다.</span></c:if>
				</div>	
				<br/>
			</td>
		</tr>
		<tr>
			<td style="background-image:url(images/dot.gif);height:1px"></td></tr>
		<tr>	
			<td align="left">
				<br/>
				<div id="images_list" style="border:1px solid #DEDEDE;padding:5px;background:#fff;font-size:x-small;"><strong>Images</strong><br/></div>
			</td>
		</tr>
		<tr>
			<td align="left">
				<br/>
				<input id="my_file_element" type="file" /><br/><br/>
				<div id="files_list" style="border:1px solid #DEDEDE;padding:5px;background:#fff;font-size:x-small;"><strong>Files (maximum 2):</strong></div>
			</td>
		</tr>
	</table>
	
	<table style="width:600px;">
		<tr style="height:24px;">
			<td align="right">
				<a href="#" onclick="jsBack(); return false;" class="button button_orange">Cancel</a> <a href="#" onclick="jsUpdate(); return false;" class="button button_blue">Edit</a>
			</td>
		</tr>			        
	</table>
		
	</form>	
	<script>
		bkLib.onDomLoaded(function() {
			new nicEditor({fullPanel : true}).panelInstance('editor');	
		});
	</script>
	<script>
		var multi_selector1 = new MultiSelector( document.getElementById( 'files_list' ), 2, 'file' );
		multi_selector1.addElement( document.getElementById( 'my_file_element' ) );
	</script>					
</body>

</html>