<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="me" uri="/WEB-INF/CustomTag.tld" %>

<html>

<head>	
	<style>
	.no{width:40px;text-align:center;}
	.category{width:60px;text-align:left;}
	.title{width:340px;text-align:left;}
	.id{width:70px; text-align:right;}
	.cnt{width:40px; text-align:center;}
	.date{width:90px; text-align:left;}
	
	</style>
	<script type="text/javascript">
	
		function jsAlert(){
		  alert("선택하신 글을 읽으실 권한이 없습니다.");
		}
		
		function jsView(no){
		  $('#article_idx').val(no);
		  $('#blog').attr('action', './articleView.action');
		  $('#blog').submit();
		}
		
		function jsWrite(){
		  $('#blog').attr('action', './articleWrite.action');
		  $('#blog').submit();
		}
		
		function jsType(type){
		  $('#type').val(type);
		  $('#blog').attr('action', './articleList.action');
		  $('#blog').submit();
		}
		
		function jsSearch(){
		  jsGoPage(1);
		}
		
		function jsGoPage(no){
		  $('#nowPage').val(no);
		  $('#blog').attr('action', './articleList.action');
		  $('#blog').submit();
		}
		
		function jsLogin(){
		  $('#blog').attr('action', './articleLogin.action');
		  $('#blog').submit();
		}
		
		function jsJoin(){
		  $('#blog').attr('action', './articleJoin.action');
		  $('#blog').submit();
		}
		
		function jsLogout(){
		  $('#blog').attr('action', './articleLogout.action');
		  $('#blog').submit();
		}
		
		function jsEdit(){
		  $('#blog').attr('action', './articleUserInfo.action');
		  $('#blog').submit();
		}
	
	</script>
</head>
<body>
	<form id="blog" action="articleList.action" method="get">
	<input type="hidden" id="group_idx" name="group_idx" value="${parameters.group_idx}"/>
	<input type="hidden" id="article_idx" name="article_idx" value="${parameters.article_idx}"/>
	<input type="hidden" id="nowPage" name="nowPage" value="${parameters.nowPage}"/>
	<input type="hidden" id="type" name="type" value="${parameters.type}"/>
		
	<table>
		<col class="no" /><col class="category" /><col class="title" /><col class="id" /><col class="cnt" /><col class="date" />					
		<tr style="height:27px;background-image:url('images/hr_bg.png');background-repeat:repeat-x;background-position:bottom">
			<td colspan="6">
				<div style="float:left;margin:5px 5px 8px 0;">
					<a href="#" onclick="jsType('list'); return false;"><img src="images/typeList.gif"/></a>
					<a href="#" onclick="jsType('grid'); return false;"><img src="images/typeGrid.gif"/></a>
				</div>
				
				<div style="float:left;">
					<div style="float:left;height:27px;width:8px;background-image:url('images/tab_active_left.png');background-repeat:no-repeat;"></div>
					<div style="float:left;height:27px;position:relative;background-image:url('images/tab_active.png');background-repeat:repeat-x;"><div style="margin:9px 9px 0 9px;font-size:x-small;font-weight:bold;">${session.SESSION_BULLETIN_INFO.GROUP_NAME}</div></div>
					<div style="float:left;height:27px;width:8px;background-image:url('images/tab_active_right.png');background-repeat:no-repeat;"></div>
				</div>
				
				<!-- <div style="float:left;">
					<div style="float:left;height:27px;width:8px;background-image:url('images/tab_left.png');background-repeat:no-repeat;"></div>
					<div style="float:left;height:27px;position:relative;background-image:url('images/tab_bg.png');background-repeat:repeat-x;"><div style="margin:9px 9px 0 9px;font-size:x-small;font-weight:bold;">HOT</div></div>
					<div style="float:left;height:27px;width:8px;background-image:url('images/tab_right.png');background-repeat:no-repeat;"></div>
				</div> -->
				
				<div style="float:right;">
					<c:if test="${session.SESSION_USER_INFO == null}"><a href="#" onclick="jsJoin(); return false;" class="button button_green">Join</a> <a href="#" onclick="jsLogin(); return false;" class="button button_red">Login</a></c:if>
					<c:if test="${session.SESSION_USER_INFO != null}">
						<c:if test="${session.SESSION_USER_INFO.USER_LEVEL == 255}"><a href="./adminBulletinList.action" class="button button_sky">Admin</a></c:if>
						<a href="#" onclick="jsLogout(); return false;" class="button button_red">Logout</a>
						<a href="#" onclick="jsEdit(); return false;" class="button button_green">Edit</a>
					</c:if>
				</div>
			</td>
		</tr>
		
		<c:if test="${parameters.type == 'grid'}">
		
		<tr style="" >
			<td colspan="6">
				<div style="text-align:left;padding:7px 0;">
					<c:forEach items="${BLOG_LIST}" var="list" varStatus="status">
						<div style="display:inline-block; *zoom:1; *display:inline; vertical-align:top; height:220px; width:29.2%; border: 1px solid #e1e8ed; border-radius: 4px; background: #fbfbfb; padding: 10px; margin:5px 0;" onmouseover="this.style.backgroundColor='#eeeeee'" onmouseout="this.style.backgroundColor=''">
							<c:if test="${session.SESSION_USER_INFO.USER_LEVEL < list.INQUERY_LEVEL}">
							<a href="#" onclick="jsAlert();" ><img style="width:100%;height:140px; background: #dedede url('upload/${list.COVER_IMAGE}') no-repeat center center" /></a><br/><br/>
							</c:if>
							<c:if test="${list.INQUERY_LEVEL == 0 || session.SESSION_USER_INFO.USER_LEVEL >= list.INQUERY_LEVEL}">
							<a href="#" onclick="jsView('${list.ARTICLE_IDX}')" ><img style="width:100%;height:140px; background: #dedede url('upload/${list.COVER_IMAGE}') no-repeat center center" /></a><br/><br/>
							</c:if>
							<span style="color: #444;font-size: 16px;line-height: 1.2;font-weight: 700;">${list.TITLE}</span><br/><br/>
							<me:datestring date="${list.INSERT_DATE}" /> ${list.USER_ID} ${list.READ_CNT}
						</div>
					</c:forEach>
				</div>
				
			</td>						
		</tr>
		
		</c:if>
		<c:if test="${parameters.type != 'grid'}">
	
		<c:forEach items="${BLOG_NLIST}" var="list" varStatus="status">
		<tr style="height:27px;background-color: #f9f9f9" onmouseover="this.style.backgroundColor='#eeeeee'" onmouseout="this.style.backgroundColor='#f9f9f9'">
			<td class="assort no">#</td>
			<td class="assort category">${list.CATEGORY_NAME}</td>			
			<td class="assort title"><span style="color: #000;padding-left:${list.DEPTH}0px"><c:if test="${list.DEPTH != 0}"><img src="images/reply_head.gif"/></c:if> <span onclick="jsView('${list.ARTICLE_IDX}')" class="hand"> ${list.TITLE}</span> <c:if test="${list.COMMENT_CNT > 0}"><span style="font-size:0.8em;color:#fc3693;">˚${list.COMMENT_CNT}</span></c:if> <c:if test="${list.COVER_IMAGE != null}"><img src="images/image.gif"/></c:if> <c:if test="${list.FILE_CNT > 0}"><img src="images/file.gif"/></c:if></span></td>
			<td class="assort id"><span style="color:#317cdb;">${list.USER_ID}</span></td>		
			<td class="assort cnt">${list.READ_CNT}</td>
			<td class="assort date"><me:datestring date="${list.INSERT_DATE}" /></td>						
		</tr>
		<tr><td colspan="6" style="background-image:url(images/dot.gif);height:1px"></td></tr>
		</c:forEach>
		<c:set var="num" value="${NUM}"/>
		<c:forEach items="${BLOG_LIST}" var="list" varStatus="status">
		<tr style="height:27px;" onmouseover="this.style.backgroundColor='#eeeeee'" onmouseout="this.style.backgroundColor=''">
			<td class="assort no">${num}<c:set var="num" value="${num-1}"/></td>
			<td class="assort category">${list.CATEGORY_NAME}</td>
			<c:if test="${session.SESSION_USER_INFO.USER_LEVEL < list.INQUERY_LEVEL}">
			<td class="assort title"><span style="color: #000;padding-left:${list.DEPTH}0px"><c:if test="${list.DEPTH != 0}"><img src="images/reply_head.gif"/></c:if> <span onclick="jsAlert()" class="hand"> ${list.TITLE}</span> <c:if test="${list.COMMENT_CNT > 0}"><span style="font-size:0.8em;color:#fc3693;">˚${list.COMMENT_CNT}</span></c:if> <c:if test="${list.COVER_IMAGE != null}"><img src="images/image.gif"/></c:if> <c:if test="${list.FILE_CNT > 0}"><img src="images/file.gif"/></c:if></span></td>			
			</c:if>
			<c:if test="${list.INQUERY_LEVEL == 0 || session.SESSION_USER_INFO.USER_LEVEL >= list.INQUERY_LEVEL}">
			<td class="assort title" style="padding-top:6px;"><div style="float:left;max-width:330px;height:18px;text-overflow: ellipsis; white-space: nowrap; overflow: hidden;"><span style="color: #000;padding-left:${list.DEPTH}0px;padding-right:4px;"><c:if test="${list.DEPTH != 0}"><img src="images/reply_head.gif"/></c:if> <span onclick="jsView('${list.ARTICLE_IDX}')" class="hand"> ${list.TITLE} </span></span></div><div><c:if test="${list.COMMENT_CNT > 0}"><span style="font-size:0.8em;color:#fc3693;">˚${list.COMMENT_CNT}</span></c:if> <c:if test="${list.COVER_IMAGE != null}"><img src="images/image.gif"/></c:if> <c:if test="${list.FILE_CNT > 0}"><img src="images/file.gif"/></c:if></div></td>
			</c:if>
			<td class="assort id"><span style="color:#317cdb;">${list.USER_ID}</span></td>		
			<td class="assort cnt">${list.READ_CNT}</td>
			<td class="assort date"><me:datestring date="${list.INSERT_DATE}" /></td>						
		</tr>
		<tr><td colspan="6" style="background-image:url(images/dot.gif);height:1px"></td></tr>
		</c:forEach>
		
		</c:if>
		
		<tr style="background-image:url('images/hr_bg.png');height:7px"><td colspan="6"></td></tr>
	</table>
	
	<table style="width:640px;">
		<tr>
			<td style="width:150px" align="left"></td>
			<td style="width:340px" align="center">${PAGE}</td>
			<td style="width:150px;padding-top:2px;" align="right"><c:if test="${session.SESSION_USER_INFO.USER_LEVEL >= session.SESSION_BULLETIN_INFO.INSERT_LEVEL}"><a href="#" onclick="jsWrite(); return false;" class="button button_blue">Write</a></c:if></td>			
		</tr>	
	</table>
	
	<br/>
	
	<table style="width:640px;">	
		<tr>
			<td style="width:100%;" align="center">	
				<select id="searchMethod" required="required" name="searchMethod">
					<option value="user_id" ${parameters.searchMethod == 'user_id'?"selected":""}>사용자 아이디	</option>
					<option value="title" ${parameters.searchMethod == 'title'?"selected":""}>제목</option>
				</select>
				<input type="text" size="24" name="searchWord" id="searchWord" value="${parameters.searchWord}" placeholder=" 검색어" style="height:17.5px;"/>
				<a href="#" onclick="jsSearch(); return false;" class="button button_orange">Search</a>
			</td>		
		</tr>
	</table>	

	</form>
</body>

</html>