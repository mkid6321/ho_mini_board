<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>

<head>
	<script type="text/javascript">	
		
		function jsFindUserId(){  
			
			if(!emailcheck( $.trim($("#email").val()) )) {alert("이메일을 확인해주세요.");return;}
			$('#blog').attr('action','./articleFindUserIdOn.action');
			$('#blog').submit();
		}
		
		function emailcheck(strValue){
			//입력을 안했으면
			if(strValue.length == 0) {	return false;	}
			
			//이메일 형식에 맞지않으면
			var regExp = /[0-9a-zA-Z][_0-9a-zA-Z-]*@[_0-9a-zA-Z-]+(\.[_0-9a-zA-Z-]+){1,2}$/;
			if (!strValue.match(regExp)) {	return false;	}
			return true;
		}
	</script>
</head>
<body style="width:100%;text-align:center;margin-top:100px;">

	<form id="blog" action="articleFindUserIdOn">
	
	<table style="width:190px;margin:0 auto;">		
		<tr>
			<td style="text-align:left;width:134px;">
				<input type="text" name="email" id="email" tabindex="1" placeholder="Email" style="border:solid 1px #999999;width:100%;height:20px;" />
			</td>
			<td align="right">
				<a href="#" onclick="jsFindUserId(); return false;" tabindex="3" class="button button_orange" style="width: 50px;height:40px;line-height:40px;">찾기</a>
			</td>				        	
		</tr>
	</table>
	</form>
	
	<!-- 서버로 부터 반환받은 context의 값을 select하는 방법 -->
	
	<table style="width:190px;margin:0 auto;">
		<tr>
			<td style="height:50px;color:red;">
				<c:if test="${USER_ID != null}">
				${USER_ID}
				</c:if>
				<c:if test="${USER_ID == null}">
				없는 사용자 입니다.
				</c:if>
			</td>
		</tr>
	</table>
	
</body>
</html>