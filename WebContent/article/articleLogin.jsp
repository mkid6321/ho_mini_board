<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>

<head>
	<script type="text/javascript">	
	
		function jsLogin(){  
			
			var userIdLength = $.trim($("#user_id").val()).length;
			var pwdLength = $.trim($("#pwd").val()).length;
			
			if(userIdLength < 5 || userIdLength > 16){
				alert("유효한 아이디가 아닙니다.\n5글자 이상 16글자 이하로 입력해주세요");
				return;
			}else if(pwdLength < 8 || pwdLength > 16){
				alert("유효한 패스워드가 아닙니다.\n8글자 이상 16글자 이하로 입력해주세요");
				return;
			}
			
			$('#blog').attr('action','./articleLogon.action');
			$('#blog').submit();
		}
		
		function jsFindUserId(){
			//Form과 상관없이 URL만 던져주는 방법
		  	location.replace("./articleFindUserId.action");	  
		}
		
		function jsFindUserPassword(){
			location.replace("./articleFindUserPassword.action");
		}
		
	</script>
</head>
<body style="width:100%;text-align:center;margin-top:100px;">

	<form id="blog" action="./articleLogon.action">
	<input type="hidden" id="group_idx" name="group_idx" value="${parameters.group_idx}"/>
	<table style="width:190px;margin:0 auto;">		
		<tr>
			<td style="text-align:left;width:134px;">
				<input type="text" name="user_id" id="user_id" tabindex="1" placeholder=" id" style="border:solid 1px #999999;width:100%;height:20px;" />
			</td>
			<td rowspan="2" align="right">
				<a href="#" onclick="jsLogin(); return false;" tabindex="3" class="button button_orange" style="width: 50px;height:40px;line-height:40px;">Login</a>
			</td>				        	
		</tr>
		<tr>
			<td style="text-align:left;width:134px;">
				<input type="password" name="pwd" id="pwd" tabindex="2" placeholder=" password" style="border:solid 1px #999999;width:100%;height:20px;" onkeyup="if(event.keyCode==13){jsLogin();}" />
			</td>				        	
		</tr>
	</table>
	
	<div style="width:196px;height:18px;margin:0 auto;"><img src="./images/line_dot.png" style="vertical-align:bottom;"/></div>	
	
	<span style="color:#227aa7" class="hand" onclick="jsFindUserId();">아이디</span> 또는 
	<span style="color:#227aa7" class="hand" onclick="jsFindUserPassword();">비밀번호</span> 찾기 
	</form>
	
	<c:if test="${LOGIN_FAIL == 'true'}">
	<table style="width:190px;margin:0 auto;">
		<tr><td style="height:50px;color:red;"></td></tr>
	</table>
	</c:if>
	<table style=" width:190px;margin:0 auto;">
		<tr>
		<br>
			<td style="height:50px;color:red;">
				<span class ="hand" onClick="jsBack();"> 메인 페이지로 돌아가기</span> 
			</td>
		</tr>
	</table>
</body>
</html>