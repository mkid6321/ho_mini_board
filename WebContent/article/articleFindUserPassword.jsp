<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>

<head>
	<script type="text/javascript">	
		
		function jsFindUserPassword(){  
			
			var userIdLength = $.trim($("#user_id").val()).length;
			if(userIdLength < 5 || userIdLength > 16){
				alert("유효한 아이디가 아닙니다.\n5글자 이상 16글자 이하로 입력해주세요");
				return;
			}
			
			if(!emailcheck( $.trim($("#email").val()) )) {alert("이메일을 확인해주세요.");return;}
			$('#blog').attr('action','./articleFindUserPasswordOn.action');
			$('#blog').submit();
		}
		
		function emailcheck(strValue){
			//입력을 안했으면
			if(strValue.length == 0) {	return false;	}
			
			//이메일 형식에 맞지않으면
			var regExp = /[0-9a-zA-Z][_0-9a-zA-Z-]*@[_0-9a-zA-Z-]+(\.[_0-9a-zA-Z-]+){1,2}$/;
			if (!strValue.match(regExp)) {	return false;	}
			return true;
		}
	</script>
</head>
<body style="width:100%;text-align:center;margin-top:100px;">

	<form id="blog" action="articleFindUserPasswordOn">
	
	<table style="width:190px;margin:0 auto;">		
		<tr>
			<td style="text-align:left;width:134px;">
				<input type="text" name="user_id" id="user_id" tabindex="1" placeholder=" id" style="border:solid 1px #999999;width:100%;height:20px;" />
			</td>
			<td rowspan="2" align="right">
				<a href="#" onclick="jsFindUserPassword(); return false;" tabindex="3" class="button button_orange" style="width: 50px;height:40px;line-height:40px;">Find</a>
			</td>				        	
		</tr>
		<tr>
			<td style="text-align:left;width:134px;">
				<input type="text" name="email" id="email" tabindex="1" placeholder="Email" style="border:solid 1px #999999;width:100%;height:20px;" onkeyup="if(event.keyCode==13){jsLogin();}"/>
			</td>				        	
		</tr>
	</table>
	
	<div style="width:196px;height:18px;margin:0 auto;"><img src="./images/line_dot.png" style="vertical-align:bottom;"/></div>	
	
	</form>
	
	<table style="width:190px;margin:0 auto;">
		<tr>
			<td style="height:50px;color:red;">
				<c:if test="${USER_PASSWORD != null}">
				${USER_PASSWORD}
				</c:if>
				<c:if test="${USER_PASSWORD == null}">
				없는 사용자 입니다.
				</c:if>
			</td>
		</tr>
	</table>
	
</body>
</html>