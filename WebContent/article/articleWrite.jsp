<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>

<head>
	<script type="text/javascript" src="js/multifile.js"></script>
	<script type="text/javascript" src="js/nicEdit.js"></script>
	<script type="text/javascript">
	
		function jsInsert(){
		  if( $.trim($("#title").val()).length == 0 ){alert("제목을 입력해주세요.");return;} 
		  var nicE = new nicEditors.findEditor('editor');
		  $('#editor').val(nicE.getContent());
		  if( $.trim($("#editor").val()).length == 0 ){alert("내용을 입력해주세요.");return;}
			
		  $('#blog').attr('action', './articleInsert.action');
		  $('#blog').submit();
		}
		
		function jsUploaded(src, contentType, length) {
			var new_row = document.createElement('div');
			new_row.style.cssText = 'display: inline-block;text-align:center;';

			// Delete button
			var new_row_button = document.createElement( 'input' );
			new_row_button.type = 'button';
			new_row_button.value = '삭제';
			
			var hidden_0 = document.createElement( 'input' );
			hidden_0.type = 'hidden';
			hidden_0.name = 'nic_filename'; 
			hidden_0.value = src;
			
			var hidden_2 = document.createElement( 'input' );
			hidden_2.type = 'hidden';
			hidden_2.name = 'nic_length'; 
			hidden_2.value = length;
			
			var hidden_1 = document.createElement( 'input' );
			hidden_1.type = 'hidden';
			hidden_1.name = 'nic_contentType'; 
			hidden_1.value = contentType;
			
			// References
			var element = document.createElement('img');
			element.style.cssText = 'width:50px;height:50px;margin:4px 1px;';
			element.src = src;
			
			new_row.appendChild(element);
			new_row.appendChild(document.createElement('br'));
			
			// Delete function
			new_row_button.onclick= function(){
				
				var nicE = new nicEditors.findEditor('editor');
				var content = nicE.getContent();
				
				var target = "<img src=\""+src+"\"";
				
				var startIndex = content.indexOf(target);
				var endIndex = content.indexOf(">", startIndex);
				
				var real = content.substring(startIndex, endIndex+1);

				nicE.setContent(content.replace(real, ""));
				
				new_row.parentNode.removeChild(new_row);

				// Appease Safari
				//    without it Safari wants to reload the browser window
				//    which nixes your already queued uploads
				return false;
			};

			// Add button
			new_row.appendChild(new_row_button);
			
			// Add param
			new_row.appendChild(hidden_0);
			new_row.appendChild(hidden_1);
			new_row.appendChild(hidden_2);

			// Add it to the list
			var list_target = document.getElementById( 'images_list' );
			list_target.appendChild(new_row);
 		}
		
	</script>
</head>
<body>
	<form id="blog" action="articleInsert.action" method="post" enctype="multipart/form-data">
	<input type="hidden" id="group_idx" name="group_idx" value="${parameters.group_idx}"/>
	<input type="hidden" id="article_idx" name="article_idx" value="${parameters.article_idx}"/>
	<input type="hidden" id="nowPage" name="nowPage" value="${parameters.nowPage}"/>
	<input type="hidden" id="searchMethod" name="searchMethod" value="${parameters.searchMethod}"/>
	<input type="hidden" id="searchWord" name="searchWord" value="${parameters.searchWord}"/>
	
	<table style="width:600px">
	<tr>
		<td align="left" style="font-size:x-small;font-weight:bold;">
			<input type="text" id="title" name="title" style="width:598px;height:20px;"/>
		</td>				        	
	</tr>
	<tr>
		<td align="left" style="padding:4px 0;">
			<c:if test="${CATEGORY_LIST.size() > 0}">
				<label style="font-size:xx-small;color:#444;font-weight:bold;">카테고리</label> 
				<select name="category_idx">
				<c:forEach var="list" items="${CATEGORY_LIST}" varStatus="status">
					<option value="${list.CATEGORY_IDX}" <c:if test="${status.first }">selected</c:if>>${list.CATEGORY_NAME}</option>
				</c:forEach>
				</select> 
			</c:if>
			<c:if test="${session.SESSION_USER_INFO.USER_LEVEL == 255}">
				<label style="font-size:xx-small;color:#444;font-weight:bold;">공지사항</label> 
				<select id="notice" name="notice">
					<option value="false" selected>일반</option>
					<option value="true">공지</option>
				</select>	
			</c:if>
			<c:if test="${session.SESSION_USER_INFO.USER_LEVEL != 255}">
				<input type="hidden" name="notice" value="false"/>
			</c:if>
			<label style="font-size:xx-small;color:#444;font-weight:bold;">접근레벨</label> 
			<select name="inquery_level" >
				<option value="0" selected>모두</option>
				<option value="${session.SESSION_USER_INFO.USER_LEVEL}">${session.SESSION_USER_INFO.USER_LEVEL} 이상</option>
				<option value="255">관리자만</option>
			</select> 
		</td>
	</tr>
	<tr>	
		<td align="left"><textarea id="editor" name="contents" style="width:600px;height:300px;"></textarea></td>				        	
	</tr>
	<tr>	
		<td align="left">
			<br/>
			<div id="images_list" style="border:1px solid #DEDEDE;padding:5px;background:#fff;font-size:x-small;"><strong>Images</strong><br/></div>
		</td>
	</tr>
	<tr>	
		<td align="left">
			<br/>
			<input id="my_file_element" type="file" /><br/><br/>
			<div id="files_list" style="border:1px solid #DEDEDE;padding:5px;background:#fff;font-size:x-small;"><strong>Files (maximum 2):</strong></div>
		</td>
	</tr>
	<tr style="height:24px;">
		<td align="right">
			<a href="#" onclick="jsBack(); return false;" class="button button_orange">Cancel</a> <a href="#" onclick="jsInsert(); return false;" class="button button_blue">Write</a>
		</td>
	</tr>				        
	</table>
	
	</form>	
	<script>
		bkLib.onDomLoaded(function() {
			new nicEditor({fullPanel : true}).panelInstance('editor');	
		});
	</script>
	<script>
		var multi_selector1 = new MultiSelector( document.getElementById( 'files_list' ), 2, 'file' );
		multi_selector1.addElement( document.getElementById( 'my_file_element' ) );
	</script>			
</body>

</html>