<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="me" uri="/WEB-INF/CustomTag.tld" %>

<html>

<head>		
	<script type="text/javascript">
	
		function jsEdit(){
			$('#blogView').attr('action', './articleEdit.action');
			$('#blogView').submit();
		}
		function jsDelete(){
			if (confirm("정말 삭제하시겠습니까??") == true){    //확인
				$('#blogView').attr('action', './articleDelete.action');
				$('#blogView').submit();
			}else{   //취소
			    return;
			}
		}
		function jsReply(){
			$('#blogView').attr('action', './articleReply.action');
			$('#blogView').submit();
		}
		function jsList(){ 
			$('#blogView').attr('action', './articleList.action');
			$('#blogView').submit();
		}
		function jsView(){
			$('#blogView').attr('action', './articleView.action');
			$('#blogView').submit();
		}
		
		function jsCommentReplyInsert(idx){
			if( $.trim($('#cmtTextarea'+idx).val()).length == 0 ){alert("코멘트를 입력해주세요.");return;}

			$('#articleComment'+idx).attr('action','./articleCommentReplyInsert.action');
			$('#articleComment'+idx).submit();
		}
		function jsCommentReply(idx){	
			var commentContentElement = $('#comment'+idx);
		
			commentContentElement.append("<br/><textarea id='cmtTextarea"+idx+"' name='commentContents' rows='3' style='width:80%'></textarea>"+
											  "<span onclick='jsCommentReplyInsert("+idx+")' style='cursor:pointer'> 댓글 </span>"+
											  "<span onclick='jsView()' style='cursor:pointer'> 취소 </span>");
		}

		function jsCommentInsert(){
			if( $.trim($('#comment').val()).length == 0 ){alert("코멘트를 입력해주세요.");return;}
		
			$('#articleCommentWrite').attr('action','./articleCommentInsert.action');
		  	$('#articleCommentWrite').submit();
		}
		
		function jsCommentUpdateEdit(idx){
			var commentContentElement = $('#comment'+idx);
			var originContent = $.trim(commentContentElement.html());
		
			commentContentElement.html("<textarea id='cmtTextarea"+idx+"' name='commentContents' rows='3' style='width:80%'>"+originContent+"</textarea>"+
									   "<span onclick='jsCommentUpdate("+idx+")' style='cursor:pointer'> 수정 </span>"+
									   "<span onclick='jsView()' style='cursor:pointer'> 취소 </span>");		
		}
		
		function jsCommentUpdate(idx){
			$('#articleComment'+idx).attr('action', './articleCommentUpdate.action');
			$('#articleComment'+idx).submit();
		}
		
		function jsCommentDelete(idx){	
			if (confirm("정말 삭제하시겠습니까??") == true){    //확인
				$('#articleComment'+idx).attr('action', './articleCommentDelete.action');
				$('#articleComment'+idx).submit();
			}else{   //취소
			    return;
			}
		}
		
	</script>
</head>
<body>
	<form id="blogView" action="articleList.action" method="get">
	<input type="hidden" id="group_idx" name="group_idx" value="${parameters.group_idx}"/>
	<input type="hidden" id="article_idx" name="article_idx" value="${parameters.article_idx}"/>
	<input type="hidden" id="nowPage" name="nowPage" value="${parameters.nowPage}"/>
	<input type="hidden" id="searchMethod" name="searchMethod" value="${parameters.searchMethod}"/>
	<input type="hidden" id="searchWord" name="searchWord" value="${parameters.searchWord}"/>
	
	<table style="width:600px">
	<tr>	
		<td align="left">
			<div >
				${BLOG_VIEW.TITLE}   
				<span class="assort" style="padding:0 5px;"><me:datestring date="${BLOG_VIEW.INSERT_DATE}" /></span>     
				<span class="assort" style="color:#317cdb;">${BLOG_VIEW.USER_ID}</span>
			</div>
			<div style="padding: 2px 0">
				<a href="#" onclick="jsList(); return false;" class="button button_orange">List</a>
			  <c:if test="${session.SESSION_USER_INFO.USER_LEVEL >= session.SESSION_BULLETIN_INFO.INSERT_LEVEL}">			
				<a href="#" onclick="jsReply(); return false;" class="button button_blue">Reply</a>
			  </c:if>
			  <c:if test="${session.SESSION_USER_INFO.USER_ID == BLOG_VIEW.USER_ID}">
				<a href="#" onclick="jsEdit(); return false;" class="button button_blue">Edit</a>		
				<a href="#" onclick="jsDelete(); return false;" class="button button_red">Delete</a>
			  </c:if>
			</div>
		</td>	        	
	</tr>
	<tr>	
		<td align="left" valign="top">
			<div style="border:1px solid #DEDEDE;padding:5px;background:#fff;font-size:x-small;text-align:left;min-height: 300px;">
				${BLOG_VIEW.CONTENTS}
			</div>
		</td>				        	
	</tr>
	</table>
	<c:if test="${BLOG_VIEW.FILELIST.size() > 0}">
	<table style="width:600px">
	<tr>
		<td>
		<br/>
		<div id="files_list" style="border:1px solid #DEDEDE;padding:5px;background:#fff;font-size:x-small;text-align:left;"><strong>Files</strong>
		<c:forEach items="${BLOG_VIEW.FILELIST}" var="list">
		<c:if test="${INPUT_TYPE != 'nicedit'}">
			<c:url var="download" value="fileDownload.action">
				<c:param name="filepath" value="${list.FILEPATH}" />
				<c:param name="filename" value="${list.FILENAME}" />			
			</c:url>
			<div id='${list.FILE_IDX}' style="font-size:x-small;text-align:left;">파일명 : <a href="${download}">${list.FILENAME}</a> 파일크기 : ${list.FILESIZE} byte </div>
		</c:if>
		</c:forEach>
		</div>	
		</td>
	</tr>
	</table>
	</c:if>
	</form>
	<c:if test="${BLOG_COMMENT_LIST.size() > 0}">
	<br/>
	<table style="width:600px">
	<tr style="background-image:url('images/hr_bg.png');height:7px;"><td></td></tr>
	<c:forEach items="${BLOG_COMMENT_LIST}" var="list" varStatus="status">
	<tr>	
		<td align="left">
			<form id="articleComment${list.COMMENT_IDX}">
			<input type="hidden" id="c_idx_${list.COMMENT_IDX}" name="comment_idx" value="${list.COMMENT_IDX}" />
			<input type="hidden" id="group_idx" name="group_idx" value="${parameters.group_idx}"/>
			<input type="hidden" id="article_idx" name="article_idx" value="${parameters.article_idx}"/>
			<input type="hidden" id="nowPage" name="nowPage" value="${parameters.nowPage}"/>
			<input type="hidden" id="searchMethod" name="searchMethod" value="${parameters.searchMethod}"/>
			<input type="hidden" id="searchWord" name="searchWord" value="${parameters.searchWord}"/>
			<div style="border:0px solid #dedede;padding:4px;margin-left:${list.DEPTH}0px">
				<div>
					<c:if test="${list.DEPTH != 0}"><img src="images/reply_head.gif"/></c:if>
					<span class="assort" style="color:#317cdb;padding:0 5px 0 0;">${list.USER_ID}</span> <span class="assort" style="padding:0 5px 0 0;"><me:datestring date="${list.INSERT_DATE}" /></span>
					<c:if test="${session.SESSION_USER_INFO.USER_ID == list.USER_ID}">
						<a href="#" onclick="jsCommentUpdateEdit('${list.COMMENT_IDX}'); return false;" class="button button_white">수정</a> 
						<a href="#" onclick="jsCommentDelete('${list.COMMENT_IDX}'); return false;" class="button button_white">삭제</a>
					</c:if>
					<c:if test="${session.SESSION_USER_INFO != null}">
						<a href="#" onclick="jsCommentReply('${list.COMMENT_IDX}'); return false;" class="button button_white">댓글</a>
					</c:if>
				</div>			
				<div id="comment${list.CONTENTS}" style="padding: 7px 0 1px 0;">
					${list.CONTENTS}
				</div>			
			</div>
			</form>
		</td>	
	</tr>	
	<tr><td style="background-image:url(images/dot.gif);height:1px"></td></tr>		        
	</c:forEach>
	<tr style="background-image:url('images/hr_bg.png');height:7px;"><td></td></tr>
	</table>
	</c:if>
	<br/>
	<c:if test="${session.SESSION_USER_INFO.USER_LEVEL >= session.SESSION_BULLETIN_INFO.INSERT_LEVEL}">
	<form id="articleCommentWrite">
	<input type="hidden" id="group_idx" name="group_idx" value="${parameters.group_idx}"/>
	<input type="hidden" id="article_idx" name="article_idx" value="${parameters.article_idx}"/>
	<input type="hidden" id="nowPage" name="nowPage" value="${parameters.nowPage}"/>
	<input type="hidden" id="searchMethod" name="searchMethod" value="${parameters.searchMethod}"/>
	<input type="hidden" id="searchWord" name="searchWord" value="${parameters.searchWord}"/>
	<table style="width:600px">
	<tr>	
		<td align="left">		
			<div>
				<div class="writer">
					<label class="smallText" >Write your message and submit</label>
					<textarea id="comment" name="comment" rows="5" style="width:598px;border:1px solid #dedede;overflow:hidden"></textarea>
				</div>
					
				<div class="submit clear">
					<a href="#" onclick="jsCommentInsert(); return false;" class="button button_blue">Submit</a>	
				</div>		
			</div>	
		</td>
	</tr>	
	</table>
	
	</form>
	</c:if>
	
</body>

</html>