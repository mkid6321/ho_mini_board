<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> 

<html>

<head>
	<script type="text/javascript">	
				
		var isCheckedId = false;
	
		function jsJoin(){
			  
		  var userIdLength = $.trim($('#user_id').val()).length;	  
		  if(userIdLength < 5 || userIdLength > 16){
			  alert("아이디가 유효하지 않습니다.\n5글자 이상 16글자 이하를 입력해주세요.");
			  return;
		  }
		  
		  if(!isCheckedId) {alert("아이디 중복 체크를 해주세요.");return;}
		  
		  var pwdLength = $.trim($('#pwd').val()).length;
		  if(pwdLength < 8 || pwdLength > 16){
			  alert("패스워드가 유효하지 않습니다.\n8글자 이상 16글자 이하를 입력해주세요.");  
			  return;
		  }
		  
		  var pwd = $.trim($('#pwd').val());
		  var pwdRe = $.trim($('#pwd_re').val());
		  if(pwd != pwdRe){
			  alert("패스워드가 일치하지않습니다.");  
			  return;
		  }
		  
		  if(!emailcheck( $.trim($("#email").val()) )) {alert("이메일을 확인해주세요.");return;}
		  if(!urlcheck( $.trim($("#homepage").val()) )) {alert("홈페이지를 확인해주세요.");return;}
		  
		  $('#blog').attr('action','./exclude/articleEntry.action');
		  $('#blog').submit();
		}
					
		function jsCheckId(){
			
		  if( $.trim($("#user_id").val()).length == 0 ){alert("아이디를 입력해주세요.");return;}
		  
		  $.ajax({
	            url:'./exclude/articleCheckId.action?user_id='+$.trim($("#user_id").val()),
	            type:'post',
	            success:function(data){
	            	if(eval(data)){
	            		isCheckedId = false; 
	            		$('#user_id').val(''); 
	            		alert('이미 존재하는 아이디입니다.'); 
	            		return;
	            	}else{ 
	            		isCheckedId = true; 
	            		alert("사용할 수 있습니다. ")
	            	}
	            }
	      })
		}
		
		function emailcheck(strValue){
			//입력을 안했으면
			if(strValue.length == 0) {	return false;	}
			
			//이메일 형식에 맞지않으면
			var regExp = /[0-9a-zA-Z][_0-9a-zA-Z-]*@[_0-9a-zA-Z-]+(\.[_0-9a-zA-Z-]+){1,2}$/;
			if (!strValue.match(regExp)) {	return false;	}
			return true;
		}
		
		function urlcheck(strValue){
			//입력을 안했으면
			if(strValue.length == 0) {	return true;	}
			
			//url형식에 맞지않으면
			var regExp = /^(((http(s?))\:\/\/)?)([0-9가-힣a-zA-Z\-]+\.)+[가-힣a-zA-Z]{2,6}(\:[0-9]+)?(\/\S*)?$/;
			if (!strValue.match(regExp)) {	return false;	}
			return true;
		}
		
	</script>
</head>
<body style="width:100%;text-align:center;margin:0 auto;margin-top:100px;">
	<form id="blog" action="articleEntry.action" method="post">
	<input type="hidden" id="group_idx" name="group_idx" value="${parameters.group_idx}"/>
	<input type="hidden" id="article_idx" name="article_idx" value="${parameters.article_idx}"/>
	<input type="hidden" id="nowPage" name="nowPage" value="${parameters.nowPage}"/>
	<input type="hidden" id="searchMethod" name="searchMethod" value="${parameters.searchMethod}"/>
	<input type="hidden" id="searchWord" name="searchWord" value="${parameters.searchWord}"/>
	<div class="skin_join">
		<ul>
			<li>
				<em>* 아이디</em>
				<div><input type="text" id="user_id" name="user_id" placeholder="영문/숫자만 입력가능, 5-12자 이내 입력" style="width:200px;" /> <a href="#" onclick="jsCheckId(); return false;" class="button button_orange" style="width:70px;">ID CHECK</a>
					<span style="color:#90c32c;">영문/숫자만 입력가능, 5-12자 이내 입력</span>
				</div>				        	
			</li>
			<li>
				<em>* 비밀번호</em>
				<div><input type="password" id="pwd" name="pwd" placeholder="8-16자 이내 입력. 영문(대/소문자 구분)/숫자" style="width:200px;" />
					<span style="color:#90c32c;">8-16자 이내 입력. 영문(대/소문자 구분)/숫자</span>
				</div>				        	
			</li>
			<li>
				<em>* 비빌번호 확인</em>
				<div><input type="password" id="pwd_re" placeholder="패스워드를 다시한번 입력해주세요." style="width:200px;" />
				</div>				        	
			</li>
			<li>
				<em>* 이름</em>
				<div><input type="text" id="name" name="name" /></div>
			</li>
			<li>
				<em>* 이메일 </em>
				<div><input type="text" id="email" name="email" style="width:200px;"/></div>
			</li>
			<li>
				<em>홈페이지 </em>
				<div><input type="text" id="homepage" name="homepage" style="width:200px;"/></div>
			</li>
			<li>
				<em>인사말 </em>
				<div><input type="text" id="description" name="description" style="width:240px;"/></div>
			</li>
			<li>
				<div><span style="color:#90c32c;" style="width:200px;">
				*표시된 항목은 필수 입력 항목입니다.
				</span></div>
			</li>
			<li>				
				<div style="text-align:center;"><img src="./images/line_dot.png" /></div>
			</li>
			<li>				
				<div style="text-align:center;">
					<a href="#" onclick="jsBack(); return false;" class="button button_blue">Cancel</a> <a href="#" onclick="jsJoin(); return false;" class="button button_red">Join</a>
				</div>		
			</li>
		</ul>
	</div>
		
	</form>
</body>
</html>