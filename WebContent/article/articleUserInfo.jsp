<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> 
<html>
<head>
<script type="text/javascript">

function jsUserEdit(){
	$('input').removeClass('empty');
	// 이름에 공백 체크
	if($('#user_name').val().length < 0 ){
		alert('이름은 필수 입력사항입니다.');
		$('#user_name').addClass('empty');
		return false;
	} else {
		var blank_pattern = /[\s]/g;
		if( blank_pattern.test( $('#user_name').val() ) == true){
		    alert('이름에 공백은 사용할 수 없습니다. ');
		    return false;
		}
	}
	
	// url 주소 체크 
	if($.trim($('#user_homepage').val()).length > 0 ){
		var chkExp = /http:\/\/([\w\-]+\.)+/g;
		if( chkExp.test( $('#user_homepage').val() ) != true ){
			alert('url이 올바른 양식이 아닙니다.');	
			$('#user_homepage').addClass('empty');
			return false;
		}
	}
	
	// email 체크
	var regExp = /[0-9a-zA-Z][_0-9a-zA-Z-]*@[_0-9a-zA-Z-]+(\.[_0-9a-zA-Z-]+){1,2}$/;
	//입력을 안했으면
	if($('#user_email').val().lenght == 0) { 
		alert('email은 필수입력사항입니다.');
		$('#user_email').addClass('empty');
		return false;	
	}
	//이메일 형식에 맞지않으면
	if (!$('#user_email').val().match(regExp)) { 
		alert('email을 형식이 맞지않습니다.');	
		$('#user_emails').addClass('empty');
		return false;
	}
	
  $('#article').attr('action','./articleUserUpdate.action');
  $('#article').submit();
}

/* $('#article')은 아래 선언한 form Tag의 id 값이다 */
function jsUserUnsubscribe(){
  $('#article').attr('action','./articleUserUnsubscribe.action');
  $('#article').submit();
}

</script>
</head>
<body>

<form id="article" action="articleUserEdit.action">
<input type="hidden" id="nowPage" name="nowPage" value="${parameters.nowPage}"/>
<input type="hidden" id="group_idx" name="group_idx" value="${parameters.group_idx}"/>
<input type="hidden" id="searchMethod" name="searchMethod" value="${parameters.searchMethod}"/>
<input type="hidden" id="searchWord" name="searchWord" value="${parameters.searchWord}"/>
<br>
<div class="skin_join">
	<ul>
		<li>
			<em>아이디</em>
			<div><input type="text" size="20" id="user_id" value="${USER_INFO.USER_ID}" class="formbgcolor" readonly />
			</div>
		</li>
		<li>
			<em>사용자 이름</em>
			<div><input type="text" size="20" id="user_name" name="user_name" value="${USER_INFO.USER_NAME}"/>
			</div>				        	
		</li>	
		<li>
			<em>사용자 레벨</em>
			<div><input type="text" size="20" value="${USER_INFO.USER_LEVEL}" class="formbgcolor" readonly />
			</div>				        	
		</li>
		<li>
			<em>홈페이지</em>
			<div><input type="text" size="20" id="user_homepage" name="homepage" value="${USER_INFO.HOMEPAGE}"/>
			</div>				        	
		</li>
		<li>
			<em>이메일</em>
			<div><input type="text" size="20" id="user_email" name="email" value="${USER_INFO.EMAIL}"/>
			</div>				        	
		</li>	
		<li>
			<em>인사말/설명</em>
			<div><input type="text" size="20" id="description" name="description" value="${USER_INFO.DESCRIPTION}"/>
			</div>				        	
		</li>	
		<li>
			<em>가입일</em>
			<div><input type="text" size="20" value="${USER_INFO.INSERT_DATE}" class="formbgcolor" readonly/>
			</div>				        	
		</li>
		<li>
			<em>최종 수정일</em>
			<div><input type="text" size="20" value="${USER_INFO.UPDATE_DATE}" class="formbgcolor" readonly/>
			</div>				        	
		</li>
	</ul>
</div>

<div>
	<a href="#" onclick="jsUserUnsubscribe(); return false;" class="button button_red">탈퇴</a> <a href="#" onclick="jsUserEdit(); return false;" class="button button_red">수정</a> <a href="#" class="button button_blue" onclick="jsBack(); return false;">취소</a>
</div>

</form>
</body>

</html>