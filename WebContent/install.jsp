<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.sql.*" %>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<link rel='stylesheet' type='text/css' media='all' href="css/style.css" />
	<style type="text/css">
	
	</style>
	<script src="js/jquery-2.1.4.min.js"></script>
	<script type="text/javascript">	
	
	</script>
</head>
<body>

<%
	String createTableUser =
			"CREATE TABLE IF NOT EXISTS `T_USER` ("+
					  "`USER_ID` varchar(64) NOT NULL,"+
					  "`PWD` varchar(255) DEFAULT NULL,"+
					  "`EMAIL` varchar(320) DEFAULT NULL,"+
					  "`USER_NAME` varchar(64) DEFAULT NULL,"+
					  "`INSERT_DATE` datetime DEFAULT NULL,"+
					  "`UPDATE_DATE` datetime DEFAULT NULL,"+
					  "`HOMEPAGE` varchar(255) DEFAULT NULL,"+
					  "`USER_LEVEL` int(11) DEFAULT NULL,"+
					  "`DEL_FLAG` varchar(5) DEFAULT NULL,"+
					  "`DESCRIPTION` varchar(512) DEFAULT NULL,"+
					  "PRIMARY KEY (`USER_ID`)"+
					") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
	
	String createTableArticleGroup = 
			"CREATE TABLE IF NOT EXISTS `T_ARTICLE_GROUP` ("+
					  "`GROUP_IDX` int(11) NOT NULL AUTO_INCREMENT,"+
					  "`GROUP_NAME` varchar(64) DEFAULT NULL,"+
					  "`DESCRIPTION` varchar(512) DEFAULT NULL,"+
					  "`INSERT_DATE` datetime DEFAULT NULL,"+
					  "`UPDATE_DATE` datetime DEFAULT NULL,"+
					  "`DEL_FLAG` varchar(5) DEFAULT NULL,"+
					  "`ACCESS_LEVEL` int(11) DEFAULT NULL,"+
					  "`INSERT_LEVEL` int(11) DEFAULT NULL,"+
					  "PRIMARY KEY (`GROUP_IDX`)"+
					") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
					
	String createTableArticleCategory = 
			"CREATE TABLE IF NOT EXISTS `T_ARTICLE_CATEGORY` ("+
					  "`CATEGORY_IDX` int(11) NOT NULL AUTO_INCREMENT,"+
					  "`CATEGORY_NAME` varchar(64) DEFAULT NULL,"+
					  "`DESCRIPTION` varchar(512) DEFAULT NULL,"+
					  "`INSERT_DATE` datetime DEFAULT NULL,"+
					  "`OUTPUT_SEQ` int(11) DEFAULT NULL,"+
					  "`DEL_FLAG` varchar(5) DEFAULT NULL,"+
					  "`GROUP_IDX` int(11) NOT NULL,"+
					  "PRIMARY KEY (`CATEGORY_IDX`),"+
					  "KEY `R_1` (`GROUP_IDX`),"+
					  "CONSTRAINT `R_1` FOREIGN KEY (`GROUP_IDX`) REFERENCES `T_ARTICLE_GROUP` (`GROUP_IDX`)"+
					") ENGINE=InnoDB DEFAULT CHARSET=utf8;";

	String createTableArticle =
			"CREATE TABLE IF NOT EXISTS `T_ARTICLE` ("+
					  "`TITLE` varchar(255) DEFAULT NULL,"+
					  "`CONTENTS` varchar(2048) DEFAULT NULL,"+
					  "`INSERT_DATE` datetime DEFAULT NULL,"+
					  "`UPDATE_DATE` datetime DEFAULT NULL,"+
					  "`DEPTH` int(11) DEFAULT NULL,"+
					  "`REPLY_ORDER` int(11) DEFAULT NULL,"+
					  "`DEL_FLAG` varchar(5) DEFAULT NULL,"+
					  "`READ_CNT` int(11) DEFAULT NULL,"+
					  "`NOTICE_FLAG` varchar(5) DEFAULT NULL,"+
					  "`INQUERY_LEVEL` int(11) DEFAULT NULL,"+
					  "`ARTICLE_IDX` int(11) NOT NULL AUTO_INCREMENT,"+
					  "`GROUP_IDX` int(11) NOT NULL,"+
					  "`CATEGORY_IDX` int(11) NOT NULL,"+
					  "`ORIGIN_IDX` int(11) DEFAULT NULL,"+
					  "`USER_ID` varchar(64) NOT NULL,"+
					  "PRIMARY KEY (`ARTICLE_IDX`),"+
					  "KEY `R_2` (`GROUP_IDX`),"+
					  "KEY `R_3` (`CATEGORY_IDX`),"+
					  "KEY `R_4` (`USER_ID`),"+
					  "CONSTRAINT `R_2` FOREIGN KEY (`GROUP_IDX`) REFERENCES `T_ARTICLE_GROUP` (`GROUP_IDX`),"+
					  "CONSTRAINT `R_3` FOREIGN KEY (`CATEGORY_IDX`) REFERENCES `T_ARTICLE_CATEGORY` (`CATEGORY_IDX`),"+
					  "CONSTRAINT `R_4` FOREIGN KEY (`USER_ID`) REFERENCES `T_USER` (`USER_ID`)"+
					") ENGINE=InnoDB DEFAULT CHARSET=utf8;";

	String createTableArticleComment =
			"CREATE TABLE IF NOT EXISTS `T_ARTICLE_COMMENT` ("+
					  "`COMMENT_IDX` int(11) NOT NULL AUTO_INCREMENT,"+
					  "`ORIGIN_IDX` int(11) DEFAULT NULL,"+
					  "`REPLY_ORDER` int(11) DEFAULT NULL,"+
					  "`DEPTH` int(11) DEFAULT NULL,"+
					  "`CONTENTS` varchar(1024) DEFAULT NULL,"+
					  "`INSERT_DATE` datetime DEFAULT NULL,"+
					  "`UPDATE_DATE` datetime DEFAULT NULL,"+
					  "`DEL_FLAG` varchar(5) DEFAULT NULL,"+
					  "`ARTICLE_IDX` int(11) NOT NULL,"+
					  "`USER_ID` varchar(64) NOT NULL,"+
					  "PRIMARY KEY (`COMMENT_IDX`),"+
					  "KEY `R_5` (`ARTICLE_IDX`),"+
					  "KEY `R_6` (`USER_ID`),"+
					  "CONSTRAINT `R_5` FOREIGN KEY (`ARTICLE_IDX`) REFERENCES `T_ARTICLE` (`ARTICLE_IDX`),"+
					  "CONSTRAINT `R_6` FOREIGN KEY (`USER_ID`) REFERENCES `T_USER` (`USER_ID`)"+
					") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
					
	String createTableArticleFile =	
			"CREATE TABLE IF NOT EXISTS `T_ARTICLE_FILE` ("+
					  "`FILE_IDX` int(11) NOT NULL AUTO_INCREMENT,"+
					  "`ARTICLE_IDX` int(11) NOT NULL,"+
					  "`FILENAME` varchar(255) DEFAULT NULL,"+
					  "`FILEPATH` varchar(255) DEFAULT NULL,"+
					  "`FILESIZE` int(11) DEFAULT NULL,"+
					  "`FILETYPE` varchar(64) DEFAULT NULL,"+
					  "`INSERT_DATE` datetime DEFAULT NULL,"+
					  "`DEL_FLAG` varchar(5) DEFAULT NULL,"+
					  "`INPUT_TYPE` varchar(32) DEFAULT NULL,"+
					  "PRIMARY KEY (`FILE_IDX`),"+
					  "KEY `R_7` (`ARTICLE_IDX`),"+
					  "CONSTRAINT `R_7` FOREIGN KEY (`ARTICLE_IDX`) REFERENCES `T_ARTICLE` (`ARTICLE_IDX`)"+
					") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
	
	String createTableProperty =
			"CREATE TABLE IF NOT EXISTS `T_PROPERTY` ("+
					  "`PROPERTY_ID` varchar(64) NOT NULL,"+
					  "`PROPERTY_VALUE` varchar(2048) DEFAULT NULL,"+
					  "`DESCRIPTION` varchar(512) DEFAULT NULL,"+
					  "PRIMARY KEY (`PROPERTY_ID`)"+
					") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
/* 				
	String createTablePropertyElement =
			"CREATE TABLE IF NOT EXISTS `T_PROPERTY_ELEMENT` ("+
					  "`ELEMENT_IDX` int(11) NOT NULL AUTO_INCREMENT,"+
					  "`ELEMENT_VALUE` varchar(1024) DEFAULT NULL,"+
					  "`PROPERTY_ID` varchar(64) NOT NULL,"+
					  "PRIMARY KEY (`ELEMENT_IDX`),"+
					  "KEY `R_8` (`PROPERTY_ID`),"+
					  "CONSTRAINT `R_8` FOREIGN KEY (`PROPERTY_ID`) REFERENCES `T_PROPERTY` (`PROPERTY_ID`)"+
					") ENGINE=InnoDB DEFAULT CHARSET=utf8;";				
*/
	String createTableMessage =
			"CREATE TABLE IF NOT EXISTS `T_MESSAGE` ("+
					"`MESSAGE_IDX` INT(11) NOT NULL AUTO_INCREMENT,"+
					"`TITLE` VARCHAR(255) NULL DEFAULT NULL,"+
					"`CONTENTS` VARCHAR(1024) NULL DEFAULT NULL,"+
					"`INSERT_DATE` DATETIME NULL DEFAULT NULL,"+
					"`DEL_FLAG` VARCHAR(5) NULL DEFAULT NULL,"+
					"`USER_ID` VARCHAR(64) NOT NULL,"+
					"`RECEIVER` VARCHAR(64) NULL DEFAULT NULL,"+
					"`STATUS` INT(11) NULL DEFAULT NULL,"+
					"PRIMARY KEY (`MESSAGE_IDX`),"+
					"KEY `R_8` (`USER_ID`),"+
					"CONSTRAINT `R_8` FOREIGN KEY (`USER_ID`) REFERENCES `T_USER` (`USER_ID`)"+
				  ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";

					
	String insertAdminAccount = 
			"INSERT INTO `T_USER` (`USER_ID`, `PWD`, `EMAIL`, `USER_NAME`, `INSERT_DATE`, `UPDATE_DATE`, `HOMEPAGE`, `USER_LEVEL`, `DEL_FLAG`, `DESCRIPTION`) VALUES"+
				"('admin', '5F4DCC3B5AA765D61D8327DEB882CF99', 'admin@abc.com', '관리자', NOW(), NULL, '', 255, 'false', '관리자계정입니다.');";
				
	String insertArticleGroup = 
			"INSERT INTO `T_ARTICLE_GROUP` (`GROUP_NAME`, `DESCRIPTION`, `INSERT_DATE`, `UPDATE_DATE`, `DEL_FLAG`, `ACCESS_LEVEL`, `INSERT_LEVEL`) VALUES"+
				"('기본게시판', '기본으로 생성되는 게시판입니다.', NOW(), NULL, 'false', 0, 0);";
				
	String insertArticleGroupCategory = 
			"INSERT INTO `T_ARTICLE_CATEGORY` (`CATEGORY_NAME`, `DESCRIPTION`, `INSERT_DATE`, `OUTPUT_SEQ`, `DEL_FLAG`, `GROUP_IDX`) VALUES"+
				"('일반', '기본 카테고리', NOW(), 1, 'false', 1);";
				
	String insertProperty1 = 
			"INSERT INTO `T_PROPERTY` (`PROPERTY_ID`, `PROPERTY_VALUE`, `DESCRIPTION`) VALUES"+
				"('ARTICLE_COUNT_PER_PAGE', 10, '페이지당 글 수');";
	String insertProperty2 = 
			"INSERT INTO `T_PROPERTY` (`PROPERTY_ID`, `PROPERTY_VALUE`, `DESCRIPTION`) VALUES"+
				"('BULLETIN_BASE_CATEGORY', '[{\"category_name\":\"기본 카테고리\",\"output_seq\":1,\"description\":\"기본 카테고리\",\"del_flag\":\"false\"}]', '기본 카테고리');";			
	String insertProperty3 = 
			"INSERT INTO `T_PROPERTY` (`PROPERTY_ID`, `PROPERTY_VALUE`, `DESCRIPTION`) VALUES"+
				"('SYSTEM_EMAIL', '', '시스템 메일');";			
									
							
	try{
 		Class.forName("org.mariadb.jdbc.Driver");
		String url = "jdbc:mariadb://127.0.0.1:3306";
		Connection con = DriverManager.getConnection(url,"mkid6321","ha1591592");
		Statement stat = con.createStatement();
		String query = "CREATE DATABASE IF NOT EXISTS `miniboard` /*!40100 DEFAULT CHARACTER SET utf8 */;";
		int result = stat.executeUpdate(query);
		System.out.println("Step 1. "+result);
		if(result > 0) {
%>
			<div>DB 생성에 성공했습니다.</div>
<%			
			stat.execute("USE `miniboard`;");
			System.out.println("Step 2. Use DB");
%>
			<div>DB 지정에 성공했습니다.</div>
<%							
			stat.executeUpdate(createTableUser);
				
			System.out.println("Step 3. ");
			
			stat.executeUpdate(createTableArticleGroup);
				
			System.out.println("Step 4. ");
					
			stat.executeUpdate(createTableArticleCategory);
					
			System.out.println("Step 5. ");
					
			stat.executeUpdate(createTableArticle);
						
			System.out.println("Step 6. ");
						
			stat.executeUpdate(createTableArticleComment);
							
			System.out.println("Step 7. ");
								
			stat.executeUpdate(createTableArticleFile);
								
			System.out.println("Step 8. ");
			
			stat.executeUpdate(createTableProperty);
			
			System.out.println("Step 9. ");
			
			stat.executeUpdate(createTableMessage);
			
			System.out.println("Step 10. ");
%>
			<div>모든 테이블 생성이 완료되었습니다.</div>
<%				
			stat.executeUpdate(insertAdminAccount);
			
			System.out.println("Step 11. ");
								
			stat.executeUpdate(insertArticleGroup);
								
			System.out.println("Step 12. ");
			
			stat.executeUpdate(insertArticleGroupCategory);
			
			System.out.println("Step 13. ");
			
			stat.executeUpdate(insertProperty1);
			
			System.out.println("Step 14. ");
			
			stat.executeUpdate(insertProperty2);
			
			System.out.println("Step 15. ");
			
			stat.executeUpdate(insertProperty3);
			
			System.out.println("Step 16. ");
%>
			<div>기본 데이터 생성이 완료되었습니다.</div>
<%	

		}  
		stat.close();
		con.close();
	}catch(Exception e){
		out.println( e );
	}
  
%>
	
</body>

</html>