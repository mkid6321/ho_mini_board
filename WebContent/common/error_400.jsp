<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<html>
<head>
<title>400</title>
</head>
<body>

	<div style="text-align: center;padding-top: 150px;width: 50%; margin-left:25%;">
		<h1 style="color: #5c90d2;font-size: 128px !important;font-stretch: semi-condensed;line-height: 1.2;font-weight: 100 !important;margin:0;">400</h1>
		<h2 style="color: #646464;font-size: 24px;font-weight: 100 !important;">We are sorry but your request contains bad syntax and cannot be fulfilled..</h2>
		<br><br>
		<a style="color: #5c90d2;font-size: 14px;" href="#" onclick="history.back(); return false;">Go Back</a>
	</div>

</body>
</html>