<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<html>
<head>
<title>MAINTENANCE</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
</head>
<body>

	<div style="text-align: center;padding-top: 150px;width: 50%; margin-left:25%;">
		<h1 style="color: #c9c9c9;font-size: 128px !important;font-stretch: semi-condensed;line-height: 1.2;font-weight: 100 !important;margin:0;"><i class="fa fa-cog fa-spin"></i></h1>
		<h2 style="color: #646464;font-size: 24px;font-weight: 100 !important;">Sorry, we’re down for maintenance.</h2>
		<br><br>
		<a style="color: #5c90d2;font-size: 14px;" href="#" onclick="history.back(); return false;">Go Back</a>
	</div>

</body>
</html>