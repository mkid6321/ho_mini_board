package mini.board.trabal;

import java.util.Map;

import com.opensymphony.xwork2.ActionContext;

import mini.board.common.BaseAction;

public class TrabalAction extends BaseAction{

	private TrabalBusiness bo = TrabalBusiness.getInstance();
	
	/**
	 * 숙박업체 목록
	 * @return 처리 성공 여부 SUCCESS값
	 */
	public String trabalLodgingList() {
		
		Map<String,Object> context = ActionContext.getContext().getContextMap();

		try {
			

		} catch (Exception e) {
			return ERROR;
		}
		return SUCCESS;
	}
	
	/**
	 * 숙박업체 등록
	 * @return 처리 성공 여부 SUCCESS값
	 */
	public String trabalLodgingAdd() {
		
		Map<String,Object> context = ActionContext.getContext().getContextMap();

		try {
			

		} catch (Exception e) {
			return ERROR;
		}
		return SUCCESS;
	}
	
	/**
	 * 숙박업체 등록
	 * @return 처리 성공 여부 SUCCESS값
	 */
	public String trabalLodgingInsert() {
		
		Map<String,Object> context = ActionContext.getContext().getContextMap();

		try {
			Object obj = entity.get("facility");
			
			if(obj instanceof String[]){
				String[] facility = (String[])entity.get("facility");
				
				String temp = "";
				for(String str : facility){
					temp += str + " ";
				}
				entity.put("facility", temp);
			}
			// Map 형태로 넘어온다...
//			entity.
			bo.trabalLodgingInsert(entity);
//			System.out.println(entity);
//			Object obj = entity.get("facility");
//			System.out.println(obj.getClass());
		
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
		return "doTrabalLodgingList";
	}
	
	/**
	 * 숙박업체 상세
	 * @return 처리 성공 여부 SUCCESS값
	 */
	public String trabalLodgingView() {
		
		Map<String,Object> context = ActionContext.getContext().getContextMap();

		try {
			

		} catch (Exception e) {
			return ERROR;
		}
		return SUCCESS;
	}
	
	/**
	 * 숙박업체 객실 등록
	 * @return 처리 성공 여부 SUCCESS값
	 */
	public String trabalRoomAdd() {
		
		Map<String,Object> context = ActionContext.getContext().getContextMap();

		try {
			

		} catch (Exception e) {
			return ERROR;
		}
		return SUCCESS;
	}
	
	/**
	 * 숙박업체 객실 가격 테이블
	 * @return 처리 성공 여부 SUCCESS값
	 */
	public String trabalPriceCalendar() {
		
		Map<String,Object> context = ActionContext.getContext().getContextMap();

		try {
			

		} catch (Exception e) {
			return ERROR;
		}
		return SUCCESS;
	}
	
	/**
	 *  숙박업체 객실룸 등록
	 *  @return 처리 성공 여부 SUCCESS값..
	 * 
	 */
	public String trabalRoomInsert(){
		Map<String, Object> context = ActionContext.getContext().getContextMap();
		try {
			bo.trabalRoomInsert(entity);
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
		return "dotrabalRoomList";
	}
	
}
