package mini.board.trabal;

import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import mini.board.common.BaseBusiness;

public class TrabalBusiness extends BaseBusiness{

	private static TrabalBusiness instance = null;	
     
    public TrabalBusiness() {
    	super();
    }

    public synchronized static TrabalBusiness getInstance(){
         if(instance == null){	instance = new TrabalBusiness();	}
         return instance;
    }    

    // 숙박업체 등록
    public void trabalLodgingInsert(Map entity){
    	SqlSession session = sqlSessionFactory.openSession();
  
    	try {
    		// 나오는것은 ID가 나온다..
			session.insert("trabalLodgingInsert",entity);
			session.commit();
    	}catch(Exception e){
    		session.rollback();
    	}finally{
			session.close();
		}
    }
    
    // 숙박업체 객식룸 등록
    public void trabalRoomInsert(Map entity){
    	SqlSession session = sqlSessionFactory.openSession();
    	
    	try {
			session.insert("trabalRoomInsert",entity);
			session.commit();
		} catch (Exception e) {
			e.printStackTrace();
			session.close();
		}finally{
			session.close();
		}
    }
}
