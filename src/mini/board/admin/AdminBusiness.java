package mini.board.admin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mini.board.common.BaseBusiness;
import mini.board.common.Cryptograph;

import org.apache.ibatis.exceptions.PersistenceException;
import org.apache.ibatis.session.SqlSession;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class AdminBusiness extends BaseBusiness{

	private static AdminBusiness instance = null;	
	
	public AdminBusiness() {
		super();
	}
	
    /**
	 * AdminBusiness 객체 생성 메소드 
	 * @return instance
	 */
    public synchronized static AdminBusiness getInstance(){
         if(instance == null){	instance = new AdminBusiness();	}
         return instance;
    }    
    
    public Map adminLogin(Map entity) {
    	SqlSession session = sqlSessionFactory.openSession();
    	
    	Map map;
    	try {
    		map = (Map)session.selectOne("adminLogon",entity);
    	}finally {
    		session.close();
    	}	

    	return map;
    }
    
    /**
	 * 생성된 게시판의 갯수를 반환한다. 
	 * @return cnt
	 */
    public int adminBulletinListCnt(Map entity) {    	
    	SqlSession session = sqlSessionFactory.openSession();
    	
    	int cnt;
    	try {
    		cnt = (Integer)session.selectOne("bulletinListCnt",entity);    	 
    	}finally {
    		session.close();
    	}

    	return cnt;
    }
    
    public List adminBulletinList(Map entity) {
    	SqlSession session = sqlSessionFactory.openSession();
    	
    	List list;
    	try {
    		list = session.selectList("bulletinList",entity); 
    	}finally {
    		session.close();
    	}	
    	return list;
    }
    
    /**
     * need transaction
     * @param entity
     * @throws Exception
     */
    public void adminBulletinInsert(Map entity){
    	SqlSession session = sqlSessionFactory.openSession();
    	
    	try {
    		int cnt = session.insert("adminBulletinInsert",entity);
    		// if(cnt == 0) exception
        	if(entity.containsKey("CATEGORY_LIST")){
        		ArrayList categoryList = (ArrayList)entity.get("CATEGORY_LIST");
        		for(int i=0; i<categoryList.size(); i++) {
        			HashMap category = (HashMap) categoryList.get(i);
        			category.put("group_idx", entity.get("LAST_INSERT_ID"));
        			
        			session.insert("adminBulletinCategoryInsert",category);
        		}
        	}
        	
        	session.commit();
    	}catch(PersistenceException e) {
    		session.rollback();
    		e.printStackTrace();
    	}finally {
    		session.close();
    	}	
    }
    
    public Map adminBulletinInfo(Map entity){
    	SqlSession session = sqlSessionFactory.openSession();
    	
    	Map map;
    	try {
    		
    		map = (Map)session.selectOne("adminBulletinInfo",entity);
        	
        	List categoryList = session.selectList("adminBulletinCategoryList", entity);
        	
        	map.put("CATEGORY_LIST", categoryList);
        	
    	}finally {
    		session.close();
    	}	
    	
    	return map;
    }
    
    /**
     * need transaction
     * @param entity
     * @throws Exception
     */
    public void adminBulletinUpdate(Map entity){
    	SqlSession session = sqlSessionFactory.openSession();
    	
    	try {
        	session.update("adminBulletinUpdate",entity);
        	
        	if(entity.containsKey("CATEGORY_LIST")){
        		ArrayList categoryList = (ArrayList)entity.get("CATEGORY_LIST");
        		for(int i=0; i<categoryList.size(); i++) {
        			HashMap category = (HashMap) categoryList.get(i);
        			category.put("group_idx", entity.get("group_idx"));
        			String categoryIdx = (String) category.get("category_idx");
        			if(categoryIdx == null || categoryIdx.trim().length() == 0) {
        				session.insert("adminBulletinCategoryInsert",category);
        			}else{
        				session.update("adminBulletinCategoryUpdate",category);	
        			}
        		}
        	}
        	
        	session.commit();
    	}catch(PersistenceException e) {
    		session.rollback();
    		e.printStackTrace();
    	}finally {
    		session.close();
    	}	
    }
    
    public int adminUserListCnt(Map entity){    	  
    	SqlSession session = sqlSessionFactory.openSession();
    	
    	int cnt = 0;
    	try {
    		cnt = (Integer)session.selectOne("adminUserListCnt",entity);
    	}finally {
    		session.close();
    	}	
    	    	    	
    	return cnt;
    }
    
    public List adminUserList(Map entity){    	 
    	SqlSession session = sqlSessionFactory.openSession();
    	
    	List list;
    	try {
    		list = session.selectList("adminUserList",entity);  
    	}finally {
    		session.close();
    	}	
    	  	    	
    	return list;
    }    

    /**
     * need transaction
     * @param entity
     * @throws Exception
     */
	public int adminUserInsert(Map entity){
    	SqlSession session = sqlSessionFactory.openSession();
    	
    	int cnt = 0;
    	try {
    		cnt = session.insert("adminUserInsert",entity);
    		
    		session.commit();
    	}catch(PersistenceException e) {
    		session.rollback();
    		e.printStackTrace();
    	}finally {
    		session.close();
    	}	
		
    	return cnt;
	}
	
    public Map adminUserInfo(Map entity){
    	SqlSession session = sqlSessionFactory.openSession();
    	
    	Map map;
    	try {
    		map = (Map)session.selectOne("adminUserInfo",entity);
    	}finally {
    		session.close();
    	}	
    	
    	return map;
    }
    
    /**
	 * 메시지의 갯수를 반환한다. 
	 * @return cnt
	 */
    public int adminMessageListCnt(Map entity) {    	
    	SqlSession session = sqlSessionFactory.openSession();
    	
    	int cnt;
    	try {
    		cnt = (Integer)session.selectOne("adminMessageListCnt",entity);    	 
    	}finally {
    		session.close();
    	}

    	return cnt;
    }
    
    public List adminMessageList(Map entity) {
		SqlSession session = sqlSessionFactory.openSession();
    	
		List list;
    	try {
    		list = session.selectList("adminMessageList", entity);  
    	}finally {
    		session.close();
    	}	
    	  	    	
    	return list;
	}
    
    /**
	 * 메시지의 갯수를 반환한다. 
	 * @return cnt
	 */
    public int adminSendMessageListCnt(Map entity) {    	
    	SqlSession session = sqlSessionFactory.openSession();
    	
    	int cnt;
    	try {
    		cnt = (Integer)session.selectOne("adminSendMessageListCnt",entity);    	 
    	}finally {
    		session.close();
    	}

    	return cnt;
    }
    
    public List adminSendMessageList(Map entity) {
		SqlSession session = sqlSessionFactory.openSession();
    	
		List list;
    	try {
    		list = session.selectList("adminSendMessageList", entity);  
    	}finally {
    		session.close();
    	}	
    	  	    	
    	return list;
	}
    
    /**
     * @param entity
     * @throws Exception
     */
	public int adminMessageInsert(Map entity){
    	SqlSession session = sqlSessionFactory.openSession();
    	
    	int cnt = 0;
    	try {
    		cnt = session.insert("adminMessageInsert",entity);
    		
    		session.commit();
    	}catch(PersistenceException e) {
    		session.rollback();
    		e.printStackTrace();
    	}finally {
    		session.close();
    	}	
		
    	return cnt;
	}
    
    public int adminUserUpdate(Map entity){
    	SqlSession session = sqlSessionFactory.openSession();
    	
    	int cnt = 0;
    	try {
    		cnt = session.update("adminUserUpdate",entity);
    		
    		session.commit();
    	}catch(PersistenceException e) {
    		session.rollback();
    		e.printStackTrace();
    	}finally {
    		session.close();
    	}	
    	
    	return cnt;
    }

	public List adminPropertyList(Map entity) {
		SqlSession session = sqlSessionFactory.openSession();
    	
		List list;
    	try {
    		list = session.selectList("adminPropertyList", entity);  
    	}finally {
    		session.close();
    	}	
    	  	    	
    	return list;
	}

	/**
     * need transaction
     * @param entity
     * @throws Exception
     */
	public int adminPropertyUpdate(Map entity){
		SqlSession session = sqlSessionFactory.openSession();
    	
    	int cnt = 0;
    	try {
    		cnt = session.update("adminPropertyUpdate",entity);
    		
    		session.commit();
    	}catch(PersistenceException e) {
    		session.rollback();
    		e.printStackTrace();
    	}finally {
    		session.close();
    	}	
		
    	return cnt;
	}
	
	public Map adminPropertySystemEmail() throws IOException{
    	SqlSession session = sqlSessionFactory.openSession();
    	
    	Map result = new HashMap();;
    	try {
    		Map map = (Map)session.selectOne("adminProperty", "SYSTEM_EMAIL");
			
    		if(map != null && map.containsKey("PROPERTY_VALUE")) {
	    		String value = (String) map.get("PROPERTY_VALUE");
	    		if(!(value.trim().length() == 0)) {
					ObjectMapper mapper = new ObjectMapper();
					
					map = mapper.readValue(value, Map.class);
		    		
		    		if(map != null) {
		    			if(((String)map.get("admin_email")).trim().length() > 0) {
		    				result.put("ADMIN_EMAIL", Cryptograph.getBASE64Decoder((String)map.get("admin_email")));
		    			}
		    			if(((String)map.get("admin_email_pwd")).trim().length() > 0) {
		    				result.put("ADMIN_EMAIL_PWD", Cryptograph.getBASE64Decoder((String)map.get("admin_email_pwd")));
		    			}
		    		}
	    		}
    		}
    	}finally {
    		session.close();
    	}	
    	
    	return result;
	}
	

    public List adminPropertyBaseCategory() {
    	SqlSession session = sqlSessionFactory.openSession();
    	
    	List result = new ArrayList();
    	
    	try{
    	
    		Map map = (Map)session.selectOne("adminProperty", "BULLETIN_BASE_CATEGORY");
			
    		String value = (String) map.get("PROPERTY_VALUE");
    		
			ObjectMapper mapper = new ObjectMapper();
			
			result = mapper.readValue(value, List.class);
			
    	} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}finally {
    		session.close();
    	}	
    	
    	return result;
    }
    
	public int adminPropertyCountPerPage() {
		SqlSession session = sqlSessionFactory.openSession();
		
		int cnt = 0;
    	try {
    		
    		Map map = (Map)session.selectOne("adminProperty", "ARTICLE_COUNT_PER_PAGE");
    		cnt = Integer.parseInt((String) map.get("PROPERTY_VALUE"));
    	}finally {
    		session.close();
    	}	
		
		return cnt;
	}

	public List adminSampleExcel() {
		SqlSession session = sqlSessionFactory.openSession();
    	
		List list;
    	try {
    		list = session.selectList("adminSampleExcel");  
    	}finally {
    		session.close();
    	}	
    	  	    	
    	return list;
	}

	public List adminManagerList(Map entity) {
		SqlSession session = sqlSessionFactory.openSession();
    	
		List list;
    	try {
    		list = session.selectList("adminManagerList");  
    	}finally {
    		session.close();
    	}	
    	  	    	
    	return list;
	}

	public Map adminDisplay(Map entity) {
		SqlSession session = sqlSessionFactory.openSession();
		
    	Map map= new HashMap();
    			
    	try {
    		
    		int cnt = (Integer)session.selectOne("adminMessageListCnt",entity);    	 
    		
    		map.put("UNREAD_MESSAGE_CNT", cnt);
    		
    	}finally {
    		session.close();
    	}	
    	
    	return map;
	}
	
}
