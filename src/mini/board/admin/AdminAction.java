package mini.board.admin;

import java.io.File;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.struts2.dispatcher.HttpParameters;
import org.apache.struts2.dispatcher.Parameter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.opensymphony.xwork2.ActionContext;

import mini.board.common.BaseAction;
import mini.board.common.Cryptograph;
import mini.board.common.Navigator;
import mini.board.common.Validator;
import mini.board.exception.NotAuthorizedException;

public class AdminAction extends BaseAction{

	private AdminBusiness bo = AdminBusiness.getInstance();
	
	/**
	 * 관리자 관련 action 시 권한을 체크
	 * 
	 * @param context
	 * @throws NotAuthorizedException 권한이 없는 요청을 행했을때 발생
	 */
	public void common(Map context) throws NotAuthorizedException {

		int userLevel = 0;
		Map userInfoMap = (Map) ActionContext.getContext().getSession().get(SESSION_USER);
		if (userInfoMap != null && userInfoMap.containsKey("USER_LEVEL")) {
			userLevel = (Integer) userInfoMap.get("USER_LEVEL");
		}
		
		System.out.println("userLevel "+userLevel);
		
		if (userLevel < 200) throw new NotAuthorizedException("접근권한이 없습니다.");

		Map data = new HashMap();
		data.put("user_id", userInfoMap.get("USER_ID"));
		data.put("status", 0);
		
		Map map = bo.adminDisplay(data);
		
		userInfoMap.putAll(map);
		
	}

	
	/**
	 * 관리자 > 대시보드
	 * @return
	 */
	public String adminDashboard(){
		
		Map<String,Object> context = ActionContext.getContext().getContextMap();
		
		try {
		
			common(context);
		
		} catch (NotAuthorizedException e) {
			System.out.println("adminDashboard "+e);
			return LOGIN;
		}
		
		return SUCCESS;
	}
	
	/**
	 * 실제 사용자의 Login의 처리하는 Method
	 * 
	 * @return
	 */
	public String adminLogon() {
		
		Map<String,Object> context = ActionContext.getContext().getContextMap();

		try {
			
			if (!Validator.validate(entity, "user_id", "pwd")) {
				return INPUT;
			}

			entity.put("pwd", Cryptograph.getCryptoStr(getText("enctype"), (String) entity.get("pwd")));
			Map map = bo.adminLogin(entity);
			System.out.println("==> "+map);
			if (map != null) {
				ActionContext.getContext().getSession().put(SESSION_USER, map);

				String url = (String) entity.get("last_url");
				System.out.println("adminLogon url : "+url);
				if (url != null) {
					context.put("LAST_URL", url);
					return "redirect";
				}
				
				return "doAdminDashboard";
			} else {
				context.put("LOGIN_FAIL", "true");
				return LOGIN;
			}

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		
		return ERROR;
	}
	
	/**
	 * 
	 * @return
	 */
	public String adminLogout() {

		ActionContext.getContext().getSession().remove(SESSION_USER);

		return LOGIN;
	}
	
	/**
	 * 관리자 > 게시판 목록 페이지 
	 * @return SUCCESS
	 */
	public String adminBulletinList(){
		
		Map<String,Object> context = ActionContext.getContext().getContextMap();

		try{
			
			common(context);
			
		    int lineCnt	= 15;
			String nowPage	= StringUtils.isBlank((CharSequence)entity.get("nowPage"))?"1":(String)entity.get("nowPage");
									
			String searchMethod = StringUtils.isBlank((CharSequence) entity.get("searchMethod"))?"group_name":(String)entity.get("searchMethod");
			String searchWord = StringUtils.isBlank((CharSequence)entity.get("searchWord"))?"":(String)entity.get("searchWord");
			
			entity.put("lineCnt", lineCnt);
			entity.put("nowPage", nowPage);
			entity.put("offset", lineCnt*(Integer.parseInt(nowPage)-1));
			
			entity.put("searchMethod", searchMethod);
			entity.put("searchWord"  , searchWord);
			
			int totlCnt	= bo.adminBulletinListCnt(entity);
			List list = bo.adminBulletinList(entity);	
			
			String page = Navigator.pageNavi(totlCnt, Integer.parseInt(nowPage), lineCnt);
			
			context.put("BULLETIN_LIST", list);									
			context.put("PAGE", page);
		
		} catch (NotAuthorizedException e) {
			return LOGIN;
		} catch(Exception e){
			e.printStackTrace();
		}
		return SUCCESS;
	}	
	
	/**
	 * 게시판 추가 페이지 
	 * @return SUCCESS
	 */
	public String adminBulletinAdd(){
		
		Map<String,Object> context = ActionContext.getContext().getContextMap();

		try{
			
			common(context);
			
			List categoryList = bo.adminPropertyBaseCategory();
			
			context.put("CATEGORY_LIST", categoryList);	
		
		} catch (NotAuthorizedException e) {
			return LOGIN;
		}catch(Exception e){
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	/**
	 * 게시판 추가 메소드 
	 * @return SUCCESS
	 */
	public String adminBulletinInsert(){
		
		Map<String,Object> context = ActionContext.getContext().getContextMap();
		
		try{
			
			common(context);
			
			if(entity.containsKey("C_CATEGORY_NAME")) {
				
				ArrayList categoryList = new ArrayList();	
				
				if(entity.get("C_CATEGORY_NAME") instanceof String[]) {
					String[] delFlagArray = (String[]) entity.get("C_DEL_FLAG");
					String[] descriptionArray = (String[]) entity.get("C_DESCRIPTION");
					String[] categoryNameArray = (String[]) entity.get("C_CATEGORY_NAME");
					
					int i=0;
					for(String str : categoryNameArray) {
						
						HashMap category = new HashMap();
						category.put("output_seq", (i+1));
						category.put("category_name", categoryNameArray[i]);
						category.put("description", descriptionArray[i]);
						category.put("del_flag", delFlagArray[i]);
						
						categoryList.add(category);
						i++;
					}
					
				}else{
					HashMap category = new HashMap();
					category.put("output_seq", 1);
					category.put("category_name", entity.get("C_CATEGORY_NAME"));
					category.put("description", entity.get("C_DESCRIPTION"));
					category.put("del_flag", entity.get("C_DEL_FLAG"));
					
					categoryList.add(category);
				}
				
				entity.put("CATEGORY_LIST", categoryList);
			}
			
			bo.adminBulletinInsert(entity);
			
		} catch (NotAuthorizedException e) {
			return LOGIN;
		} catch(Exception e){
			e.printStackTrace();
		}
		return "doAdminBulletinList";
	}
	
	/**
	 * 게시판 설정 수정 폼 구성 
	 * @return SUCCESS
	 */
	public String adminBulletinEdit(){
		
		Map<String,Object> context = ActionContext.getContext().getContextMap();
		
		try {
			common(context);
			
			Map map = bo.adminBulletinInfo(entity);
			
			context.put("BULLETIN_INFO", map);
			
		} catch (NotAuthorizedException e) {
			return LOGIN;
		}
			
		return SUCCESS;
	}
	
	/**
	 * 관리자 > 게시판 수정 
	 * @return doAdminBulletinList
	 */
	public String adminBulletinUpdate(){
		
		Map<String,Object> context = ActionContext.getContext().getContextMap();
		
		try{
			
			common(context);
		
			if(entity.containsKey("C_CATEGORY_NAME")) {
				
				ArrayList categoryList = new ArrayList();	
				if(entity.get("C_CATEGORY_NAME") instanceof String[]) {
					String[] delFlagArray = (String[]) entity.get("C_DEL_FLAG");
					String[] descriptionArray = (String[]) entity.get("C_DESCRIPTION");
					String[] categoryNameArray = (String[]) entity.get("C_CATEGORY_NAME");
					String[] categoryIdxArray = (String[]) entity.get("C_CATEGORY_IDX");
					
					int i=0;
					for(String str : categoryNameArray) {
						
						HashMap category = new HashMap();
						category.put("output_seq", (i+1));
						category.put("category_name", categoryNameArray[i]);
						category.put("description", descriptionArray[i]);
						category.put("del_flag", delFlagArray[i]);
						category.put("category_idx", categoryIdxArray[i]);
						
						categoryList.add(category);
						i++;
					}
					
				}else{
					HashMap category = new HashMap();
					category.put("output_seq", 1);
					category.put("category_name", entity.get("C_CATEGORY_NAME"));
					category.put("description", entity.get("C_DESCRIPTION"));
					category.put("del_flag", entity.get("C_DEL_FLAG"));
					category.put("category_idx", entity.get("C_CATEGORY_IDX"));
					
					categoryList.add(category);
				}
				
				entity.put("CATEGORY_LIST", categoryList);
			}
			bo.adminBulletinUpdate(entity);			
			
		} catch (NotAuthorizedException e) {
			return LOGIN;
		} catch(Exception e){
			e.printStackTrace();
		}
		return "doAdminBulletinList";
	}
	
	/**
	 * 게시판 기본 설정 데이터 불러오기
	 * @return
	 */
	public String adminBulletinSetup() {
		
		Map<String,Object> context = ActionContext.getContext().getContextMap();

		try {
			common(context);
			
			// ARTICLE_COUNT_PER_PAGE - 페이지당 글 수
			// BULLETIN_BASE_CATEGORY - 게시판 생성시 기본 카테고리
			
			Map map = new HashMap();
			
			int acpp = bo.adminPropertyCountPerPage();
			map.put("ARTICLE_COUNT_PER_PAGE", acpp);
			
			List bbcList = bo.adminPropertyBaseCategory();
			System.out.println(bbcList);
			map.put("BULLETIN_BASE_CATEGORY", bbcList);
			
			context.put("BULLETIN_SETUP", map);
			
		} catch (NotAuthorizedException e) {
			return LOGIN;
		}
		
		return SUCCESS;
	}
	
	/**
	 * 관리자 > 사용자 리스트 화면 
	 * @return SUCCESS
	 */
	public String adminUserList(){
		
		Map<String,Object> context = ActionContext.getContext().getContextMap();

		try{
			
			common(context);
			
		    int lineCnt	= 15;		    
			String nowPage	= (entity.get("nowPage")==null||"".equals((String)entity.get("nowPage")))?"1":(String)entity.get("nowPage");
									
			String searchMethod = entity.get("searchMethod")==null?"user_id":(String)entity.get("searchMethod");
			String searchWord = entity.get("searchWord")==null?"":(String)entity.get("searchWord");
			
			entity.put("lineCnt", lineCnt);
			entity.put("nowPage", nowPage);
			entity.put("offset", lineCnt*(Integer.parseInt(nowPage)-1));
			
			entity.put("searchMethod", searchMethod);
			entity.put("searchWord"  , searchWord);
			
			int totlCnt	= bo.adminUserListCnt(entity);
			List list = bo.adminUserList(entity);	
			
			String page = Navigator.pageNavi(totlCnt, Integer.parseInt(nowPage), lineCnt);
			
			context.put("USER_LIST", list);									
			context.put("PAGE", page);
			context.put("NUM", totlCnt - ((Integer.parseInt(nowPage) -1)*lineCnt));
		
		} catch (NotAuthorizedException e) {
			return LOGIN;
		} catch(Exception e){
			e.printStackTrace();
		}
		return SUCCESS;
	}	
	
	/**
	 * 관리자 > User 추가 
	 * @return
	 */
	public String adminUserAdd(){
		
		Map<String,Object> context = ActionContext.getContext().getContextMap();

		try{
					
			common(context);
			
		} catch (NotAuthorizedException e) {
			return LOGIN;	
		} catch(Exception e){
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	/**
	 * 관리자 > User 추가 
	 * @return
	 */
	public String adminUserInsert(){
		
		Map<String,Object> context = ActionContext.getContext().getContextMap();
		
		try{	
			
			common(context);
			
			if(!Validator.validate(entity, "user_id", "pwd", "user_name", "user_level", "homepage", "email", "description", "del_flag")) {
				return INPUT;
			}
			
			entity.put("pwd", Cryptograph.getCryptoStr(getText("enctype"), (String)entity.get("pwd")));
			
			bo.adminUserInsert(entity);
			
		} catch (NotAuthorizedException e) {
			return LOGIN;				
		} catch(Exception e){
			e.printStackTrace();
		}
		return "doAdminUserList";	
	}
	
	/**
	 * 일반 User 수정 
	 * @return SUCCESS
	 */
	public String adminUserEdit(){
		
		Map<String,Object> context = ActionContext.getContext().getContextMap();

		try{
			
			common(context);
			
			Map map = bo.adminUserInfo(entity);
			
			context.put("USER_INFO", map);
			
		} catch (NotAuthorizedException e) {
			return LOGIN;				
		} catch(Exception e){
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	/**
	 * 관리자 > User 수정
	 * @return doAdminUserList
	 */
	public String adminUserUpdate(){
		
		Map<String,Object> context = ActionContext.getContext().getContextMap();
		
		try{
					
			common(context);
			
			// 관리자가 User 정보 변경시 password를 넘기지 않으면 password 수정 안되게 변경 
			if( "".equals(((String)entity.get("pwd")).trim())){
				if(!Validator.validate(entity, "user_id","user_name", "user_level", "homepage", "email", "description", "del_flag")) {
					return INPUT;
				}
			} else {
				if(!Validator.validate(entity, "user_id", "pwd", "user_name", "user_level", "homepage", "email", "description", "del_flag")) {
					return INPUT;
				}
				entity.put("pwd", Cryptograph.getCryptoStr(getText("enctype"), (String)entity.get("pwd")));
			}
			bo.adminUserUpdate(entity);			
		
		} catch (NotAuthorizedException e) {
			return LOGIN;				
		} catch(Exception e){
			e.printStackTrace();
		}
		
		return "doAdminUserList";
	}
	
	/**
	 * 관리자 > 메시지 리스트 화면 
	 * @return SUCCESS
	 */
	public String adminMessageList(){
		
		Map<String,Object> context = ActionContext.getContext().getContextMap();

		try{
			
			common(context);
			
			Map session_map = (Map) ActionContext.getContext().getSession().get(SESSION_USER);
			entity.put("user_id", session_map.get("USER_ID"));
			
		    int lineCnt	= 15;		    
			String nowPage	= (entity.get("nowPage")==null||"".equals((String)entity.get("nowPage")))?"1":(String)entity.get("nowPage");
									
			String searchMethod = entity.get("searchMethod")==null?"user_id":(String)entity.get("searchMethod");
			String searchWord = entity.get("searchWord")==null?"":(String)entity.get("searchWord");
			
			entity.put("lineCnt", lineCnt);
			entity.put("nowPage", nowPage);
			entity.put("offset", lineCnt*(Integer.parseInt(nowPage)-1));
			
			entity.put("searchMethod", searchMethod);
			entity.put("searchWord"  , searchWord);
			
			int totlCnt	= bo.adminMessageListCnt(entity);
			List list = bo.adminMessageList(entity);	
			System.out.println(entity+ ", count:"+totlCnt+", "+list);
			
			String page = Navigator.pageNavi(totlCnt, Integer.parseInt(nowPage), lineCnt);
			
			context.put("MESSAGE_LIST", list);									
			context.put("PAGE", page);
			context.put("NUM", totlCnt - ((Integer.parseInt(nowPage) -1)*lineCnt));
				
		} catch (NotAuthorizedException e) {
			return LOGIN;				
		} catch(Exception e){
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	/**
	 * 관리자 > 발송 메시지 리스트 화면 
	 * @return SUCCESS
	 */
	public String adminSendMessageList(){
		
		Map<String,Object> context = ActionContext.getContext().getContextMap();

		try{
			
			common(context);
			
			Map session_map = (Map) ActionContext.getContext().getSession().get(SESSION_USER);
			entity.put("user_id", session_map.get("USER_ID"));
			
		    int lineCnt	= 15;		    
			String nowPage	= (entity.get("nowPage")==null||"".equals((String)entity.get("nowPage")))?"1":(String)entity.get("nowPage");
									
			String searchMethod = entity.get("searchMethod")==null?"user_id":(String)entity.get("searchMethod");
			String searchWord = entity.get("searchWord")==null?"":(String)entity.get("searchWord");
			
			entity.put("lineCnt", lineCnt);
			entity.put("nowPage", nowPage);
			entity.put("offset", lineCnt*(Integer.parseInt(nowPage)-1));
			
			entity.put("searchMethod", searchMethod);
			entity.put("searchWord"  , searchWord);
			
			int totlCnt	= bo.adminSendMessageListCnt(entity);
			List list = bo.adminSendMessageList(entity);	
			System.out.println(entity+ ", count:"+totlCnt+", "+list);
			
			String page = Navigator.pageNavi(totlCnt, Integer.parseInt(nowPage), lineCnt);
			
			context.put("MESSAGE_LIST", list);									
			context.put("PAGE", page);
			context.put("NUM", totlCnt - ((Integer.parseInt(nowPage) -1)*lineCnt));
				
		} catch (NotAuthorizedException e) {
			return LOGIN;				
		} catch(Exception e){
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	/**
	 * 관리자 > 메시지 작성 화면 
	 * @return SUCCESS
	 */
	public String adminMessageAdd(){
		
		Map<String,Object> context = ActionContext.getContext().getContextMap();

		try{
			
			common(context);
			
			List list = bo.adminManagerList(entity);
			
			context.put("MANAGER_LIST", list);
				
		} catch (NotAuthorizedException e) {
			return LOGIN;				
		} catch(Exception e){
			e.printStackTrace();
		}
		return SUCCESS;
	}	
	
	/**
	 * 관리자 > 메시지 등록 화면 
	 * @return SUCCESS
	 */
	public String adminMessageInsert(){
		
		Map<String,Object> context = ActionContext.getContext().getContextMap();

		try{
			
			common(context);
			
			Map userInfoMap = (Map) ActionContext.getContext().getSession().get(SESSION_USER);
			String user_id = (String)userInfoMap.get("USER_ID");
			
			entity.put("status", "0");
			entity.put("user_id", user_id);
			
			int count = bo.adminMessageInsert(entity);
			
			context.put("RESULT", "true");
			
		} catch (NotAuthorizedException e) {
			return LOGIN;				
		} catch(Exception e){
			e.printStackTrace();
		}
		return "resultText";
	}	
	
	public String adminPropertyList() {
		Map<String,Object> context = ActionContext.getContext().getContextMap();

		try{
			
			common(context);
			
			String searchMethod = entity.get("searchMethod")==null?"property_id":(String)entity.get("searchMethod");
			String searchWord = entity.get("searchWord")==null?"":(String)entity.get("searchWord");
			
			entity.put("searchMethod", searchMethod);
			entity.put("searchWord"  , searchWord);
			
			List list = bo.adminPropertyList(entity);
			
			context.put("PROPERTY_LIST", list);
						
		} catch (NotAuthorizedException e) {
			return LOGIN;				
		} catch(Exception e){
			e.printStackTrace();
		}
		return SUCCESS;	
	}
	
	/**
	 * 프로퍼티 업데이트 
	 * @return
	 */
	public String adminPropertyUpdate(){
		
		Map<String,Object> context = ActionContext.getContext().getContextMap();
		
		String result = "doAdminBulletinSetup";
		
		try{	

			common(context);
			
			String propertyId = (String) entity.get("property_id");
			if("BULLETIN_BASE_CATEGORY".equals(propertyId)) {
				if(entity.containsKey("C_CATEGORY_NAME")) {
					
					ArrayList categoryList = new ArrayList();	
					
					if(entity.get("C_CATEGORY_NAME") instanceof String[]) {
						String[] delFlagArray = (String[]) entity.get("C_DEL_FLAG");
						String[] descriptionArray = (String[]) entity.get("C_DESCRIPTION");
						String[] categoryNameArray = (String[]) entity.get("C_CATEGORY_NAME");
						
						int i=0;
						for(String str : categoryNameArray) {
							
							HashMap category = new HashMap();
							category.put("output_seq", (i+1));
							category.put("category_name", categoryNameArray[i]);
							category.put("description", descriptionArray[i]);
							category.put("del_flag", delFlagArray[i]);
							
							categoryList.add(category);
							i++;
						}
						
					}else{
						HashMap category = new HashMap();
						category.put("output_seq", 1);
						category.put("category_name", entity.get("C_CATEGORY_NAME"));
						category.put("description", entity.get("C_DESCRIPTION"));
						category.put("del_flag", entity.get("C_DEL_FLAG"));
						
						categoryList.add(category);
					}
					
					ObjectMapper mapper = new ObjectMapper();
					String json = mapper.writeValueAsString(categoryList);
					
					entity.put("property_value", json);
				}
			}else if("SYSTEM_EMAIL".equals(propertyId)) {
				
				result = "adminPropertySystemEmail";
				
				HashMap email = new HashMap();
				
				if(((String)entity.get("admin_email")).trim().length() > 0) {
					email.put("admin_email", Cryptograph.getBASE64Encoder((String)entity.get("admin_email")));
    			}
    			if(((String)entity.get("admin_email_pwd")).trim().length() > 0) {
    				email.put("admin_email_pwd", Cryptograph.getBASE64Encoder((String)entity.get("admin_email_pwd")));
    			}
    			
    			ObjectMapper mapper = new ObjectMapper();
				String json = mapper.writeValueAsString(email);
				
				entity.put("property_value", json);
			}
			
			bo.adminPropertyUpdate(entity);

		} catch (NotAuthorizedException e) {
			return LOGIN;				
		} catch(Exception e){
			e.printStackTrace();
		}
		
		return result;	
	}
	
	public String adminPropertySystemEmail(){
		
		Map<String,Object> context = ActionContext.getContext().getContextMap();

		try{
			
			common(context);
			
			Map map = bo.adminPropertySystemEmail();
			
			context.put("SETUP", map);
						
		} catch (NotAuthorizedException e) {
			return LOGIN;				
		} catch(Exception e){
			e.printStackTrace();
		}
		return SUCCESS;	
	}
	
	
	public String adminSampleExcelInsert() {
		
		Map<String,Object> context = ActionContext.getContext().getContextMap();
		
		try{
			
			common(context);
			
			HttpParameters httpParameters = ActionContext.getContext().getParameters();
			Parameter parameter = httpParameters.get("file");
			org.apache.struts2.dispatcher.multipart.UploadedFile[] ff = (org.apache.struts2.dispatcher.multipart.UploadedFile[])parameter.getObject();
			
			File f = (File)ff[0].getContent();
			Workbook mWorkbook = new XSSFWorkbook(f);
			
			Sheet mSheet = mWorkbook.getSheetAt(0);
			Iterator<Row> rowIterator = mSheet.iterator(); 
			
			int rowNum = 0;
	        while (rowIterator.hasNext()) {
	        	rowNum++;
	        	
	            Row row = rowIterator.next();
	            
	            if(rowNum == 1) continue;
	            
	            Cell cellConketId = row.getCell(0);
	            Cell cellConcertIdx = row.getCell(1);
	          
	            System.out.println(cellConketId+ " : "+cellConcertIdx);
	           
	        }
			
	        mWorkbook.close();
						
		} catch (NotAuthorizedException e) {
			return LOGIN;				
		} catch(Exception e){
			e.printStackTrace();
		}
		return "doAdminSampleExcel";	
	}
	
	
	
	public String adminSampleExcel(){
		
		Map<String,Object> context = ActionContext.getContext().getContextMap();

		try{	
			
			common(context);
			
			/*List list = bo.adminSampleExcel();
			
			context.put("CALENDAR", list);*/
						
		} catch (NotAuthorizedException e) {
			return LOGIN;				
		} catch(Exception e){
			e.printStackTrace();
		}
		return SUCCESS;	
	}
	
}