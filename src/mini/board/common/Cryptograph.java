package mini.board.common;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Cryptograph {
	
	/**
	 * BASE64 부호화
	 * @param str 
	 * @return
	 */
	public static String getBASE64Encoder(String str){
	    BASE64Encoder encoder = new BASE64Encoder();
	    return encoder.encode(str.getBytes());	   
	}
	
	/**
	 * BASE64 복호화
	 * @param str 부호화된 텍스트
	 * @return
	 * @throws IOException 
	 */
	public static String getBASE64Decoder(String str) throws IOException{
		BASE64Decoder decoder = new BASE64Decoder();	        
	    byte[] b1 = decoder.decodeBuffer(str);
	    return new String(b1);	   
	}
	
	/**
	 * MD5, SHA1로 암호화
	 * @param enc MD5, SHA1 중 하나
	 * @param str 암호화될 텍스트
	 * @return
	 */
	public static String getCryptoStr(String enc, String str) throws NoSuchAlgorithmException{		
		String cryptoStr = "";
		
		MessageDigest md = MessageDigest.getInstance(enc);
		md.update(str.getBytes());
		byte[] digest = md.digest();	
			
		for(int i = 0 ; i < digest.length ; i++){
			cryptoStr += Integer.toHexString(digest[i] & 0xff).toUpperCase();
		}				
		
		return cryptoStr;
	}
	
}
