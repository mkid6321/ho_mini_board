package mini.board.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import java.util.Calendar;

public class FileService {
	public void makeBasePath(String path){
		File dir = new File(path);
		if(!dir.exists())dir.mkdirs();
	}
	
	public String saveFile(File file, String basePath, String fileName) throws IOException {
		if(null==file || file.getName().equals("") || file.length() <=0) return null;
		
		makeBasePath(basePath);
		
		Calendar cal = Calendar.getInstance();
	    String rename = fileName+"-"+cal.getTimeInMillis();
	    
		String serverFullPath = basePath + "/" + rename;
		
		FileInputStream fis = new FileInputStream(file);
		FileOutputStream fos = new FileOutputStream(serverFullPath);
		
		int bytesRead = 0;
		byte[] buffer = new byte[1024];
		while((bytesRead = fis.read(buffer, 0, 1024)) != -1) {
			fos.write(buffer, 0, bytesRead);
		}
		
		fos.close();
		fis.close();
		
		return rename;
	}

}
