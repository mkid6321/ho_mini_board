package mini.board.common;

import java.util.Iterator;
import java.util.Map;

/**
 * 
 * @author star_namu
 * 안녕하세요
 *
 */
public class Validator {
	
	public static boolean validate(Map<String, String> entity, String... params) {
		
		for(String param : params) {
			boolean isExist = entity.containsKey(param);
			
			if(isExist) {
				boolean b = rules(param, entity.get(param));
				if(!b) return false;
			}else{
				return false;	
			}
		}
		
		return true;
	}
	
	/**
	 * 파라미터맵의 모든 파라미터를 체크합니다. 
	 * @param entity 파라미터 맵
	 * @return 체크대상인 파라미터가 모두 유효할 경우 true, 아닐 경우 false
	 */
	public static boolean validate(Map<String, String> entity) {

		Iterator<String> iterator = entity.keySet().iterator();
		
		while(iterator.hasNext()){
			String param = iterator.next();
			if(!rules(param, entity.get(param))){
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * 파라미터 별 규칙을 적용합니다.
	 * @param key 파라미터 key
	 * @param value 파리미터 value
	 * @return 해당 파라미터의 규칙에 적합하면 true, 그렇지않으면 false
	 */
	private static boolean rules(String key, String value) {

		if("user_id".equals(key)) {
			if(!Regex.isIdType(value)) {
				System.out.println("The user_id must be at least 5 characters 16 characters or less. No Whitespace.");
				return false;
			}
		}else if("pwd".equals(key)) {
			if(value.length() < 8 || value.length() > 16) {
				System.out.println("The pwd must be at least 8 characters 16 characters or less.");
				
				return false;
			}
		}else if("name".equals(key)) {
			if(value.length() == 0) {
				System.out.println("The name should not be blank.");
				
				return false;
			}
		}else if("email".equals(key)) {
			if(!Regex.isEmail(value)) {
				System.out.println("The email is not valid.");
				
				return false;
			}
		}else if("homepage".equals(key)) {
			if(value.trim().length() != 0 && !Regex.isURL(value)) {
				System.out.println("The homepage is not valid");
				
				return false;
			}
		}else if("title".equals(key)) {
			if(value.length() == 0) {
				System.out.println("The title should not be blank.");
				
				return false;
			}
		}else if("group_name".equals(key)) {
			if(value.length() == 0) {
				System.out.println("The group_name should not be blank.");
				
				return false;
			}
		}else if("user_name".equals(key)) {
			if(value.length() == 0) {
				System.out.println("The user_name should not be blank.");
				
				return false;
			}
		}else if("user_level".equals(key)) {
			try{
				int userLevel = Integer.parseInt(value);
				
				if(userLevel < 0 || userLevel > 255) {
					System.out.println("The user_level should be less than 0 and 255.");
					
					return false;
				}
			}catch(NumberFormatException e) {
				System.out.println("The user_level must be a numeric type.");
				
				return false;
			}
		}
		
		return true;
	}

}
