package mini.board.common;

import java.io.IOException;
import java.util.Date;
import java.util.Locale;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

public class DateTag extends TagSupport {
	
	private String locale;
	private Date date;
	
	public int doStartTag() throws JspException {
		
		JspWriter out = pageContext.getOut();
		
		DurationFromNow.setLocale(Locale.KOREAN);
		
		try {
			out.println(DurationFromNow.getTimeDiffLabel(date));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return SKIP_BODY;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

}
