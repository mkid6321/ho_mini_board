package mini.board.common;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonTag extends TagSupport {
	
	private Object object;
	
	public int doStartTag() throws JspException {
		
		JspWriter out = pageContext.getOut();
		
		try {
			
			ObjectMapper mapper = new ObjectMapper();
			String json = mapper.writeValueAsString(object);
			out.println(json);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return SKIP_BODY;
	}

	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}

}
