package mini.board.common;

import java.util.regex.Pattern;

public class Regex {
	
	/**
	 * 정상적인 이메일 인지 검증.
	 */
    public static boolean isEmail(String email) {
        if (email==null) return false;
        boolean b = Pattern.matches(
            "[\\w\\~\\-\\.]+@[\\w\\~\\-]+(\\.[\\w\\~\\-]+)+", 
            email.trim());
        return b;
    }

	/**
	 * 정상적인 URL 인지 검증.
	 */
    public static boolean isURL(String url) {
        if (url==null) return false;
        boolean b = Pattern.matches(
        		"^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]", 
            url.trim());
        return b;
    }
    
    /**
	 * 공백을 가지고 있는지 검증.
	 */
    public static boolean isTrimmed(String str) {
        if (str==null) return false;
        boolean b = str.indexOf(" ")==-1;
        return b;
    }
    
    /**
	 * ID 형식인지 검증(최소 5자에서 16자까지 공백없는 문자).
	 */
    public static boolean isIdType(String str) {
    	if (str==null) return false;
        boolean b = Pattern.matches(
        		"^[a-z0-9_-]{5,16}$", 
        		str);
        return b;
    }
    
    
}
