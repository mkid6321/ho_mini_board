package mini.board.common;

public class Navigator {

	public static String pageNavi(int totlCnt, int nowPage, int lineCnt) {
		String pageNavi = "<ul class=\"pagination\">";

		int totalPageCnt = (totlCnt / lineCnt) + (totlCnt % lineCnt > 0 ? 1 : 0);
		int startPageNo = nowPage % lineCnt == 0 ? ((nowPage - 1) / lineCnt) * lineCnt + 1
				: (nowPage / lineCnt) * lineCnt + 1;
		int endPageNo = nowPage % lineCnt == 0 ? Math.min(((nowPage - 1) / lineCnt + 1) * lineCnt, totalPageCnt)
				: Math.min((nowPage / lineCnt + 1) * lineCnt, totalPageCnt);

		if (nowPage > 1) {
			pageNavi = pageNavi + "<li class=\"page-item\"><a class=\"page-link\" href=\"javascript:jsGoPage(" + (nowPage - 1) + ")\">PREV</a></li>";
		} else {
			pageNavi = pageNavi + "<li class=\"page-item\"><a class=\"page-link\" href=\"#\">PREV</a></li>";
		}

		if (startPageNo != 1) {
			pageNavi = pageNavi + "<li class=\"page-item\"><a class=\"page-link\" href=\"javascript:jsGoPage(1)\">1</a></li>";
		}

		String aa = "";
		for (int i = startPageNo; i <= endPageNo; i++) {
			if (i == nowPage) {
				aa += "<li class=\"page-item\"><a class=\"page-link\" href=\"javascript:jsGoPage(" + i + ")\"><span class='selected'>" + i + "</span></a></li>";
			} else {
				aa += "<li class=\"page-item\"><a class=\"page-link\" href=\"javascript:jsGoPage(" + i + ")\">" + i + "</a></li>";
			}
		}

		pageNavi = pageNavi + aa;

		if (endPageNo != totalPageCnt) {
			pageNavi = pageNavi + "<li class=\"page-item\"><a class=\"page-link\" href=\"javascript:jsGoPage(" + totalPageCnt + ")\">" + totalPageCnt + "</a></li>";
		}

		if (nowPage < totalPageCnt) {
			pageNavi = pageNavi + "<li class=\"page-item\"><a class=\"page-link\" href=\"javascript:jsGoPage(" + (nowPage + 1) + ")\">NEXT &#187;</a></li>";
		} else {
			pageNavi = pageNavi + "<li class=\"page-item\"><a class=\"page-link\" href=\"#\">NEXT</a></li>";
		}

		pageNavi = pageNavi + "</ul>";

		return pageNavi;
	}

/*
	public static String pageNavi(int totlCnt, int nowPage, int lineCnt) {
		String pageNavi = "<span class='paging'>";

		int totalPageCnt = (totlCnt / lineCnt) + (totlCnt % lineCnt > 0 ? 1 : 0);
		int startPageNo = nowPage % lineCnt == 0 ? ((nowPage - 1) / lineCnt) * lineCnt + 1
				: (nowPage / lineCnt) * lineCnt + 1;
		int endPageNo = nowPage % lineCnt == 0 ? Math.min(((nowPage - 1) / lineCnt + 1) * lineCnt, totalPageCnt)
				: Math.min((nowPage / lineCnt + 1) * lineCnt, totalPageCnt);

		if (nowPage > 1) {
			pageNavi = pageNavi + "<a href='javascript:jsGoPage(" + (nowPage - 1) + ")'>&#171; PREV</a> : ";
		} else {
			pageNavi = pageNavi + "&#171; PREV : ";
		}

		if (startPageNo != 1) {
			pageNavi = pageNavi + "<a href='javascript:jsGoPage(1)'>1</a> ... ";
		}

		String aa = "";
		for (int i = startPageNo; i <= endPageNo; i++) {
			if (i == nowPage) {
				aa += " <a href='javascript:jsGoPage(" + i + ")'><span class='selected'>" + i + "</span></a> : ";
			} else {
				aa += " <a href='javascript:jsGoPage(" + i + ")'>" + i + "</a> : ";
			}
		}

		pageNavi = pageNavi + aa;

		if (endPageNo != totalPageCnt) {
			pageNavi = pageNavi + "... <a href='javascript:jsGoPage(" + totalPageCnt + ")'>" + totalPageCnt + "</a>";
		}

		if (nowPage < totalPageCnt) {
			pageNavi = pageNavi + " <a href='javascript:jsGoPage(" + (nowPage + 1) + ")'>NEXT &#187;</a>";
		} else {
			pageNavi = pageNavi + " NEXT &#187;";
		}

		pageNavi = pageNavi + "</span>";

		return pageNavi;
	}
*/
}
