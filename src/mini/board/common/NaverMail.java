package mini.board.common;

import java.io.File;
import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class NaverMail {

	File attach;
	
	String mail, pwd;
	
	public NaverMail(String mail, String pwd) {
		this.mail = mail;
		this.pwd = pwd;
	}
	
	public NaverMail(String mail, String pwd, File file) {
		this(mail, pwd);
		attach = file;
	}

	public boolean sendMail(String receiver, String title, String contents) {
		Properties p = System.getProperties();
		p.put("mail.smtp.user" , mail);
		p.put("mail.smtp.host", "smtp.naver.com");
		p.put("mail.smtp.port", "465");
		p.put("mail.smtp.starttls.enable", "true");
		p.put("mail.smtp.auth", "true");
		p.put("mail.smtp.debug", "true");
		p.put("mail.smtp.socketFactory.port", "465");
		p.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		p.put("mail.smtp.socketFactory.fallback", "false");
           
        Authenticator auth = new NaverAuthentication(mail+"@naver.com", pwd);
         
        //session 생성 및  MimeMessage생성
        Session session = Session.getDefaultInstance(p, auth);
        MimeMessage msg = new MimeMessage(session);
         
        try{
            //편지보낸시간
            msg.setSentDate(new Date());
             
            InternetAddress from = new InternetAddress(mail+"<"+mail+"@naver.com>"); //보내는 사람
             
            // 이메일 발신자
            msg.setFrom(from);
             
            // 이메일 수신자
            InternetAddress to = new InternetAddress(receiver);
            msg.setRecipient(Message.RecipientType.TO, to);
             
            // 이메일 제목
            msg.setSubject(title, "UTF-8");
             
            // 이메일 내용 
            msg.setText(contents, "UTF-8");
             
            // 이메일 헤더 
            msg.setHeader("content-Type", "text/html");
            
            if(attach != null) {
            	BodyPart messageBodyPart = new MimeBodyPart(); 
                DataSource source = new FileDataSource(attach.getAbsolutePath()); 
                messageBodyPart.setDataHandler(new DataHandler(source)); 
                messageBodyPart.setFileName(attach.getName()); 
             
                MimeMultipart multipart = new MimeMultipart(); 
                multipart.addBodyPart(messageBodyPart); 
                msg.setContent(multipart);
            }
            
            //메일보내기
            javax.mail.Transport.send(msg);
             
            return true;
        }catch (AddressException addr_e) {
            addr_e.printStackTrace();
        }catch (MessagingException msg_e) {
            msg_e.printStackTrace();
        }
        
        return false;
	}
 
}
 
 
class NaverAuthentication extends Authenticator {
      
    PasswordAuthentication pa;
 
    /**
     * 
     * @param id Naver mail address
     * @param pwd Naver password
     */
    public NaverAuthentication(String id, String pwd){
         
        // ID와 비밀번호를 입력한다.
        pa = new PasswordAuthentication(id, pwd);
    }
 
    // 시스템에서 사용하는 인증정보
    public PasswordAuthentication getPasswordAuthentication() {
        return pa;
    }
}