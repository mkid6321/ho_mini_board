package mini.board.common;

import java.io.IOException;
import java.io.Reader;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

public class BaseBusiness {
	protected final SqlSessionFactory sqlSessionFactory;
	   
	public BaseBusiness(){
		try{
	    	Reader reader = Resources.getResourceAsReader("mybatis.xml");
	        sqlSessionFactory = new SqlSessionFactoryBuilder().build(reader);
	        reader.close();
    	}catch (IOException e) {
    		throw new RuntimeException("Something bad happened while building the SqlSessionFactory instance." + e, e);
		}  
	}
}