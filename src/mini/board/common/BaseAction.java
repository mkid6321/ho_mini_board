package mini.board.common;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.dispatcher.HttpParameters;
import org.apache.struts2.dispatcher.Parameter;
import org.apache.struts2.interceptor.HttpParametersAware;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;

import com.opensymphony.xwork2.ActionSupport;

public class BaseAction extends ActionSupport implements HttpParametersAware,ServletRequestAware,ServletResponseAware {
		
	public String SESSION_USER = "SESSION_USER_INFO"; 
	public String SESSION_BULLETIN = "SESSION_BULLETIN_INFO"; 
	
	public Map entity;
	
	public HttpServletRequest request;
	public HttpServletResponse response;

	@Override
	public void setParameters(HttpParameters parameters) {
		this.entity = new HashMap();
		Iterator<Entry<String, Parameter>> it = parameters.entrySet().iterator();
		while(it.hasNext()){
			Map.Entry<String, Parameter> dto = (Map.Entry<String, Parameter>)it.next();		
			entity.put(dto.getKey(), (dto.getValue().isMultiple())? (dto.getValue().getMultipleValues().length > 1 ? dto.getValue().getMultipleValues():dto.getValue().getMultipleValues()[0]) :dto.getValue().getValue());
		}
	}
	
	/**
	 * parameters context를 통해 값을 가져올때 본 함수를 통해서 가져간다.
	 * @return entity 파라미터맵
	 */
	public Map getParameters(){
		return entity;
	}
	
	@Override
	public void setServletRequest(HttpServletRequest request){	
	    this.request = request;
	}

	public HttpServletRequest getServletRequest(){
	    return request;
	}

	@Override
	public void setServletResponse(HttpServletResponse response){
	    this.response = response;
	}

	public HttpServletResponse getServletResponse(){
	    return response;
	}

}
