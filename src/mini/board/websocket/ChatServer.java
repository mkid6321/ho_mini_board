package mini.board.websocket;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;
import javax.websocket.EndpointConfig;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint(value="/websocket", configurator = GetHttpSessionConfigurator.class)
public class ChatServer {
    
    private static HashMap<String,ChatServer> sessions = new HashMap<String,ChatServer>();

    private Session session;
    private HttpSession httpSession;
    
    private String userId;

    @OnOpen
    public void onOpen(Session session, EndpointConfig config) {
        this.session = session;
        this.httpSession = (HttpSession) config.getUserProperties().get(HttpSession.class.getName());
        
        Map user = (Map)httpSession.getAttribute("SESSION_USER_INFO");
    	userId = (String)user.get("USER_ID");
        sessions.put(userId, this);
        
        System.out.println("[new connected] online:" + sessions.size());
    }

    /**
     * 연결이 닫혔을때 호출됨
     */
    @OnClose
    public void onClose(){
    	
    	sessions.remove(this.userId);
    	
        System.out.println("[somebody disconnected] online:" + sessions.size());
    }

    @OnMessage
    public void onMessage(String message, Session session) {
        System.out.println("onMessage:" + message);
        try {
        	
        	String[] msg = message.split(" ", 2);
        	String receiver = msg[0].replace("to.", "");
        	
        	ChatServer item = sessions.get(receiver);
        	
        	System.out.println("onMessage : from "+userId+ ", to "+receiver+"(online? "+(item != null)+"), msg "+msg[1]);
        	
    		if(item != null) item.sendMessage(msg[1]);
    		
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @OnError
    public void onError(Session session, Throwable t){
        System.out.println(t.getMessage());
        System.out.println(t.getCause());
        t.printStackTrace();
    }

    public void sendMessage(String message) throws IOException{
        this.session.getBasicRemote().sendText(message);
        //this.session.getAsyncRemote().sendText(message);
    }

}