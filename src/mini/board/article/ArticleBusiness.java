package mini.board.article;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import mini.board.common.BaseBusiness;
import mini.board.common.Cryptograph;

import org.apache.ibatis.exceptions.PersistenceException;
import org.apache.ibatis.session.SqlSession;

public class ArticleBusiness extends BaseBusiness{

	private static ArticleBusiness instance = null;	
     
    public ArticleBusiness() {
    	super();
    }

    public synchronized static ArticleBusiness getInstance(){
         if(instance == null){	instance = new ArticleBusiness();	}
         return instance;
    }    
    
    public int articleCountPerPage() {
		SqlSession session = sqlSessionFactory.openSession();
		
		int cnt = 0;
    	try {
    		
    		Map map = (Map)session.selectOne("adminProperty", "ARTICLE_COUNT_PER_PAGE");
    		cnt = Integer.parseInt((String) map.get("PROPERTY_VALUE"));
    	}finally {
    		session.close();
    	}	
		
		return cnt;
	}
    
    public int articleListCnt(Map entity) {    	
    	SqlSession session = sqlSessionFactory.openSession();
    	
    	int cnt;
    	try {
    		cnt = (Integer)session.selectOne("articleListCnt",entity);
    	}finally {
    		session.close();
    	}	
    	    	    	
    	return cnt;
    }
    
    public List articleList(Map entity) {
    	SqlSession session = sqlSessionFactory.openSession();
    	
    	List list;
    	try {
    		list = session.selectList("articleList",entity);
    	}finally {
    		session.close();
    	}	
    	
    	return list;
    }
    
    public List articleNList(Map entity) {    	
    	SqlSession session = sqlSessionFactory.openSession();
    	
    	List list;
    	try {
    		list = session.selectList("articleNList",entity);	
    	}finally {
    		session.close();
    	}	
    	    	    	
    	return list;
    }
   
    public void articleInsert(Map entity) {
    	SqlSession session = sqlSessionFactory.openSession();
    	
    	try {
    		int cnt = session.insert("articleInsert",entity);
    		// if(cnt == 0) exception; 
    		
        	List imageFileList = (List)entity.get("imageFileList");
        	List fileFileList = (List)entity.get("fileFileList");
        	
        	Map tempMap = null;
        	for(int i=0; i<imageFileList.size(); i++){
        		tempMap = (Map)imageFileList.get(i);
        		tempMap.put("article_idx", entity.get("LAST_INSERT_ID"));
        		int fidx = session.insert("articleFileInsert",tempMap);
        	}
        	for(int i=0; i<fileFileList.size(); i++){
        		tempMap = (Map)fileFileList.get(i);
        		tempMap.put("article_idx", entity.get("LAST_INSERT_ID"));
        		int fidx = session.insert("articleFileInsert",tempMap);
        	}
        	
        	session.commit();
    	}catch(PersistenceException e) {
    		session.rollback();
    		e.printStackTrace();
    	}finally {
    		session.close();
    	}	
    }
    
    public Map articleView(Map entity) {
    	SqlSession session = sqlSessionFactory.openSession();
    	
    	Map map;
    	try {
    		map = (Map)session.selectOne("articleView",entity);
        	
        	List list = session.selectList("articleViewFile",entity);    	
        	map.put("FILELIST", list);
    	}finally {
    		session.close();
    	}	
    	
    	return map;
    }
    
    public int articleAddCnt(Map entity) {
    	SqlSession session = sqlSessionFactory.openSession();
    	
    	int cnt = 0;
    	try {
    		cnt = session.update("articleAddCnt",entity);
    		
    		session.commit();
    	}catch(PersistenceException e) {
    		session.rollback();
    		e.printStackTrace();
    	}finally {
    		session.close();
    	}	
    	
    	return cnt;
    }
    
    public void articleCommentReplyInsert(Map entity) {
    	SqlSession session = sqlSessionFactory.openSession();
    	
    	try {
    		
        	Map map = (Map)session.selectOne("articleCommentView",entity);
        	
        	entity.put("origin_idx", (Integer)map.get("ORIGIN_IDX"));
        	entity.put("reply_order", (Integer)map.get("REPLY_ORDER"));
        	entity.put("depth", (Integer)map.get("DEPTH"));
        	
    		session.update("articleCommentReplyOrder", entity);
    		session.insert("articleCommentReply", entity);
    		
    		session.commit();
    	}catch(PersistenceException e) {
    		session.rollback();
    		e.printStackTrace();
    	}finally {
    		session.close();
    	}	
    }	
    
    public void articleReplyInsert(Map entity) {
    	SqlSession session = sqlSessionFactory.openSession();
    	
    	try {
    		
    		session.update("articleReplyOrder", entity);
    		int cnt = session.insert("articleReply", entity);
    		
    		List imageFileList = (List)entity.get("imageFileList");
        	List fileFileList = (List)entity.get("fileFileList");
        	
        	Map tempMap = null;
        	for(int i=0; i<imageFileList.size(); i++){
        		tempMap = (Map)imageFileList.get(i);
        		tempMap.put("article_idx", entity.get("LAST_INSERT_ID"));
        		session.insert("articleFileInsert",tempMap);
        	}
        	for(int i=0; i<fileFileList.size(); i++){
        		tempMap = (Map)fileFileList.get(i);
        		tempMap.put("article_idx", entity.get("LAST_INSERT_ID"));
        		session.insert("articleFileInsert",tempMap);
        	}
    		
    		session.commit();
    	}catch(PersistenceException e) {
    		session.rollback();
    		e.printStackTrace();
    	}finally {
    		session.close();
    	}	
    }

    public void articleUpdate(Map entity) {
    	SqlSession session = sqlSessionFactory.openSession();
    	
    	try {
    		session.update("articleUpdate", entity);
        	
        	List imageFileList = (List)entity.get("imageFileList");
        	List fileFileList = (List)entity.get("fileFileList");
        	List deleteFileList = (List)entity.get("deleteFileList");
        	
        	Map tempMap = null;
        	for(int i=0; i<imageFileList.size(); i++){
        		tempMap = (Map)imageFileList.get(i);
        		tempMap.put("article_idx", (String)entity.get("article_idx"));
        		session.insert("articleFileInsert",tempMap);
        	}
        	for(int i=0; i<fileFileList.size(); i++){
        		tempMap = (Map)fileFileList.get(i);
        		tempMap.put("article_idx", (String)entity.get("article_idx"));
        		session.insert("articleFileInsert",tempMap);
        	}
        	
        	for(int i=0; i<deleteFileList.size(); i++){
        		tempMap = (Map)deleteFileList.get(i);
        		session.update("articleFileDelete",tempMap);
        	}
        	
        	session.commit();
    	}catch(PersistenceException e) {
    		session.rollback();
    		e.printStackTrace();
    	}finally {
    		session.close();
    	}	
    }

    public int articleDelete(Map entity) {
    	SqlSession session = sqlSessionFactory.openSession();
    	
    	int cnt = 0;
    	try {
    		cnt = session.update("articleDelete", entity);
    		
    		session.commit();
    	}catch(PersistenceException e) {
    		session.rollback();
    		e.printStackTrace();
    	}finally {
    		session.close();
    	}	

    	return cnt;
    }
    
    public List articleFileList(Map entity) {
    	SqlSession session = sqlSessionFactory.openSession();
    	
    	List list;
    	try {
    		list = session.selectList("articleViewFile",entity);
    	}finally {
    		session.close();
    	}	

    	return list;
    }
    
    public Map articleFileView(Map entity) {
    	SqlSession session = sqlSessionFactory.openSession();
    	
    	Map map;
    	try {
    		map = (Map)session.selectOne("articleFileView",entity);
    	}finally {
    		session.close();
    	}	

    	return map;
    }
    
    public int articleFileDelete(Map entity) {
    	SqlSession session = sqlSessionFactory.openSession();
    	
    	int cnt = 0;
    	try {
    		cnt = (Integer)session.delete("articleFileDelete", entity);
    		
    		session.commit();
    	}catch(PersistenceException e) {
    		session.rollback();
    		e.printStackTrace();
    	}finally {
    		session.close();
    	}	

    	return cnt;
    }
    
    public List articleCommentList(Map entity) {
    	SqlSession session = sqlSessionFactory.openSession();
    	
    	List list;
    	try {
    		list = session.selectList("articleCommentList",entity);
    	}finally {
    		session.close();
    	}	

    	return list;
    }  
    
    public int articleCommentInsert(Map entity) {
    	SqlSession session = sqlSessionFactory.openSession();
    	
    	int cnt = 0;
    	try {
    		cnt = session.insert("articleCommentInsert", entity);	
    		
    		session.commit();
    	}catch(PersistenceException e) {
    		session.rollback();
    		e.printStackTrace();
    	}finally {
    		session.close();
    	}
    	
    	return cnt;
    }

    public int articleCommentDelete(Map entity) {
    	SqlSession session = sqlSessionFactory.openSession();
    	
    	int cnt = 0;
    	try {
    		cnt = session.update("articleCommentDelete", entity);
    		
    		session.commit();
    	}catch(PersistenceException e) {
    		session.rollback();
    		e.printStackTrace();
    	}finally {
    		session.close();
    	}	

    	return cnt;
    }
    
    public int articleCommentUpdate(Map entity) {
    	SqlSession session = sqlSessionFactory.openSession();
    	
    	int cnt = 0;
    	try {
    		cnt = session.update("articleCommentUpdate", entity);
    		
    		session.commit();
    	}catch(PersistenceException e) {
    		session.rollback();
    		e.printStackTrace();
    	}finally {
    		session.close();
    	}	
    	
    	return cnt;
    }

    public Map articleLogin(Map entity) {
    	SqlSession session = sqlSessionFactory.openSession();
    	
    	Map map;
    	try {
    		map = (Map)session.selectOne("articleLogin",entity);
    	}finally {
    		session.close();
    	}	

    	return map;
    }
    
    public int articleJoin(Map entity) {
    	SqlSession session = sqlSessionFactory.openSession();
    	
    	int cnt = 0;
    	try {
    		cnt = session.insert("articleJoin",entity);	
    		
    		session.commit();
    	}catch(PersistenceException e) {
    		session.rollback();
    		e.printStackTrace();
    	}finally {
    		session.close();
    	}	

    	return cnt;
    }
    
    public boolean articleCheckId(Map entity) {
    	SqlSession session = sqlSessionFactory.openSession();
    	
    	boolean result = false;
    	try {
        	int cnt = (Integer)session.selectOne("articleCheckId",entity);
        	if(cnt == 1){result = true;}
    	}finally {
    		session.close();
    	}	
    	
    	return result;
    }

    public List articleBulletinList(Map entity) {
    	SqlSession session = sqlSessionFactory.openSession();
    	
    	List list;
    	try {
    		list = session.selectList("articleBulletinList",entity);
    	}finally {
    		session.close();
    	}	

    	return list;
    }
    
    public List articleCategory(Map entity)  {
    	SqlSession session = sqlSessionFactory.openSession();
    	
    	List list;
    	try {
    		list = session.selectList("articleCategory", entity);	
    	}finally {
    		session.close();
    	}	

    	return list;
    }
    
    public Map articleGroupInfo(Map entity)  {
    	SqlSession session = sqlSessionFactory.openSession();
    	
    	Map map;
    	try {
    		map = (Map)session.selectOne("articleGroupInfo", entity);	
    	}finally {
    		session.close();
    	}	
    	return map;
    }
    
    public Map articleFindUserId(Map entity)  {
    	SqlSession session = sqlSessionFactory.openSession();
    	
    	Map map;
    	try {
    		map = (Map)session.selectOne("articleFindUserId", entity);	
    	}finally {
    		session.close();
    	}	
    	
    	return map;
    }

    public Map articleFindUserPassword(Map entity) {
    	SqlSession session = sqlSessionFactory.openSession();
    	
    	Map map;
    	try {
    		map = (Map)session.selectOne("articleFindUserPassword",entity);	
    	}finally {
    		session.close();
    	}	
    	
    	return map;
    }
    
    public int articleUpdateUserPassword(Map entity)  {
    	SqlSession session = sqlSessionFactory.openSession();
    	
    	int cnt = 0;
    	try {
    		cnt = session.update("articleUpdateUserPassword",entity);
    		
    		session.commit();
    	}catch(PersistenceException e) {
    		session.rollback();
    		e.printStackTrace();
    	}finally {
    		session.close();
    	}	

    	return cnt;
    }

	public Map getAdminMail() throws IOException {
    	SqlSession session = sqlSessionFactory.openSession();
    	
    	Map map;
    	try {
    		map = (Map)session.selectOne("adminSetupInfo");
    		
    		map.put("ADMIN_EMAIL", Cryptograph.getBASE64Decoder((String)map.get("ADMIN_EMAIL")));
    		map.put("ADMIN_EMAIL_PWD", Cryptograph.getBASE64Decoder((String)map.get("ADMIN_EMAIL_PWD")));
    	}finally {
    		session.close();
    	}	

    	return map;
	}
	
	public Map articleUserInfo(Map entity) {
    	SqlSession session = sqlSessionFactory.openSession();
    	
    	Map map;
    	try {
    		map = (Map)session.selectOne("articleUserInfo",entity);	
    	}finally {
    		session.close();
    	}	

		return map;
	}
	
	public boolean articleUserUpdate(Map entity)throws Exception{
    	SqlSession session = sqlSessionFactory.openSession();
    	
    	boolean result = false;
    	try {
    		int cnt = session.update("articleUserUpdate",entity);
    		if(cnt == 1){result = true;}
    		
    		session.commit();
    	}catch(PersistenceException e) {
    		session.rollback();
    		e.printStackTrace();
    	}finally {
    		session.close();
    	}	

		return result;
	}
	
	public boolean articleUserUnsubscribe(Map entity) {
    	SqlSession session = sqlSessionFactory.openSession();
    	
    	boolean result = false;
    	try {
    		int cnt = session.update("articleUserUnsubscribe",entity);
    		if(cnt == 1){result = true;}
    		
    		session.commit();
    	}catch(PersistenceException e) {
    		session.rollback();
    		e.printStackTrace();
    	}finally {
    		session.close();
    	}	

    	return result;
	}
}
