package mini.board.article;

import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import mini.board.common.BaseAction;
import mini.board.common.Cryptograph;
import mini.board.common.FileService;
import mini.board.common.NaverMail;
import mini.board.common.Navigator;
import mini.board.common.Validator;
import mini.board.exception.NotAuthorizedException;

import com.opensymphony.xwork2.ActionContext;

public class ArticleAction extends BaseAction {

	private ArticleBusiness bo = ArticleBusiness.getInstance();

	/**
	 * 게시판 관련 action 시 권한을 체크
	 * 
	 * @param context
	 * @throws NotAuthorizedException 권한이 없는 요청을 행했을때 발생
	 */
	public void common(Map context) throws NotAuthorizedException {

		// 게시판 접근레벨과 사용자 레벨을 체크하는 부분 - 시작
		int accessLevel = 0;
		if (entity.containsKey("group_idx")) {
			Map map = bo.articleGroupInfo(entity);
			ActionContext.getContext().getSession().put(SESSION_BULLETIN, map);

			if (map != null && map.containsKey("ACCESS_LEVEL")) {
				accessLevel = (Integer) map.get("ACCESS_LEVEL");
			}
		}

		int userLevel = 0;
		Map userInfoMap = (Map) ActionContext.getContext().getSession().get(SESSION_USER);
		if (userInfoMap != null && userInfoMap.containsKey("USER_LEVEL")) {
			userLevel = (Integer) userInfoMap.get("USER_LEVEL");
		}
		
		if (accessLevel > userLevel) throw new NotAuthorizedException("접근권한이 없습니다.");
		// 끝
	}

	/**
	 * articleList.jsp에 보여줄 값을 읽어 온다.
	 * 
	 * @return 처리 성공 여부 SUCCESS값
	 */
	public String articleList() {
		
		Map<String,Object> context = ActionContext.getContext().getContextMap();

		try {
			
			common(context);

			int lineCnt = bo.articleCountPerPage();
			String nowPage = StringUtils.isBlank((CharSequence)entity.get("nowPage"))?"1":(String) entity.get("nowPage");
			String group_idx = StringUtils.isBlank((CharSequence)entity.get("group_idx"))?"1":(String) entity.get("group_idx");

			String searchMethod = StringUtils.isBlank((CharSequence)entity.get("searchMethod"))?"title":(String)entity.get("searchMethod");
			String searchWord = StringUtils.isBlank((CharSequence)entity.get("searchWord"))?"":(String)entity.get("searchWord");

			entity.put("group_idx", group_idx);
			entity.put("lineCnt", lineCnt);
			entity.put("nowPage", nowPage);
			entity.put("offset", lineCnt * (Integer.parseInt(nowPage) - 1));

			entity.put("searchMethod", searchMethod);
			entity.put("searchWord", searchWord);

			int totlCnt = bo.articleListCnt(entity);
			List list = bo.articleList(entity);
			List nList = bo.articleNList(entity);

			String page = Navigator.pageNavi(totlCnt, Integer.parseInt(nowPage), lineCnt);

			context.put("BLOG_LIST", list);
			context.put("BLOG_NLIST", nList);
			context.put("PAGE", page);

			context.put("NUM", totlCnt - ((Integer.parseInt(nowPage) - 1) * lineCnt));

		} catch (NotAuthorizedException e) {
			return ERROR;
		}
		return SUCCESS;
	}

	/**
	 * articleWrite.jsp javascript를 호출한다.
	 * 
	 * @return 성공여부 String로 반환
	 */
	public String articleWrite() {
		
		Map<String,Object> context = ActionContext.getContext().getContextMap();
		
		try {

			common(context);

			List list = bo.articleCategory(entity);// cartegory name를 list에 담는다.
			context.put("CATEGORY_LIST", list); // 임시저장소 context에 cartegory name
												// list를 담는다.
		} catch (NotAuthorizedException e) {
			return ERROR;
		}
		return SUCCESS;
	}

	/**
	 * 글을 등록한다.
	 */
	public String articleInsert() {
		
		Map<String,Object> context = ActionContext.getContext().getContextMap();

		try {

			common(context);

			// SESSION값의 user_id를 entity 넣는다.
			Map session_map = (Map) ActionContext.getContext().getSession().get(SESSION_USER);
			entity.put("user_id", session_map.get("USER_ID"));

			String basePath = getText("path.upload_folder");
			
			FileService fs = new FileService();

			File[] file = (File[]) entity.get("file");
			String[] imageFilename = null;
			String[] imageContentType = null;
			String[] imageLength = null;
			if (entity.get("nic_filename") instanceof String) {
				imageFilename = new String[1];
				imageFilename[0] = (String) entity.get("nic_filename");
				imageContentType = new String[1];
				imageContentType[0] = (String) entity.get("nic_contentType");
				imageLength = new String[1];
				imageLength[0] = (String) entity.get("nic_length");
			} else if (entity.get("nic_filename") instanceof String[]) {
				imageFilename = (String[]) entity.get("nic_filename");
				imageContentType = (String[]) entity.get("nic_contentType");
				imageLength = (String[]) entity.get("nic_length");
			}

			ArrayList imageFileList = new ArrayList();
			HashMap imageMap = null;
			for (int i = 0; i < (imageFilename == null ? -1 : imageFilename.length); i++) {
				imageMap = new HashMap();
				
				String fname = imageFilename[i];
				int p = fname.lastIndexOf("/");
				if (p > 0) {
					fname = fname.substring(p + 1);
				}
				
				imageMap.put("filename", fname);
				imageMap.put("filepath", basePath);
				imageMap.put("filetype", imageContentType[i]);
				imageMap.put("filesize", imageLength[i]);
				imageMap.put("input_type", "nicedit");
				imageFileList.add(imageMap);
			}

			ArrayList fileFileList = new ArrayList();
			HashMap fileMap = null;
			for (int i = 0; i < (file == null ? -1 : file.length); i++) {
				String filename = fs.saveFile(file[i], basePath, ((String[]) entity.get("fileFileName"))[i]);
				fileMap = new HashMap();
				fileMap.put("filename", filename);
				fileMap.put("filepath", basePath);
				fileMap.put("filetype", ((String[]) entity.get("fileContentType"))[i]);
				fileMap.put("filesize", file[i].length());
				fileMap.put("input_type", "");
				fileFileList.add(fileMap);
			}
			entity.put("imageFileList", imageFileList);
			entity.put("fileFileList", fileFileList);
			
			bo.articleInsert(entity);

		} catch (NotAuthorizedException e) {
			return ERROR;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "doArticleList";
	}

	/**
	 * 이미지 업로드용
	 * 
	 * @return
	 */
	public String articleImageUpload() {
		
		Map<String,Object> context = ActionContext.getContext().getContextMap();

		try {

			String basePath = getText("path.upload_folder");

			FileService fs = new FileService();

			File[] image = (File[]) entity.get("image");

			String filename = fs.saveFile(image[0], basePath, ((String[]) entity.get("imageFileName"))[0]);

			String contentType = ((String[]) entity.get("imageContentType"))[0];

			long length = image[0].length();
			
			String result = "{\"upload\":{\"links\":{\"original\":\"./upload/" + filename
					+ "\"},\"image\":{\"width\":100,\"height\":65,\"contentType\":\"" + contentType + "\",\"length\":"
					+ length + "}}}";

			context.put("RESULT", result);

		} catch (IOException e) {
			e.printStackTrace();
		}

		return "resultText";
	}

	/**
	 * CommentList(댓글)를 불러온다
	 * 
	 * @return
	 */
	public String articleView() {
		
		Map<String,Object> context = ActionContext.getContext().getContextMap();

		try {

			common(context);

			entity.put("input_type", "nicedit");
			Map map = bo.articleView(entity);

			int inquery_level = (Integer) map.get("INQUERY_LEVEL");
			int user_Level = 0;
			
			Map sessionMap = (Map) ActionContext.getContext().getSession().get(SESSION_USER);
			if(sessionMap != null) user_Level = (Integer) sessionMap.get("USER_LEVEL");

			if (user_Level < inquery_level) {
				return ERROR;
			}

			bo.articleAddCnt(entity);
			context.put("BLOG_VIEW", map);

			List list = bo.articleCommentList(entity);
			context.put("BLOG_COMMENT_LIST", list);

		} catch (NotAuthorizedException e) {
			return ERROR;
		}
		return SUCCESS;
	}

	/**
	 * 개시판에 쓴 글에 대하여 댓글을 단다
	 * 
	 * @return
	 */
	public String articleReply() {
		
		Map<String,Object> context = ActionContext.getContext().getContextMap();

		try {

			common(context);

			Map map = bo.articleView(entity);

			context.put("BLOG_VIEW", map);

			List list = bo.articleCategory(entity);
			context.put("CATEGORY_LIST", list);

		} catch (NotAuthorizedException e) {
			return ERROR;
		}
		return SUCCESS;
	}

	/**
	 * articleReply.jsp에서 articleReplayInsert Button Action이 발생하면 호출
	 * articlelogon과 articlelogin이 있는 원리와 같다. Image와 File을 저장하는 Action을 실행한다.
	 * 
	 * @return
	 */
	public String articleReplyInsert() {
		
		Map<String,Object> context = ActionContext.getContext().getContextMap();

		try {

			common(context);

			Map session_map = (Map) ActionContext.getContext().getSession().get(SESSION_USER);
			entity.put("user_id", session_map.get("USER_ID"));

			String basePath = getText("path.upload_folder");

			FileService fs = new FileService();

			File[] image = (File[]) entity.get("image");
			File[] file = (File[]) entity.get("file");

			// 이미지 파일이 저장된 경로와 속성을 Map형태로 ArayList에 넣는다.
			ArrayList imageFileList = new ArrayList();
			HashMap imageMap = null;
			for (int i = 0; i < (image == null ? -1 : image.length); i++) {
				String filename = fs.saveFile(image[i], basePath, ((String[]) entity.get("imageFileName"))[i]);
				imageMap = new HashMap();
				imageMap.put("type_code", "0");
				imageMap.put("filename", filename);
				imageMap.put("filepath", basePath);
				imageMap.put("filetype", ((String[]) entity.get("imageContentType"))[i]);
				imageMap.put("filesize", image[i].length());
				imageMap.put("input_type", "nicedit");
				imageFileList.add(imageMap);
			}

			// 첨부파일이 저장된 경로와 속성을 Map형태로 ArrayList에 저장
			ArrayList fileFileList = new ArrayList();
			HashMap fileMap = null;
			for (int i = 0; i < (file == null ? -1 : file.length); i++) {
				String filename = fs.saveFile(file[i], basePath, ((String[]) entity.get("fileFileName"))[i]);
				fileMap = new HashMap();
				fileMap.put("type_code", "1");
				fileMap.put("filename", filename);
				fileMap.put("filepath", basePath);
				fileMap.put("filetype", ((String[]) entity.get("fileContentType"))[i]);
				fileMap.put("filesize", file[i].length());
				fileMap.put("input_type", "");
				fileFileList.add(fileMap);
			}
			// entity에 imageFile과 첨부File을 저장한다.
			entity.put("imageFileList", imageFileList);
			entity.put("fileFileList", fileFileList);

			// DataBase에 이미지
			bo.articleReplyInsert(entity);

		} catch (NotAuthorizedException e) {
			return ERROR;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "doArticleList";
	}

	/**
	 * 
	 * @return
	 */
	public String articleEdit() {
		
		Map<String,Object> context = ActionContext.getContext().getContextMap();

		try {

			common(context);

			Map map = bo.articleView(entity);

			context.put("BLOG_VIEW", map);

			List list = bo.articleCategory(entity);
			context.put("CATEGORY_LIST", list);

		} catch (NotAuthorizedException e) {
			return ERROR;
		}
		return SUCCESS;
	}

	/**
	 * Write.jsp page에서 첨부파일및 이미지를 수정 하는 Method
	 * 
	 * @return
	 */
	public String articleUpdate() {
		
		Map<String,Object> context = ActionContext.getContext().getContextMap();

		try {

			common(context);

			String basePath = getText("path.upload_folder");

			FileService fs = new FileService();

			File[] file = (File[]) entity.get("file");
			String[] imageFilename = null;
			String[] imageContentType = null;
			String[] imageLength = null;
			if (entity.get("nic_filename") instanceof String) {
				imageFilename = new String[1];
				imageFilename[0] = (String) entity.get("nic_filename");
				imageContentType = new String[1];
				imageContentType[0] = (String) entity.get("nic_contentType");
				imageLength = new String[1];
				imageLength[0] = (String) entity.get("nic_length");
			} else if (entity.get("nic_filename") instanceof String[]) {
				imageFilename = (String[]) entity.get("nic_filename");
				imageContentType = (String[]) entity.get("nic_contentType");
				imageLength = (String[]) entity.get("nic_length");
			}

			ArrayList imageFileList = new ArrayList();
			HashMap imageMap = null;
			for (int i = 0; i < (imageFilename == null ? -1 : imageFilename.length); i++) {
				imageMap = new HashMap();

				String fname = imageFilename[i];
				int p = fname.lastIndexOf("/");
				if (p > 0) {
					fname = fname.substring(p + 1);
				}

				imageMap.put("filename", fname);
				imageMap.put("filepath", basePath);
				imageMap.put("filetype", imageContentType[i]);
				imageMap.put("filesize", imageLength[i]);
				imageMap.put("input_type", "nicedit");
				imageFileList.add(imageMap);
			}
			ArrayList fileFileList = new ArrayList();
			HashMap fileMap = null;
			for (int i = 0; i < (file == null ? -1 : file.length); i++) {
				String filename = fs.saveFile(file[i], basePath, ((String[]) entity.get("fileFileName"))[i]);
				fileMap = new HashMap();
				fileMap.put("filename", filename);
				fileMap.put("filepath", basePath);
				fileMap.put("filetype", ((String[]) entity.get("fileContentType"))[i]);
				fileMap.put("filesize", file[i].length());
				fileMap.put("input_type", "");
				fileFileList.add(fileMap);
			}
			entity.put("imageFileList", imageFileList);
			entity.put("fileFileList", fileFileList);

			ArrayList deleteFileList = new ArrayList();
			HashMap deleteMap = null;
			if (entity.get("delete_file") instanceof String) {
				fileMap = new HashMap();
				fileMap.put("file_idx", entity.get("delete_file"));
				deleteFileList.add(fileMap);

			} else if (entity.get("delete_file") instanceof String[]) {
				String[] deleteList = (String[]) entity.get("delete_file");
				for (String fileIdx : deleteList) {
					fileMap = new HashMap();
					fileMap.put("file_idx", fileIdx);
					deleteFileList.add(fileMap);
				}
			}

			entity.put("deleteFileList", deleteFileList);

			bo.articleUpdate(entity);

		} catch (NotAuthorizedException e) {
			return ERROR;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "doArticleList";
	}

	public String articleDelete() {
		
		Map<String,Object> context = ActionContext.getContext().getContextMap();

		try {
			
			common(context);
			
			Map session_map = (Map) ActionContext.getContext().getSession().get(SESSION_USER);
			entity.put("user_id", session_map.get("USER_ID"));
			
			bo.articleDelete(entity);

		} catch (NotAuthorizedException e) {
			return ERROR;
		}
		return "doArticleList";
	}

	/**
	 * 첨부 파일을 삭제하는 Method
	 * 
	 * @return
	 */
	public String articleFileDelete() {
		
		Map<String,Object> context = ActionContext.getContext().getContextMap();

		try {

			common(context);

			Map fileMap = bo.articleFileView(entity);

			int cnt = bo.articleFileDelete(entity);

			String result = "false";

			if (cnt == 1) {
				File file = new File((String) fileMap.get("FILEPATH") + (String) fileMap.get("FILENAME"));
				if (file.delete()) {
					result = "true";
				}
			}

			context.put("RESULT", result);

		} catch (NotAuthorizedException e) {
			return ERROR;
		}
		return "resultText";
	}

	public String articleCommentReplyInsert() {
		
		Map<String,Object> context = ActionContext.getContext().getContextMap();

		try {

			common(context);

			// 필수 파라미터 user_id, comment_idx

			bo.articleCommentReplyInsert(entity);

		} catch (NotAuthorizedException e) {
			return ERROR;
		}
		return "doArticleView";
	}

	/**
	 * 댓글을 입력하는 Method
	 * 
	 * @return
	 */
	public String articleCommentInsert() {
		
		Map<String,Object> context = ActionContext.getContext().getContextMap();

		try {

			common(context);

			Map session_map = (Map) ActionContext.getContext().getSession().get(SESSION_USER);
			entity.put("user_id", session_map.get("USER_ID"));

			bo.articleCommentInsert(entity);

		} catch (NotAuthorizedException e) {
			return ERROR;
		}
		return "doArticleView";
	}

	/**
	 * 댓글을 삭제하는 Method
	 * 
	 * @return
	 */
	public String articleCommentDelete() {
		
		Map<String,Object> context = ActionContext.getContext().getContextMap();

		try {

			common(context);

			bo.articleCommentDelete(entity);

		} catch (NotAuthorizedException e) {
			return ERROR;
		}
		return "doArticleView";
	}

	/**
	 * 댓글을 수정하는 Method
	 * 
	 * @return
	 */
	public String articleCommentUpdate() {
		
		Map<String,Object> context = ActionContext.getContext().getContextMap();

		try {

			common(context);

			bo.articleCommentUpdate(entity);

		} catch (NotAuthorizedException e) {
			return ERROR;
		}
		return "doArticleView";
	}

	/**
	 * Login에서 사용자의 ID와 PassWord를 입력받는 Method Logon을 실행시키기 위한 Method
	 * 
	 * @return
	 */
	public String articleLogin() {
		Map<String,Object> context = ActionContext.getContext().getContextMap();

		Map map = bo.articleGroupInfo(entity);
		ActionContext.getContext().getSession().put(SESSION_BULLETIN, map);
		String url = request.getHeader("referer");

		ActionContext.getContext().getSession().put("LAST_URL", url);
		
		return SUCCESS;
	}

	/**
	 * 실제 사용자의 Login의 처리하는 Method
	 * 
	 * @return
	 */
	public String articleLogon() {
		
		Map<String,Object> context = ActionContext.getContext().getContextMap();

		try {
			
			if (!Validator.validate(entity, "user_id", "pwd")) {
				return INPUT;
			}

			entity.put("pwd", Cryptograph.getCryptoStr(getText("enctype"), (String) entity.get("pwd")));
			Map map = bo.articleLogin(entity);

			if (map != null) {
				ActionContext.getContext().getSession().put(SESSION_USER, map);

				String url = (String) ActionContext.getContext().getSession().get("LAST_URL");
				if (url != null) {
					context.put("LAST_URL", url);
					return "redirect";
				}

				return "doArticleList";
			} else {
				context.put("LOGIN_FAIL", "true");
				return "loginFail";
			}

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return ERROR;
	}

	/**
	 * 사용자의 아이디를 찾을때 사용자의 입력을 받는 Method
	 * 
	 * @return
	 */
	public String articleFindUserId() {

		Map<String,Object> context = ActionContext.getContext().getContextMap();

		return SUCCESS;
	}

	/**
	 * 사용자 아이디 찾기를 실제로 처리하는 Method
	 * 
	 * @return
	 */
	public String articleFindUserIdOn() {
		Map<String,Object> context = ActionContext.getContext().getContextMap();

		Map map = bo.articleFindUserId(entity);

		if (map != null) {
			context.put("USER_ID", map.get("USER_ID"));
		} else {
			context.put("USER_ID", null);
		}

		return "articleFindUserIdPage";
	}

	/**
	 * 사용자의 PassWord를 찾을때 필요 입력값을 받은 Method
	 * 
	 * @return
	 */
	public String articleFindUserPassword() {

		Map<String,Object> context = ActionContext.getContext().getContextMap();

		return SUCCESS;
	}

	/**
	 * 사용자의 PassWord 찾기를 실제로 처리하는 Method
	 * 
	 * @return
	 */
	public String articleFindUserPasswordOn() {
		
		if (!Validator.validate(entity, "user_id", "email")) {
			return INPUT;
		}
		
		Map<String,Object> context = ActionContext.getContext().getContextMap();
		
		try {
			
			Map map = bo.articleFindUserPassword(entity);

			/**
			 * bo로 부터 돌려받은 Data와 entity로부터 받은 Data를 비교하여 두 값이 모두 같으면
			 */
			if (map != null) {
				if (((String) map.get("USER_ID")).equals((String) entity.get("user_id"))
						&& (((String) map.get("EMAIL")).equals((String) entity.get("email")))) {

					StringBuffer sb = new StringBuffer();
					for (int i = 0; i < 10; i++) {
						sb.append((char) ((Math.random() * 94) + 33));// 특수문자포함
																		// 10글자
																		// 생성
					}
					String newPwd = sb.toString();

					entity.put("pwd", Cryptograph.getCryptoStr(getText("enctype"), newPwd));
					bo.articleUpdateUserPassword(entity);

					Map adminMail = bo.getAdminMail();
					String adminEmail = (String) adminMail.get("ADMIN_EMAIL");
					String adminEmailPwd = (String) adminMail.get("ADMIN_EMAIL_PWD");

					if (adminEmail.length() > 0 && adminEmailPwd.length() > 0) {
						NaverMail navermail = new NaverMail(adminEmail, adminEmailPwd);
						navermail.sendMail((String) map.get("EMAIL"), "비밀번호를 알려드립니다.",
								"새로 생성된 임시 비밀번호 [" + newPwd + "]");
						context.put("USER_PASSWORD", newPwd);
					}
				}
			} else {
				context.put("USER_PASSWORD", null);
			}

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "articleFindUserPasswordPage";
	}

	/**
	 * 마지막에 접속해 들어온 URL을 반환하는 Method Login에서 Logon을 처리후 다시 Login으로 갈때 Login값을
	 * 반환하는 Logic
	 * 
	 * @return
	 */
	public String articleReturnPage() {
		
		Map<String,Object> context = ActionContext.getContext().getContextMap();

		String url = request.getHeader("referer");
		if (url != null) {
			context.put("LAST_URL", url);
			return "redirect";
		}

		return "doArticleList";
	}

	/**
	 * 
	 * @return
	 */
	public String articleLogout() {

		Map<String,Object> context = ActionContext.getContext().getContextMap();
		
		ActionContext.getContext().getSession().remove(SESSION_USER);
		// String url = request.getHeader("referer");
		// if(url != null) {
		// context.put("LAST_URL", url);
		// return "redirect";
		// }

		return "welcome";
	}

	/**
	 * 
	 * @return
	 */
	public String articleJoin() {
		
		Map<String,Object> context = ActionContext.getContext().getContextMap();

		Map map = bo.articleGroupInfo(entity);
		// 회원 가입과 동시에 SESSION을 갖는다.
		ActionContext.getContext().getSession().put(SESSION_BULLETIN, map);

		context.put("SKIN_PATH", (String) map.get("SKIN_PATH"));

		return SUCCESS;
	}

	/**
	 * 
	 * @return
	 */
	public String articleEntry() {
		try {
			entity.put("pwd", Cryptograph.getCryptoStr(getText("enctype"), (String) entity.get("pwd")));
			entity.put("user_level", 0);

			bo.articleJoin(entity);

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return "doArticleList";
	}

	/**
	 * 회원 가입시 UserID 중복 체크
	 * 
	 * @return
	 */
	public String articleCheckId() {
		
		Map<String,Object> context = ActionContext.getContext().getContextMap();

		String result = "false";
		if (bo.articleCheckId(entity)) {
			result = "true";
		}

		context.put("RESULT", result);

		return "resultText";
	}

	public String articleUserInfo() {

		Map<String,Object> context = ActionContext.getContext().getContextMap();
		
		Map userInfoMap = (Map) ActionContext.getContext().getSession().get(SESSION_USER);

		if (userInfoMap == null) {
			return ERROR;
		}

		entity.put("user_id", userInfoMap.get("USER_ID"));
		Map map = bo.articleUserInfo(entity);
		context.put("USER_INFO", map);

		return SUCCESS;
	}

	public String articleUserUpdate() {
		try {
			Map userInfoMap = (Map) ActionContext.getContext().getSession().get(SESSION_USER);
			if (userInfoMap == null) {
				return ERROR;
			}
			entity.put("user_id", userInfoMap.get("USER_ID"));

			if (!bo.articleUserUpdate(entity)) {
				return ERROR;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return "doArticleList";
	}

	public String articleUserUnsubscribe() {

		try {
			Map userInfoMap = (Map) ActionContext.getContext().getSession().get(SESSION_USER);
			if (userInfoMap == null) {
				return ERROR;
			}
			
			entity.put("user_id", userInfoMap.get("USER_ID"));
			if (!bo.articleUserUnsubscribe(entity)) {
				return ERROR;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return "doArticleLogout";
	}
}
