package mini.board.admin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import mini.board.common.BaseAction;
import mini.board.common.Cryptograph;
import mini.board.common.Navigator;
import mini.board.common.Validator;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.opensymphony.xwork2.ActionContext;

public class AdminAction extends BaseAction{

	private AdminBusiness bo = AdminBusiness.getInstance();
	
	/**
	 * 관리자 > 게시판 목록 페이지 
	 * @return SUCCESS
	 */
	public String adminBulletinList(){
		
		Map<String,Object> context = ActionContext.getContext().getContextMap();

		try{
			boolean adminBoolean = ActionContext.getContext().getSession().get(SESSION_USER) == null ? false : (Integer)((Map)ActionContext.getContext().getSession().get(SESSION_USER)).get("USER_LEVEL")==255?true:false;
			if(!adminBoolean) return LOGIN;
			
		    int lineCnt	= 15;
			String nowPage	= StringUtils.isBlank((CharSequence)entity.get("nowPage"))?"1":(String)entity.get("nowPage");
									
			String searchMethod = StringUtils.isBlank((CharSequence) entity.get("searchMethod"))?"group_name":(String)entity.get("searchMethod");
			String searchWord = StringUtils.isBlank((CharSequence)entity.get("searchWord"))?"":(String)entity.get("searchWord");
			
			entity.put("lineCnt", lineCnt);
			entity.put("nowPage", nowPage);
			entity.put("offset", lineCnt*(Integer.parseInt(nowPage)-1));
			
			entity.put("searchMethod", searchMethod);
			entity.put("searchWord"  , searchWord);
			
			int totlCnt	= bo.adminBulletinListCnt(entity);
			List list = bo.adminBulletinList(entity);	
			
			String page = Navigator.pageNavi(totlCnt, Integer.parseInt(nowPage), lineCnt);
			
			context.put("BULLETIN_LIST", list);									
			context.put("PAGE", page);
				
		}catch(Exception e){
			e.printStackTrace();
		}
		return SUCCESS;
	}	
	
	/**
	 * 게시판 추가 페이지 
	 * @return SUCCESS
	 */
	public String adminBulletinAdd(){
		
		Map<String,Object> context = ActionContext.getContext().getContextMap();

		try{
			
			boolean adminBoolean = ActionContext.getContext().getSession().get(SESSION_USER) == null ? false : (Integer)((Map)ActionContext.getContext().getSession().get(SESSION_USER)).get("USER_LEVEL")==255?true:false;
			if(!adminBoolean) return LOGIN;
			
			List categoryList = bo.adminPropertyBaseCategory();
			
			context.put("CATEGORY_LIST", categoryList);	
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	/**
	 * 게시판 추가 메소드 
	 * @return SUCCESS
	 */
	public String adminBulletinInsert(){
		try{
			
			boolean adminBoolean = ActionContext.getContext().getSession().get(SESSION_USER) == null ? false : (Integer)((Map)ActionContext.getContext().getSession().get(SESSION_USER)).get("USER_LEVEL")==255?true:false;
			if(!adminBoolean) return LOGIN;
			
			if(entity.containsKey("C_CATEGORY_NAME")) {
				
				ArrayList categoryList = new ArrayList();	
				
				if(entity.get("C_CATEGORY_NAME") instanceof String[]) {
					String[] delFlagArray = (String[]) entity.get("C_DEL_FLAG");
					String[] descriptionArray = (String[]) entity.get("C_DESCRIPTION");
					String[] categoryNameArray = (String[]) entity.get("C_CATEGORY_NAME");
					
					int i=0;
					for(String str : categoryNameArray) {
						
						HashMap category = new HashMap();
						category.put("output_seq", (i+1));
						category.put("category_name", categoryNameArray[i]);
						category.put("description", descriptionArray[i]);
						category.put("del_flag", delFlagArray[i]);
						
						categoryList.add(category);
						i++;
					}
					
				}else{
					HashMap category = new HashMap();
					category.put("output_seq", 1);
					category.put("category_name", entity.get("C_CATEGORY_NAME"));
					category.put("description", entity.get("C_DESCRIPTION"));
					category.put("del_flag", entity.get("C_DEL_FLAG"));
					
					categoryList.add(category);
				}
				
				entity.put("CATEGORY_LIST", categoryList);
			}
			
			bo.adminBulletinInsert(entity);
			
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return "doAdminBulletinList";
	}
	
	/**
	 * 게시판 설정 수정 폼 구성 
	 * @return SUCCESS
	 */
	public String adminBulletinEdit(){
		
		Map<String,Object> context = ActionContext.getContext().getContextMap();

		boolean adminBoolean = ActionContext.getContext().getSession().get(SESSION_USER) == null ? false : (Integer)((Map)ActionContext.getContext().getSession().get(SESSION_USER)).get("USER_LEVEL")==255?true:false;
		if(!adminBoolean) return LOGIN;
		
		Map map = bo.adminBulletinInfo(entity);
		
		context.put("BULLETIN_INFO", map);
			
		return SUCCESS;
	}
	
	/**
	 * 관리자 > 게시판 수정 
	 * @return doAdminBulletinList
	 */
	public String adminBulletinUpdate(){
		try{
			
			boolean adminBoolean = ActionContext.getContext().getSession().get(SESSION_USER) == null ? false : (Integer)((Map)ActionContext.getContext().getSession().get(SESSION_USER)).get("USER_LEVEL")==255?true:false;
			if(!adminBoolean) return LOGIN;
		
			if(entity.containsKey("C_CATEGORY_NAME")) {
				
				ArrayList categoryList = new ArrayList();	
				if(entity.get("C_CATEGORY_NAME") instanceof String[]) {
					String[] delFlagArray = (String[]) entity.get("C_DEL_FLAG");
					String[] descriptionArray = (String[]) entity.get("C_DESCRIPTION");
					String[] categoryNameArray = (String[]) entity.get("C_CATEGORY_NAME");
					String[] categoryIdxArray = (String[]) entity.get("C_CATEGORY_IDX");
					
					int i=0;
					for(String str : categoryNameArray) {
						
						HashMap category = new HashMap();
						category.put("output_seq", (i+1));
						category.put("category_name", categoryNameArray[i]);
						category.put("description", descriptionArray[i]);
						category.put("del_flag", delFlagArray[i]);
						category.put("category_idx", categoryIdxArray[i]);
						
						categoryList.add(category);
						i++;
					}
					
				}else{
					HashMap category = new HashMap();
					category.put("output_seq", 1);
					category.put("category_name", entity.get("C_CATEGORY_NAME"));
					category.put("description", entity.get("C_DESCRIPTION"));
					category.put("del_flag", entity.get("C_DEL_FLAG"));
					category.put("category_idx", entity.get("C_CATEGORY_IDX"));
					
					categoryList.add(category);
				}
				
				entity.put("CATEGORY_LIST", categoryList);
			}
			bo.adminBulletinUpdate(entity);			
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return "doAdminBulletinList";
	}
	
	/**
	 * 게시판 기본 설정 데이터 불러오기
	 * @return
	 */
	public String adminBulletinSetup() {
		
		Map<String,Object> context = ActionContext.getContext().getContextMap();

		boolean adminBoolean = ActionContext.getContext().getSession().get(SESSION_USER) == null ? false : (Integer)((Map)ActionContext.getContext().getSession().get(SESSION_USER)).get("USER_LEVEL")==255?true:false;
		if(!adminBoolean) return LOGIN;
		
		// ARTICLE_COUNT_PER_PAGE - 페이지당 글 수
		// BULLETIN_BASE_CATEGORY - 게시판 생성시 기본 카테고리
		
		Map map = new HashMap();
		
		int acpp = bo.adminPropertyCountPerPage();
		map.put("ARTICLE_COUNT_PER_PAGE", acpp);
		
		List bbcList = bo.adminPropertyBaseCategory();
		System.out.println(bbcList);
		map.put("BULLETIN_BASE_CATEGORY", bbcList);
		
		context.put("BULLETIN_SETUP", map);
			
		return SUCCESS;
	}
	
	/**
	 * 관리자 > 사용자 리스트 화면 
	 * @return SUCCESS
	 */
	public String adminUserList(){
		
		Map<String,Object> context = ActionContext.getContext().getContextMap();

		try{
			
			boolean adminBoolean = ActionContext.getContext().getSession().get(SESSION_USER) == null ? false : (Integer)((Map)ActionContext.getContext().getSession().get(SESSION_USER)).get("USER_LEVEL")==255?true:false;
			if(!adminBoolean) return LOGIN;
			
		    int lineCnt	= 15;		    
			String nowPage	= (entity.get("nowPage")==null||"".equals((String)entity.get("nowPage")))?"1":(String)entity.get("nowPage");
									
			String searchMethod = entity.get("searchMethod")==null?"user_id":(String)entity.get("searchMethod");
			String searchWord = entity.get("searchWord")==null?"":(String)entity.get("searchWord");
			
			entity.put("lineCnt", lineCnt);
			entity.put("nowPage", nowPage);
			entity.put("offset", lineCnt*(Integer.parseInt(nowPage)-1));
			
			entity.put("searchMethod", searchMethod);
			entity.put("searchWord"  , searchWord);
			
			int totlCnt	= bo.adminUserListCnt(entity);
			List list = bo.adminUserList(entity);	
			
			String page = Navigator.pageNavi(totlCnt, Integer.parseInt(nowPage), lineCnt);
			
			context.put("USER_LIST", list);									
			context.put("PAGE", page);
			context.put("NUM", totlCnt - ((Integer.parseInt(nowPage) -1)*lineCnt));
				
		}catch(Exception e){
			e.printStackTrace();
		}
		return SUCCESS;
	}	
	
	/**
	 * 관리자 > User 추가 
	 * @return
	 */
	public String adminUserAdd(){
		try{
					
			boolean adminBoolean = ActionContext.getContext().getSession().get(SESSION_USER) == null ? false : (Integer)((Map)ActionContext.getContext().getSession().get(SESSION_USER)).get("USER_LEVEL")==255?true:false;
			if(!adminBoolean) return LOGIN;
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	/**
	 * 관리자 > User 추가 
	 * @return
	 */
	public String adminUserInsert(){
		try{	
			
			if(!Validator.validate(entity, "user_id", "pwd", "user_name", "user_level", "homepage", "email", "description", "del_flag")) {
				return INPUT;
			}
			
			entity.put("pwd", Cryptograph.getCryptoStr(getText("enctype"), (String)entity.get("pwd")));
			
			bo.adminUserInsert(entity);
						
		}catch(Exception e){
			e.printStackTrace();
		}
		return "doAdminUserList";	
	}
	
	/**
	 * 일반 User 수정 
	 * @return SUCCESS
	 */
	public String adminUserEdit(){
		
		Map<String,Object> context = ActionContext.getContext().getContextMap();

		try{
			
			boolean adminBoolean = ActionContext.getContext().getSession().get(SESSION_USER) == null ? false : (Integer)((Map)ActionContext.getContext().getSession().get(SESSION_USER)).get("USER_LEVEL")==255?true:false;
			if(!adminBoolean) return LOGIN;
			
			Map map = bo.adminUserInfo(entity);
			
			context.put("USER_INFO", map);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	/**
	 * 관리자 > User 수정
	 * @return doAdminUserList
	 */
	public String adminUserUpdate(){
		try{
					
			boolean adminBoolean = ActionContext.getContext().getSession().get(SESSION_USER) == null ? false : (Integer)((Map)ActionContext.getContext().getSession().get(SESSION_USER)).get("USER_LEVEL")==255?true:false;
			if(!adminBoolean) return LOGIN;
			
			// 관리자가 User 정보 변경시 password를 넘기지 않으면 password 수정 안되게 변경 
			if( "".equals(((String)entity.get("pwd")).trim())){
				if(!Validator.validate(entity, "user_id","user_name", "user_level", "homepage", "email", "description", "del_flag")) {
					return INPUT;
				}
			} else {
				if(!Validator.validate(entity, "user_id", "pwd", "user_name", "user_level", "homepage", "email", "description", "del_flag")) {
					return INPUT;
				}
				entity.put("pwd", Cryptograph.getCryptoStr(getText("enctype"), (String)entity.get("pwd")));
			}
			bo.adminUserUpdate(entity);			
		}catch(Exception e){
			e.printStackTrace();
		}
		return "doAdminUserList";
	}
	
	public String adminPropertyList() {
		Map<String,Object> context = ActionContext.getContext().getContextMap();

		try{
			
			String searchMethod = entity.get("searchMethod")==null?"property_id":(String)entity.get("searchMethod");
			String searchWord = entity.get("searchWord")==null?"":(String)entity.get("searchWord");
			
			entity.put("searchMethod", searchMethod);
			entity.put("searchWord"  , searchWord);
			
			List list = bo.adminPropertyList(entity);
			
			context.put("PROPERTY_LIST", list);
						
		}catch(Exception e){
			e.printStackTrace();
		}
		return SUCCESS;	
	}
	
	/**
	 * 프로퍼티 업데이트 
	 * @return
	 */
	public String adminPropertyUpdate(){
		String result = "doAdminBulletinSetup";
		
		try{	

			String propertyId = (String) entity.get("property_id");
			if("BULLETIN_BASE_CATEGORY".equals(propertyId)) {
				if(entity.containsKey("C_CATEGORY_NAME")) {
					
					ArrayList categoryList = new ArrayList();	
					
					if(entity.get("C_CATEGORY_NAME") instanceof String[]) {
						String[] delFlagArray = (String[]) entity.get("C_DEL_FLAG");
						String[] descriptionArray = (String[]) entity.get("C_DESCRIPTION");
						String[] categoryNameArray = (String[]) entity.get("C_CATEGORY_NAME");
						
						int i=0;
						for(String str : categoryNameArray) {
							
							HashMap category = new HashMap();
							category.put("output_seq", (i+1));
							category.put("category_name", categoryNameArray[i]);
							category.put("description", descriptionArray[i]);
							category.put("del_flag", delFlagArray[i]);
							
							categoryList.add(category);
							i++;
						}
						
					}else{
						HashMap category = new HashMap();
						category.put("output_seq", 1);
						category.put("category_name", entity.get("C_CATEGORY_NAME"));
						category.put("description", entity.get("C_DESCRIPTION"));
						category.put("del_flag", entity.get("C_DEL_FLAG"));
						
						categoryList.add(category);
					}
					
					ObjectMapper mapper = new ObjectMapper();
					String json = mapper.writeValueAsString(categoryList);
					
					entity.put("property_value", json);
				}
			}else if("SYSTEM_EMAIL".equals(propertyId)) {
				
				result = "adminPropertySystemEmail";
				
				HashMap email = new HashMap();
				
				if(((String)entity.get("admin_email")).trim().length() > 0) {
					email.put("admin_email", Cryptograph.getBASE64Encoder((String)entity.get("admin_email")));
    			}
    			if(((String)entity.get("admin_email_pwd")).trim().length() > 0) {
    				email.put("admin_email_pwd", Cryptograph.getBASE64Encoder((String)entity.get("admin_email_pwd")));
    			}
    			
    			ObjectMapper mapper = new ObjectMapper();
				String json = mapper.writeValueAsString(email);
				
				entity.put("property_value", json);
			}
			
			bo.adminPropertyUpdate(entity);

		}catch(Exception e){
			e.printStackTrace();
		}
		return result;	
	}
	
	public String adminPropertySystemEmail(){
		
		Map<String,Object> context = ActionContext.getContext().getContextMap();

		try{	
			Map map = bo.adminPropertySystemEmail();
			
			context.put("SETUP", map);
						
		}catch(Exception e){
			e.printStackTrace();
		}
		return SUCCESS;	
	}
	
}